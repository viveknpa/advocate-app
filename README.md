Steps to run this project on a local machine :-

Download node.js from https://www.nodejs/org/en/ 

Download mongodb database from https://www.mongodb.com 

install yarn from https://yarnpkg.com/en/docs/install#windows-stable

After download complete, to start mongodb database server, we need to provide a local path to save the data and then
    -> start mongodb database server 


run command - "yarn add package" to install all dependcies

from root directory,
    run command, "yarn build:dev"
    run command, "yarn start"

now visit browser window: localhost:3000
