var {User} = require('./../models/user');
const moment = require('moment');

var authenticate = (req, res, next) => {
  var token = req.header('x-auth');

  User.findByToken(token).then((user) => {
    const accountExpired = moment().valueOf() > user.accountExpiresOn;
    if(!user) {
      return Promise.reject();
    } else if(user.accountLocked) {
      return res.status(401).send({ accountLocked: true });
    } else if(accountExpired) {
      return res.status(401).send({ accountExpired: true });
    } 

    req.user = user;
    req.token = token;
    next();
  }).catch((e) => {
      res.status(401).send();    //401 means authentication is required
  });
};

module.exports = {authenticate};
