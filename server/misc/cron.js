const moment = require("moment");
var schedule = require("node-schedule");
const axios = require("axios");

const { Tarikh } = require("../models/tarikh");
const { Message } = require("../models/message");
var rule = new schedule.RecurrenceRule();
rule.dayOfWeek = [1, 2, 3, 4, 5, 6]; //0 from sunday
rule.hour = [8, 15];
rule.minute = 0;

var job = schedule.scheduleJob(rule, () => {
  console.log(moment(), "Script for tarikh message will run!");
  Tarikh.find()
    .populate([
      {
        path: "_client",
        select: "caseId mobile sentMessages"
      },
      {
        path: "_creator",
        select: "fullName mobile sentMessages"
      }
    ])
    .then(tarikhs => {
      tarikhs.forEach(tarikh => {
        if (tarikh._creator.sentMessages && tarikh._client.sentMessages) {
          const isTodayTarikh = moment().isSame(moment(tarikh.date), "day");
          const isTomorrowTarikh = moment()
            .add(1, "days")
            .isSame(moment(tarikh.date), "day");
          if (isTodayTarikh || isTomorrowTarikh) {
            let text;
            if (isTodayTarikh) {
              text = `Today Case Id - ${tarikh._client.caseId}, Start Time - ${
                tarikh.startTime
              }, End Time - ${tarikh.endTime}, Advocate - ${
                tarikh._creator.fullName
              }, Contact No - ${tarikh._creator.mobile} `;
            } else {
              text = `Tomorrow Case Id - ${
                tarikh._client.caseId
              }, Start Time - ${tarikh.startTime}, End Time - ${
                tarikh.endTime
              }, Advocate - ${tarikh._creator.fullName}, Contact No - ${
                tarikh._creator.mobile
              } `;
            }
            setTimeout(() => {
              axios
                .get(
                  `http://jumbosms.shlrtechnosoft.com/websms/sendsms.aspx?userid=mayankmodi&password=mayank@051&sender=MANGLA&mobileno=${
                    tarikh._client.mobile
                  }&msg=${text}`
                )
                .then(response => {
                  const responseMessage = response.data;
                  if (responseMessage.toLowerCase().indexOf("success") !== -1) {
                    //200 status
                    new Message({
                      text,
                      mobile: tarikh._client.mobile,
                      createdAt: moment().valueOf(),
                      _client: tarikh._client._id,
                      _creator: tarikh._creator._id
                    }).save();
                  }
                  // 400 status
                });
            }, 5000);
          }
        }
      });
    });
});
