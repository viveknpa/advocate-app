const multer = require('multer');
const path = require('path');

//set the storage Engine
const storage = multer.diskStorage({
    destination: path.join(__dirname, '..', '..', 'public', 'files'),
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    }
})

//init upload
const uploadImage = multer({
    storage,
    limits: { fileSize: 3000000 },
    fileFilter: function (req, file, cb) {
        //allowed ext
        const filetypes = /jpeg|jpg|png|gif/;
        //check ext
        const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
        //check mime
        const mimetype = filetypes.test(file.mimetype);

        if (mimetype && extname) {
            return cb(null, true);
        } else {
            cb('Error: Images Only!');
        }
    }
}).single('image');

//init upload
const uploadFile = multer({
    storage,
    limits: { fileSize: 3000000 }
}).single('file');

module.exports = {uploadImage, uploadFile};