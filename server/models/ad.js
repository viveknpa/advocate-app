var mongoose = require('mongoose');

var AdsSchema = new mongoose.Schema({
    title: String,
    description: String,
    fileName: String,
    approve: { type: Boolean, default: false },
    active: { type: Boolean, default: false },
    duration: { type: Number },
    _creator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
});

var Ad = mongoose.model('Ad', AdsSchema);

module.exports = { Ad };
