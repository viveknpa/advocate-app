var mongoose = require('mongoose');

var AppFeatureSchema = new mongoose.Schema({
    title: { type: String, required: true, minlength: 1 },
    description: String,
    usesTitle: String,
    usesPoints: [{
        text: String,
        _id: { type: Number, required: true }
    }]
})

var AppFeature = mongoose.model('AppFeature', AppFeatureSchema);

module.exports = { AppFeature };
