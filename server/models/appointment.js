var mongoose = require('mongoose');

var AppointmentSchema = new mongoose.Schema({
    description: String,
    date: Number,
    startTime: String,
    endTime: String,
    appointmentTo: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' },
    _creator: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' }  
});

var Appointment = mongoose.model('Appointment', AppointmentSchema);

module.exports = { Appointment };
