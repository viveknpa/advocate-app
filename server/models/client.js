var mongoose = require("mongoose");

var ClientSchema = new mongoose.Schema({
  fullName: { type: String, required: true, minlength: 1 },
  fatherName: { type: String, required: true, minlength: 1 },
  mobile: Number,
  altMobile: Number,
  address: String,
  antiClient_fullName: { type: String, required: true, minlength: 1 },
  antiClient_fatherName: String,
  antiClient_mobile: Number,
  antiClient_altMobile: Number,
  antiClient_address: String,
  assignTo: { type: mongoose.Schema.Types.ObjectId, ref: "Employee" },
  subject: String,
  caseId: { type: Number, required: true },
  caseNo: String,
  caseClosed: { type: Boolean, default: false },
  _creator: mongoose.Schema.Types.ObjectId,
  sentMessages: { type: Boolean, default: false }
});

var Client = mongoose.model("Client", ClientSchema);

module.exports = { Client };
