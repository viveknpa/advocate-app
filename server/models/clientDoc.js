var mongoose = require('mongoose');

var ClientDocSchema = new mongoose.Schema({
    url: {type: String, required: true},
    name: {type: String, required: true},
    createdAt: { type: Number, required: true },
    _client: { type: mongoose.Schema.Types.ObjectId, required: true },
    _creator: { type: mongoose.Schema.Types.ObjectId, required: true },
});

var ClientDoc = mongoose.model('ClientDoc', ClientDocSchema);

module.exports = { ClientDoc };
