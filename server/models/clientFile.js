var mongoose = require('mongoose');

var ClientFileSchema = new mongoose.Schema({
    name: {type: String, required: true},
    text: String,
    createdAt: { type: Number, required: true },
    _client: { type: mongoose.Schema.Types.ObjectId, required: true },
    _creator: { type: mongoose.Schema.Types.ObjectId, required: true }
});

var ClientFile = mongoose.model('ClientFile', ClientFileSchema);

module.exports = { ClientFile };
