var mongoose = require('mongoose');
var validator = require('validator');

var EmployeeSchema = new mongoose.Schema({
    firstName: { type: String, required: true, minlength: 1 },
    lastName: { type: String, required: true, minlength: 1 },
    email: {
        type: String,
        required: true,
        trim: true,
        minlength: 1,
        unique: true,
        validate: {
          validator: validator.isEmail,
          message: `{VALUE} is not a valid email`
        }
    },
    mobile: {type: Number, required: true},
    altMobile: Number,
    address: String,
    _creator: mongoose.Schema.Types.ObjectId
});

var Employee = mongoose.model('Employee', EmployeeSchema);

module.exports = { Employee };
