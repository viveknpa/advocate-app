var mongoose = require('mongoose');

var EventSchema = new mongoose.Schema({
    title: String,
    description: String,
    start: String,
    end: String,
    court: String,
    state: String,
    city: String,
    _creator: mongoose.Schema.Types.ObjectId
});

var Event = mongoose.model('Event', EventSchema);

module.exports = { Event };
