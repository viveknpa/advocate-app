var mongoose = require('mongoose');

var MessageSchema = new mongoose.Schema({
    text: {type: String, required: true, minlength: 1},
    mobile: Number,
    createdAt: Number,
    _client: mongoose.Schema.Types.ObjectId,
    _creator: mongoose.Schema.Types.ObjectId
});

var Message = mongoose.model('Message', MessageSchema);

module.exports = { Message };
