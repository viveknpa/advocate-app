var mongoose = require('mongoose');

var PassbookSchema = new mongoose.Schema({
    balance: { type: Number, default: 0 },
    _client: { type: mongoose.Schema.Types.ObjectId, required: true },
    _creator: {type: mongoose.Schema.Types.ObjectId, required: true}
})

var Passbook = mongoose.model('Passbook', PassbookSchema);

module.exports = { Passbook };