var mongoose = require('mongoose');

var ProfileSchema = new mongoose.Schema({
    fullName: String,
    dpUrl: { type: String },
    bio: {
        heading: { type: String, default: 'Working at XYZ' },
        state: { type: String, default: 'Delhi' },
        city: { type: String, default: 'North' },
    },
    experience: [{
        title: String,
        company: String,
        location: String,
        start: {
            month: String,
            year: Number
        },
        end: {
            month: String,
            year: Number
        },
        description: String
    }],
    education: [{
        school: String,
        degree: String,
        field: String,
        startYear: Number,
        endYear: Number,
        grade: String
    }],
    award: [{
        title: String,
        issuer: String,
        month: String,
        year: Number,
        description: String
    }],
    contactInfo: {
        mobile: String,
        officeAddress: String,
        website: String,
        birthday: String
    },
    _creator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
});

var Profile = mongoose.model('Profile', ProfileSchema);

module.exports = { Profile };
