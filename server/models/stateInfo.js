var mongoose = require('mongoose');

var StateInfoSchema = new mongoose.Schema({
    state: String,
    cities: [String]
});

var StateInfo = mongoose.model('StateInfo', StateInfoSchema);

module.exports = { StateInfo };
