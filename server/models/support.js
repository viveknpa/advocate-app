var mongoose = require('mongoose');

var SupportSchema = new mongoose.Schema({
    category: { type: String, required: true, trim: true, minlength: 1 },
    query: { type: String, required: true, trim: true, minlength: 1 },
    queriedAt: Number,
    reply: [{
        repliedAt: Number,
        _repliedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        text: String
    }],
    _creator: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
});

var Support = mongoose.model('Support', SupportSchema);

module.exports = { Support };
