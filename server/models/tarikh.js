var mongoose = require('mongoose');

var TarikhSchema = new mongoose.Schema({
    reason: { type: String, required: true },
    conclusion: String,
    date: Number,
    startTime: String,
    endTime: String,
    appeared: Boolean,
    _client: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'Client' },
    _creator: { type: mongoose.Schema.Types.ObjectId, required: true, ref: 'User' }
});

var Tarikh = mongoose.model('Tarikh', TarikhSchema);

module.exports = { Tarikh };
