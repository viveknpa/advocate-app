var mongoose = require('mongoose');

var TemplateFileSchema = new mongoose.Schema({
    name: {type: String, required: true},
    text: String,
    createdAt: { type: Number, required: true },
    _creator: { type: mongoose.Schema.Types.ObjectId, required: true }
});

var TemplateFile = mongoose.model('TemplateFile', TemplateFileSchema);

module.exports = { TemplateFile };
