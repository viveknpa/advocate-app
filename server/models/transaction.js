var mongoose = require('mongoose');

var TransactionSchema = new mongoose.Schema({
    action: String, //debit or credit
    amount: Number,
    reason: String,
    balance: { type: Number, default: 0 },
    createdAt: Number,
    _client: { type: mongoose.Schema.Types.ObjectId, ref: 'Client' },
    _creator: mongoose.Schema.Types.ObjectId
})

var Transaction = mongoose.model('Transaction', TransactionSchema);

module.exports = { Transaction };
