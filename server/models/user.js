var mongoose = require("mongoose");
var validator = require("validator");
const jwt = require("jsonwebtoken");
const _ = require("lodash");
var bcrypt = require("bcryptjs");

var UserSchema = new mongoose.Schema(
  {
    fullName: { type: String, required: true, trim: true, minlength: 1 },
    mobile: Number,
    email: {
      type: String,
      required: true,
      trim: true,
      minlength: 1,
      unique: true,
      validate: {
        validator: validator.isEmail,
        message: `{VALUE} is not a valid email`
      }
    },
    userType: {
      type: Number,
      default: 16
    },
    profileStatus: { type: Number, default: 0 }, //0 for not updated, 1 for updated! , for advocate(basic) only
    loginDetail: [Number],
    password: {
      type: String,
      required: true,
      minlength: 6
    },
    secretToken: String,
    tokens: [
      {
        access: {
          type: String,
          required: true
        },
        token: {
          type: String,
          required: true
        }
      }
    ],
    _creator: mongoose.Schema.Types.ObjectId,
    active: { type: Boolean, default: false }, //will be activated by email validation after signUp
    accountExpiresOn: Number,
    sentMessages: { type: Boolean, default: false }, //will be activated by admin
    accountLocked: { type: Boolean, default: false } //admin can lock an account
  },
  { usePushEach: true }
);

//overwriting the inbuilt instance method toJSON()
UserSchema.methods.toJSON = function() {
  var user = this;
  var userObject = user.toObject(); //toObject() will convert user to regular object where only properties exist

  return _.pick(userObject, [
    "_id",
    "fullName",
    "email",
    "mobile",
    "active",
    "userType",
    "_creator",
    "dpUrl",
    "loginDetail",
    "profileStatus",
    "accountExpiresOn",
    "sentMessages",
    "accountLocked"
  ]);
};

UserSchema.methods.generateAuthToken = function() {
  var user = this;
  var access = "auth";
  var token = jwt
    .sign({ _id: user._id.toHexString(), access }, "nvhf@#i1234ndhbhb")
    .toString();

  user.tokens.push({ access, token });

  return user.save().then(() => {
    return token;
  });
};

UserSchema.methods.removeToken = function(token) {
  var user = this;

  return user.update({
    $pull: {
      tokens: { token }
    }
  });
};

UserSchema.statics.findByToken = function(token) {
  var User = this;
  var decoded;

  try {
    decoded = jwt.verify(token, "nvhf@#i1234ndhbhb");
  } catch (e) {
    // return new Promise((resolve, reject) => {
    //   reject();
    // });
    return Promise.reject();
  }

  return User.findOne({
    _id: decoded._id,
    "tokens.token": token,
    "tokens.access": "auth"
  });
};

UserSchema.statics.findByCredentials = function(email, password) {
  var User = this;

  return User.findOne({ email }).then(user => {
    if (!user) {
      return Promise.reject();
    }

    return new Promise((resolve, reject) => {
      bcrypt.compare(password, user.password, (err, res) => {
        if (res) {
          resolve(user);
        } else {
          reject();
        }
      });
    });
  });
};

UserSchema.pre("save", function(next) {
  var user = this;

  if (user.isModified("password")) {
    bcrypt.genSalt(10, (err, salt) => {
      bcrypt.hash(user.password, salt, (err, hash) => {
        user.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});

var User = mongoose.model("User", UserSchema);

module.exports = { User };
