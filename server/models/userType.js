var mongoose = require('mongoose');

var UserTypeSchema = new mongoose.Schema({ 
    type: { type: Number, required: true },  
    description: String, 
    name: String,
    authority: {}
})

var UserType = mongoose.model('UserType', UserTypeSchema);

module.exports = {UserType};