const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
const { Ad } = require("../models/ad");
const { uploadFile } = require("../misc/multer");
require("../misc/cron");
module.exports = app => {
  //get ads
  app.get("/advertise", authenticate, (req, res) => {
    Ad.find()
      .populate("_creator", "fullName")
      .then(ads => {
        res.send({ ads });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });

  //post advertisement
  app.post("/advertise", authenticate, (req, res) => {
    uploadFile(req, res, err => {
      if (err) {
        return res.status(400).send(err);
        console.log(err);
      } else {
        const body = _.pick(req.body, ["title", "description"]);
        body._creator = req.user._id;
        if (req.file) {
          body.fileName = req.file.filename;
        }
        new Ad(body).save().then(
          ad => {
            res.send({ ad });
          },
          err => {
            res.status(400).send(err);
            console.log(err);
          }
        );
      }
    });
  });

  //update ad by admin
  app.post("/advertise/:id", authenticate, (req, res) => {
    const id = req.params.id;
    const body = _.pick(req.body, ["approve", "active", "duration"]);
    Ad.findByIdAndUpdate(id, { $set: body }, { new: true })
      .then(ad => {
        res.send({ ad });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });
};
