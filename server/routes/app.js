var { ObjectID } = require("mongodb");
const { AppFeature } = require("../models/appFeature");
const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
module.exports = app => {
  //app feature
  app.get("/app/feature", authenticate, (req, res) => {
    AppFeature.find()
      .then(features => {
        res.send({ features });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });

  // add new app feature
  app.post("/app/feature", authenticate, (req, res) => {
    const body = _.pick(req.body, [
      "title",
      "description",
      "usesTitle",
      "usesPoints"
    ]);
    console.log(body);
    new AppFeature(body)
      .save()
      .then(feature => {
        res.send({ feature });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });

  //edit app feature
  app.patch("/app/feature/:id", authenticate, (req, res) => {
    const id = req.params.id;
    if (!ObjectID.isValid(id)) {
      return res.status(404).send();
    }
    const body = _.pick(req.body, [
      "title",
      "description",
      "usesTitle",
      "usesPoints"
    ]);
    AppFeature.findByIdAndUpdate(id, { $set: body }, { new: true })
      .then(feature => {
        if (!feature) {
          res.status(404).send();
        }
        res.send({ feature });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });

  app.delete("/app/feature/:id", authenticate, (req, res) => {
    const id = req.params.id;
    if (!ObjectID.isValid(id)) {
      return res.status(404).send();
    }
    AppFeature.findByIdAndRemove(id)
      .then(feature => {
        if (!feature) {
          res.status(404).send();
        }
        res.status(200).send();
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });
};
