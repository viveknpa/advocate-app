const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
const { Appointment } = require("../models/appointment");
module.exports = app => {
  //get all appointments
  app.get("/appointment", authenticate, (req, res) => {
    Appointment.find({ appointmentTo: req.user._id })
      .populate([
        {
          path: "_creator",
          select: "fullName mobile"
        }
      ])
      .then(appointments => {
        res.send({ appointments });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });

  //book an appointment
  app.post("/appointment/:appointmentTo", authenticate, (req, res) => {
    const appointmentTo = req.params.appointmentTo;
    const body = _.pick(req.body, [
      "date",
      "startTime",
      "endTime",
      "description"
    ]);
    body.appointmentTo = appointmentTo;
    body._creator = req.user._id;
    new Appointment(body)
      .save()
      .then(appointment => {
        res.send({ appointment });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });
};
