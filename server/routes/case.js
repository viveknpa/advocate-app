const { Client } = require("../models/client");
const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
module.exports = app => {
  //Case search

  function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
  }

  app.get("/case/search/:searchBy/:value", authenticate, (req, res) => {
    const searchBy = req.params.searchBy;
    const value = req.params.value;
    const regex = new RegExp(escapeRegex(value), "gi");
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    const body = { _creator };
    if (searchBy === "fullName") {
      body[searchBy] = regex;
    } else {
      body[searchBy] = value;
    }
    Client.find(body)
      .then(clients => {
        res.send({ clients });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });
};
