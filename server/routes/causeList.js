const { Tarikh } = require("../models/tarikh");
const { authenticate } = require("../middleware/authenticate");
const moment = require("moment");
const _ = require("lodash");
module.exports = app => {
  app.post("/causeList", authenticate, (req, res) => {
    const date = req.body.date;
    Tarikh.find({ _creator: req.user._id })
      .where("date")
      .gte(
        moment(date)
          .startOf("day")
          .valueOf()
      )
      .lt(
        moment(date)
          .endOf("day")
          .valueOf()
      )
      .populate("_client")
      .exec()
      .then(docs => {
        const allPromises = [];
        const data = {};
        const causeList = [];
        docs.forEach(doc => {
          const clientId = doc._client._id.toString();
          data[clientId] = {};
          data[clientId]._client = doc._client;
          const next = Tarikh.find({
            date: { $gt: doc.date },
            _client: doc._client._id
          })
            .sort({ date: 1 })
            .limit(1)
            .exec()
            .then(next => {
              if (next.length) {
                data[clientId].nextDate = {
                  date: next[0].date,
                  reason: next[0].reason,
                  conclusion: next[0].conclusion
                };
              }
            });
          const prev = Tarikh.find({
            date: { $lt: doc.date },
            _client: doc._client._id
          })
            .sort({ date: -1 })
            .limit(1)
            .exec()
            .then(prev => {
              if (prev.length) {
                data[clientId].lastDate = {
                  date: prev[0].date,
                  reason: prev[0].reason,
                  conclusion: prev[0].conclusion
                };
              }
            });

          allPromises.push(next);
          allPromises.push(prev);
        });
        Promise.all(allPromises).then(() => {
          Object.keys(data).forEach(clientId => {
            causeList.push(data[clientId]);
          });
          res.send({ causeList });
        });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });
};
