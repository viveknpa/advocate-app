const { Client } = require("../models/client");
const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
module.exports = app => {
  //Register client
  app.post("/client", authenticate, (req, res) => {
    const body = _.pick(req.body, [
      "fullName",
      "fatherName",
      "address",
      "mobile",
      "altMobile",
      "antiClient_fullName",
      "antiClient_fatherName",
      "antiClient_address",
      "antiClient_mobile",
      "antiClient_altMobile",
      "subject"
    ]);
    body._creator = req.user._id;

    Client.find()
      .distinct("caseId")
      .then(caseIds => {
        let newCaseId;
        if (caseIds.length) {
          newCaseId = Math.max(...caseIds) + 1;
        } else {
          newCaseId = 1253;
        }
        body.caseId = newCaseId;
        return new Client(body).save().then(client => {
          res.send({ client });
          return new Passbook({
            _client: client._id,
            _creator: req.user._id
          }).save();
        });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //client doc update
  app.patch("/client/:id", authenticate, (req, res) => {
    const id = req.params.id;
    const body = _.pick(req.body, [
      "fullName",
      "fatherName",
      "address",
      "mobile",
      "altMobile",
      "antiClient_fullName",
      "antiClient_fatherName",
      "antiClient_address",
      "antiClient_mobile",
      "antiClient_altMobile",
      "subject",
      "caseNo",
      "caseClosed",
      "sentMessages"
    ]);
    Client.findOneAndUpdate(
      { _id: id, _creator: req.user._id },
      { $set: body },
      { new: true }
    )
      .populate("assignTo")
      .then(client => {
        if (!client) {
          return res.status(404).send();
        }
        res.send({ client });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //assign client case to employee
  app.patch("/client/assign/:id", authenticate, (req, res) => {
    const id = req.params.id;
    const { assignTo } = req.body;
    Client.findOneAndUpdate(
      { _id: id, _creator: req.user._id },
      { $set: { assignTo } },
      { new: true }
    )
      .populate("assignTo")
      .then(client => {
        if (!client) {
          res.status(404).send();
        }
        res.send({ client });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //list of all clients
  app.get("/client", authenticate, (req, res) => {
    Client.find({ _creator: req.user._id })
      .populate("assignTo")
      .then(clients => {
        res.send({ clients });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });
};
