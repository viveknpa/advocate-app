const { ClientDoc } = require("../models/clientDoc");
const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
const { uploadFile } = require("../misc/multer");
module.exports = app => {
  //get client docs
  app.get("/clientdoc/:clientId", authenticate, (req, res) => {
    const _client = req.params.clientId;
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    ClientDoc.find({ _creator, _client })
      .then(docs => {
        res.send({ docs });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });

  //add new client docs
  app.post("/clientdoc/:clientId", authenticate, (req, res) => {
    const _client = req.params.clientId;
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    uploadFile(req, res, err => {
      if (err) {
        res.status(400).send(err);
      } else {
        if (req.file == undefined) {
          res.status(400).send("Error: No File Selected!");
        } else {
          const body = { name: req.body.name };
          body.url = `${req.file.filename}`;
          body.createdAt = moment().valueOf();
          body._client = _client;
          body._creator = _creator;
          new ClientDoc(body)
            .save()
            .then(doc => {
              res.send({ doc });
            })
            .catch(e => {
              console.log(e);
              res.status(400).send();
            });
        }
      }
    });
  });

  //delete client doc
  app.delete("/clientdoc/:id", authenticate, (req, res) => {
    const _id = req.params.id;
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    ClientDoc.findOneAndRemove({ _id, _creator })
      .then(doc => {
        if (!doc) {
          return res.status(404).send();
        }
        res.send({ doc });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });
};
