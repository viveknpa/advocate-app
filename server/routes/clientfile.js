const { ClientFile } = require("../models/clientFile");
const _ = require("lodash");
const { authenticate } = require("../middleware/authenticate");
var { ObjectID } = require("mongodb");
var moment = require("moment");
module.exports = app => {
  //get case files of a client
  app.get("/clientfile/:clientId", authenticate, (req, res) => {
    const _client = req.params.clientId;
    if (!ObjectID.isValid(_client)) {
      return res.status(404).send();
    }
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    ClientFile.find({ _creator, _client })
      .then(files => {
        res.send({ files });
      })
      .catch(e => {
        res.status(400).send();
      });
  });

  //create new case file
  app.post("/clientfile/:clientId", authenticate, (req, res) => {
    const _client = req.params.clientId;
    if (!ObjectID.isValid(_client)) {
      return res.status(404).send();
    }
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    const body = _.pick(req.body, ["name", "text"]);
    body.createdAt = moment().valueOf();
    body._client = _client;
    body._creator = _creator;
    new ClientFile(body)
      .save()
      .then(file => {
        res.send({ file });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //edit a client case file
  app.patch("/clientfile/:id", authenticate, (req, res) => {
    const _id = req.params.id;
    if (!ObjectID.isValid(_id)) {
      return res.status(404).send();
    }
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    const body = _.pick(req.body, ["name", "text"]);
    ClientFile.findOneAndUpdate(
      { _id, _creator },
      { $set: body },
      { new: true }
    )
      .then(file => {
        if (!file) {
          return res.status(404).send();
        }
        res.send({ file });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });

  //delete case file
  app.delete("/clientfile/:id", authenticate, (req, res) => {
    const _id = req.params.id;
    if (!ObjectID.isValid(_id)) {
      return res.status(404).send();
    }
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    ClientFile.findOneAndRemove({ _creator, _id })
      .then(file => {
        if (!file) {
          return res.status(404).send();
        }
        res.send({ file });
      })
      .catch(e => {
        res.status(400).send();
      });
  });
};
