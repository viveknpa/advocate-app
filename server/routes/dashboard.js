const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
const { Passbook } = require("../models/passbook");
var { ObjectID } = require("mongodb");
const { Tarikh } = require("../models/tarikh");
const { Client } = require("../models/client");
const { Employee } = require("../models/employee");

const moment = require("moment");
const { ClientDoc } = require("../models/clientDoc");

const { Message } = require("../models/message");

module.exports = app => {
  //advocate dashboard
  app.get("/dashboard/advocate", authenticate, (req, res) => {
    const userId = req.user._id;

    const dashboardData = {};
    const names = Client.find({ _creator: userId })
      .select("fullName")
      .then(names => {
        dashboardData.clientCount = names.length;
        dashboardData.names = names;
      });
    const totalAssociates = Employee.find({ _creator: req.user._id })
      .count()
      .then(count => {
        dashboardData.totalAssociates = count;
      });
    const tarikhs = Tarikh.find({ _creator: userId })
      .select("date appeared reason")
      .then(tarikhs => {
        dashboardData.tarikhs = tarikhs;
        dashboardData.tarikhsCount = tarikhs.length;
        let tarikhsToday = [],
          tarikhsWeek = [],
          tarikhsMonth = [],
          tarikhsYear = [];
        tarikhs.forEach(tarikh => {
          if (moment(tarikh.date).isAfter(moment(), "day")) {
            if (moment().isSame(moment(tarikh.date), "day"))
              tarikhsToday.push(tarikh);
            if (moment().isSame(moment(tarikh.date), "week"))
              tarikhsWeek.push(tarikh);
            if (moment().isSame(moment(tarikh.date), "month"))
              tarikhsMonth.push(tarikh);
            if (moment().isSame(moment(tarikh.date), "year"))
              tarikhsYear.push(tarikh);
          }
        });
        dashboardData.tarikhsToday = tarikhsToday;
        dashboardData.tarikhsWeek = tarikhsWeek;
        dashboardData.tarikhsMonth = tarikhsMonth;
        dashboardData.tarikhsYear = tarikhsYear;
      });
    Promise.all([names, tarikhs, totalAssociates])
      .then(() => {
        res.send({ dashboardData });
      })
      .catch(e => console.log(e));
  });

  //client dashboard
  app.get("/dashboard/client/:id", authenticate, (req, res) => {
    const id = req.params.id;
    const userId = req.user._id;
    if (!ObjectID.isValid(id)) {
      return res.status(404).send();
    }
    const dashboardData = {};
    const files = ClientFile.find({ _creator: userId, _client: id })
      .select("name")
      .then(files => {
        dashboardData.files = files;
      });
    const docs = ClientDoc.find({ _creator: userId, _client: id })
      .select("name url")
      .then(docs => {
        dashboardData.docs = docs;
      });
    const tarikhs = Tarikh.find({ _creator: userId, _client: id })
      .select("date appeared reason")
      .then(tarikhs => {
        dashboardData.tarikhs = tarikhs;
        dashboardData.tarikhsCount = tarikhs.length;
        let appearedCount = 0,
          notAppearedCount = 0,
          tarikhsToday = [],
          tarikhsWeek = [],
          tarikhsMonth = [],
          tarikhsYear = [];
        tarikhs.forEach(tarikh => {
          if (moment(tarikh.date).isAfter(moment(), "day")) {
            if (moment().isSame(moment(tarikh.date), "day"))
              tarikhsToday.push(tarikh);
            if (moment().isSame(moment(tarikh.date), "week"))
              tarikhsWeek.push(tarikh);
            if (moment().isSame(moment(tarikh.date), "month"))
              tarikhsMonth.push(tarikh);
            if (moment().isSame(moment(tarikh.date), "year"))
              tarikhsYear.push(tarikh);
          }
          if (tarikh.date < moment().valueOf()) {
            if (tarikh.appeared === true) {
              appearedCount += 1;
            } else if (tarikh.appeared === false) {
              notAppearedCount += 1;
            }
          }
        });
        dashboardData.tarikhsToday = tarikhsToday;
        dashboardData.tarikhsWeek = tarikhsWeek;
        dashboardData.tarikhsMonth = tarikhsMonth;
        dashboardData.tarikhsYear = tarikhsYear;
        dashboardData.appearedCount = appearedCount;
        dashboardData.notAppearedCount = notAppearedCount;
      });
    const passbook = Passbook.find({ _creator: userId, _client: id })
      .select("balance")
      .then(passbook => {
        dashboardData.balance = passbook.balance;
      });
    const message = Message.find({ _creator: userId, _client: id })
      .count()
      .then(count => {
        dashboardData.messageCount = count;
      });
    Promise.all([files, docs, tarikhs, passbook, message])
      .then(() => {
        res.send({ dashboardData });
      })
      .catch(e => console.log(e));
  });

  //tarikh by year
  app.get("/dashboard/advocate/tarikhs/:year", authenticate, (req, res) => {
    const { year } = req.params;
    const userId = req.user._id;

    var tarikhPromise;
    const countTarikh = [];
    tarikhPromise = Tarikh.find({ _creator: userId })
      .select("date")
      .then(tarikh => {
        for (let i = 1; i <= 12; i++) {
          let tarikhs = tarikh.filter(t => {
            return (
              moment(t.date).format("YYYY") == year &&
              moment(t.date).format("M") == i
            );
          });
          countTarikh.push(tarikhs.length);
        }
      })
      .catch(e => console.log(e));

    Promise.all([tarikhPromise])
      .then(() => {
        res.send({ countTarikh });
      })
      .catch(e => console.log(e));
  });

  //tarikhs by months
  app.get(
    "/dashboard/advocate/tarikhs/:year/:month",
    authenticate,
    (req, res) => {
      const { year, month } = req.params;
      const userId = req.user._id;

      var tarikhPromise;
      const countWeekTarikh = [0, 0, 0, 0, 0];
      tarikhPromise = Tarikh.find({ _creator: userId })
        .select("date")
        .then(tarikh => {
          let tarikhs = tarikh.filter(
            t =>
              moment(t.date).format("YYYY") == year &&
              moment(t.date).format("M") == month
          );
          tarikhs.forEach(t1 => {
            if (moment(t1.date).format("D") <= 7) ++countWeekTarikh[0];
            else if (moment(t1.date).format("D") <= 14) ++countWeekTarikh[1];
            else if (moment(t1.date).format("D") <= 21) ++countWeekTarikh[2];
            else if (moment(t1.date).format("D") <= 28) ++countWeekTarikh[3];
            else if (moment(t1.date).format("D") > 28) ++countWeekTarikh[4];
          });
        })
        .catch(e => console.log(e));

      Promise.all([tarikhPromise])
        .then(() => {
          res.send({ countWeekTarikh });
        })
        .catch(e => console.log(e));
    }
  );
};
