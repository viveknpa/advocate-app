const { Employee } = require("../models/employee");
const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
module.exports = app => {
  //Register employee
  app.post("/employee", authenticate, (req, res) => {
    const body = _.pick(req.body, [
      "firstName",
      "lastName",
      "email",
      "mobile",
      "altMobile",
      "address",
      "password"
    ]);
    body._creator = req.user._id;
    new Employee(body)
      .save()
      .then(employee => {
        new User({
          fullName: `${body.firstName} ${body.lastName}`,
          email: body.email,
          mobile: body.mobile,
          password: body.password,
          active: true,
          _creator: req.user._id,
          userType: 13
        }).save();
        res.send({ employee });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });

  //employee doc update
  app.patch("/employee/:id", authenticate, (req, res) => {
    const id = req.params.id;
    const body = _.pick(req.body, [
      "firstName",
      "lastName",
      "email",
      "mobile",
      "altMobile",
      "address"
    ]);
    Employee.findOneAndUpdate(
      {
        _id: id,
        _creator: req.user._id
      },
      {
        $set: body
      },
      { new: true }
    )
      .then(employee => {
        if (!employee) {
          return res.status(404).send();
        }
        res.send({ employee });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //get employees list
  app.get("/employee", authenticate, (req, res) => {
    Employee.find({ _creator: req.user._id })
      .then(employees => {
        res.send({ employees });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });
};
