const { Event } = require("../models/event");
const { authenticate } = require("../middleware/authenticate");

module.exports = app => {
  //get events list
  app.get("/event/:court?", authenticate, (req, res) => {
    const court = req.params.court;
    const query = court ? { court } : { _creator: req.user._id };
    Event.find(query)
      .then(events => {
        res.send({ events });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //add event on calender
  app.post("/event/:court?", authenticate, (req, res) => {
    const { title, description, start, end, state, city } = req.body;
    const court = req.params.court;
    const query = court
      ? { title, description, start, end, state, city, court }
      : { title, description, start, end, _creator: req.user._id };
    new Event(query)
      .save()
      .then(newEvent => {
        res.send({ newEvent });
      })
      .catch(e => {
        res.status(400).send(e);
        console.log(e);
      });
  });
};
