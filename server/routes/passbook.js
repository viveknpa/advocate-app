const { authenticate } = require("../middleware/authenticate");
const { Passbook } = require("../models/passbook");
const { Transaction } = require("../models/transaction");
var { ObjectID } = require("mongodb");
const _ = require("lodash");
module.exports = app => {
  //get a client passbook
  app.get("/passbook/:clientId", authenticate, (req, res) => {
    const _client = req.params.clientId;
    if (!ObjectID.isValid(_client)) {
      return res.status(404).send();
    }
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    Passbook.findOne({ _creator, _client })
      .then(passbook => {
        if (!passbook) {
          return res.status(404).send();
        }
        return Transaction.find({ _creator, _client })
          .select({
            action: 1,
            amount: 1,
            reason: 1,
            balance: 1,
            _id: 1,
            createdAt: 1
          })
          .then(transactions => {
            passbook = passbook.toObject();
            passbook.transactions = transactions;
            return passbook;
          });
      })
      .then(passbook => {
        res.send({ passbook });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //Delete transaction of passbook
  app.delete("/passbook/:id", authenticate, (req, res) => {
    const _id = req.params.id;
    Transaction.findOneAndRemove({ _id, _creator: req.user._id })
      .then(doc => {
        if (!doc) {
          return res.status(404).send();
        }
        res.send({ doc });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });

  //new transaction update to passbook
  app.patch("/passbook/:clientId", authenticate, (req, res) => {
    const _client = req.params.clientId;
    if (!ObjectID.isValid(_client)) {
      return res.status(404).send();
    }
    const { action, amount, reason, balance } = req.body;
    const _creator = req.user._creator ? req.user._creator : req.user._id; //account holder is either parent or sub advocate
    Passbook.findOneAndUpdate(
      { _client, _creator },
      { $set: { balance: balance } },
      { new: true }
    )
      .then(passbook => {
        if (!passbook) {
          return res.status(404).send();
        }
        new Transaction({
          action,
          amount,
          reason,
          balance,
          _creator,
          _client: _client,
          createdAt: moment().valueOf()
        }).save();
        res.send({ currentBalance: passbook.balance });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //get advocate passbook
  app.get("/passbook-advocate", authenticate, (req, res) => {
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    Transaction.find({ _creator })
      .populate("_client")
      .then(transactions => {
        res.send({ passbook: transactions });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });
};
