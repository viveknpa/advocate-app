const { Profile } = require("../models/profile");
const { authenticate } = require("../middleware/authenticate");
const { uploadImage } = require("../misc/multer");
module.exports = app => {
  //profile search
  app.get("/profile/search/:name", (req, res) => {
    const regex = new RegExp(escapeRegex(req.params.name), "gi");
    Profile.find({ fullName: regex })
      .select({ bio: 1, fullName: 1, dpUrl: 1, _creator: 1 })
      .then(profiles => {
        res.send({ profiles });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send(e);
      });
  });

  //get profile informations
  app.get("/profile/:id", (req, res) => {
    const id = req.params.id;
    Profile.findOne({ _creator: id })
      .populate("_creator", "email")
      .then(profile => {
        if (!profile) {
          return res.status(404).send();
        }
        res.send({ profile });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send(e);
      });
  });

  //profile image update
  app.patch("/profile/image", authenticate, (req, res) => {
    uploadImage(req, res, err => {
      if (err) {
        console.log(err);
        return res.status(400).send(err);
      } else {
        if (!req.file.filename) {
          return res.status(400).send("Error: No file Selected!");
        }
        const _creator = req.user._id;
        const query = { _creator };
        const update = { $set: { _creator, dpUrl: req.file.filename } };
        const options = { upsert: true, new: true, setDefaultsOnInsert: true };

        Profile.findOneAndUpdate(query, update, options)
          .populate("_creator", "email")
          .then(profile => {
            res.send({ dpUrl: profile.dpUrl });
          })
          .catch(e => {
            res.status(400).send();
            console.log(e);
          });
      }
    });
  });

  //profile info update
  app.patch("/profile/:infoSection", authenticate, (req, res) => {
    const infoSection = req.params.infoSection;
    const updateData = {};
    updateData[infoSection] = req.body[infoSection];
    if (infoSection === "bio") {
      // sending fullName under bio section, so we need to separate it
      updateData.fullName = req.body[infoSection].fullName;
    }
    const _creator = req.user._id;
    const query = { _creator };
    const update = { $set: updateData };
    const options = { upsert: true, new: true, setDefaultsOnInsert: true };

    Profile.findOneAndUpdate(query, update, options)
      .populate("_creator", "email")
      .then(profile => {
        if (infoSection === "bio" && req.user.fullName !== profile.fullName) {
          User.findByIdAndUpdate(_creator, {
            $set: { fullName: profile.fullName }
          }).catch(e => console.log(e, "error: update users doc"));
        }
        if (
          infoSection === "contactInfo" &&
          req.user.mobile !== profile.contactInfo.mobile
        ) {
          User.findByIdAndUpdate(_creator, {
            $set: { mobile: profile.contactInfo.mobile }
          }).catch(e => console.log(e, "error: update users doc"));
        }
        res.send({ profile });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });
};
