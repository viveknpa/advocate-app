const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
const { User } = require("../models/user");
module.exports = app => {
  //reset password
  app.post("/reset/:secretToken", (req, res) => {
    const secretToken = req.params.secretToken;
    User.findOne({ secretToken })
      .then(user => {
        if (!user) {
          return res.status(404).send();
        }
        user.password = req.body.password;
        user.secretToken = "";
        return user.save();
      })
      .then(() => {
        res.status(200).send();
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //reset password for authenticated user
  app.post("/reset", authenticate, (req, res) => {
    User.findOne({ _id: req.user._id })
      .then(user => {
        if (!user) {
          return res.status(404).send();
        }
        user.password = req.body.password;
        return user.save();
      })
      .then(() => {
        res.status(200).send();
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });
};
