const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
const { User } = require("../models/user");
var randomstring = require("randomstring");
const { sendVerifyMail } = require("../misc/sendgrid");
module.exports = app => {
  //forgot password || resend code
  app.post("/sendVerificationCode", (req, res) => {
    const { email, action } = req.body;
    User.findOne({ email })
      .then(user => {
        if (!user) {
          return res.status(404).send();
        }
        const secretToken = randomstring.generate();

        const to = req.body.email;

        let subject, text;
        if (action === "resend") {
          subject = "Verify your account on npaGuru";
          text = `copy and paste the following code: ${secretToken}`;
        } else if (action === "forgot") {
          subject = "reset password on npaGuru";
          text =
            "You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n" +
            "Please click on the following link, or paste this into your browser to complete the process:\n\n" +
            "https://advoart.com" +
            "/reset/" +
            secretToken +
            "\n\n" +
            "If you did not request this, please ignore this email and your password will remain unchanged.\n";
        }

        sendVerifyMail(to, subject, text);
        return user.update({
          $set: {
            secretToken
          }
        });
      })
      .then(() => {
        res.status(200).send();
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });
};
