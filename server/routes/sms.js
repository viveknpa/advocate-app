var { ObjectID } = require("mongodb");
const axios = require("axios");
const { Message } = require("../models/message");
const { authenticate } = require("../middleware/authenticate");
const moment = require("moment");
module.exports = app => {
  //send text sms to client
  app.post("/sms/:clientId", authenticate, (req, res) => {
    const _client = req.params.clientId;
    if (!ObjectID.isValid(_client)) {
      return res.status(404).send();
    }
    const { mobile, text } = req.body;
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    axios
      .get(
        `http://jumbosms.shlrtechnosoft.com/websms/sendsms.aspx?userid=mayankmodi&password=mayank@051&sender=MANGLA&mobileno=${mobile}&msg=${text}`
      )
      .then(response => {
        const responseMessage = response.data;
        if (responseMessage.toLowerCase().indexOf("success") !== -1) {
          new Message({
            mobile,
            text,
            _client,
            _creator,
            createdAt: moment().valueOf()
          }).save();
          return res.status(200).send();
        }
        res.status(400).send();
      });
  });

  //get all messages
  app.get("/sms/:clientId", authenticate, (req, res) => {
    const _client = req.params.clientId;
    if (!ObjectID.isValid(_client)) {
      return res.status(404).send();
    }
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    Message.find({ _client, _creator })
      .then(messages => {
        res.send({ messages });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });
};
