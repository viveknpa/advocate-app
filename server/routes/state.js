const _ = require("lodash");
const { StateInfo } = require("../models/stateInfo");
const { authenticate } = require("../middleware/authenticate");
module.exports = app => {
  //get list of all state infos
  app.get("/state", authenticate, (req, res) => {
    StateInfo.find()
      .then(infos => {
        res.send({ infos });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //add State informations
  app.post("/state", authenticate, (req, res) => {
    const body = _.pick(req.body, ["state", "cities"]);
    new StateInfo(body)
      .save()
      .then(info => {
        res.send({ info });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //update state infos
  app.patch("/state/:id", authenticate, (req, res) => {
    const id = req.params.id;
    const body = _.pick(req.body, ["state", "cities"]);
    StateInfo.findByIdAndUpdate(id, { $set: body }, { new: true })
      .then(info => {
        if (!info) {
          return res.status(404).send();
        }
        res.send({ info });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //delete state info
  app.delete("/state/:id", authenticate, (req, res) => {
    const id = req.params.id;

    StateInfo.findByIdAndRemove(id)
      .then(info => {
        if (!info) {
          return res.status(404).send();
        }
        res.send({ info });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });
};
