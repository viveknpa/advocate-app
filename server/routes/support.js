const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
const { Support } = require("../models/support");
module.exports = app => {
  //get all issues
  app.get("/support", authenticate, (req, res) => {
    Support.find()
      .populate([
        {
          path: "reply._repliedBy",
          select: "fullName mobile"
        },
        {
          path: "_creator",
          select: "fullName mobile"
        }
      ])
      .then(queries => {
        res.send({ queries });
      })
      .then(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //get all issues raised by individual user
  app.get("/support/me", authenticate, (req, res) => {
    Support.find({ _creator: req.user._id })
      .populate([
        {
          path: "reply._repliedBy",
          select: "fullName mobile"
        },
        {
          path: "_creator",
          select: "fullName mobile"
        }
      ])
      .then(queries => {
        res.send({ queries });
      })
      .then(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //post support
  app.post("/support", authenticate, (req, res) => {
    const { category, query } = req.body;
    const queriedAt = new Date().getTime();
    new Support({ category, query, queriedAt, _creator: req.user._id })
      .save()
      .then(query => {
        return Support.populate(query, {
          path: "_creator",
          select: "fullName"
        });
      })
      .then(query => {
        res.send({ query });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //reply to a query in support by admin or sub-admin
  app.patch("/support/reply/:id", authenticate, (req, res) => {
    const id = req.params.id;
    const body = _.pick(req.body, ["text"]);
    body._repliedBy = req.user._id;
    body.repliedAt = new Date().getTime();
    return Support.findByIdAndUpdate(
      id,
      { $push: { reply: body } },
      { new: true }
    )
      .populate([
        {
          path: "reply._repliedBy",
          select: "fullName mobile"
        },
        {
          path: "_creator",
          select: "fullName mobile"
        }
      ])
      .then(query => {
        res.send({ query });
      })
      .catch(e => {
        res.status(e => {
          res.status(400).send(e);
        });
      });
  });
};
