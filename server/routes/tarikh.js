const { Tarikh } = require("../models/tarikh");
const { authenticate } = require("../middleware/authenticate");
var { ObjectID } = require("mongodb");
const _ = require("lodash");
require("../misc/cron");
module.exports = app => {
  //list of all tarikhs(all clients)
  app.get("/tarikh", authenticate, (req, res) => {
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    Tarikh.find({ _creator })
      .populate("_client", "caseId")
      .then(tarikhs => {
        res.send({ tarikhs });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //list of all tarikhs of a client
  app.get("/tarikh/:clientId", authenticate, (req, res) => {
    const _client = req.params.clientId;
    if (!ObjectID.isValid(_client)) {
      return res.status(404).send();
    }
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    Tarikh.find({ _creator, _client })
      .then(tarikhs => {
        res.send({ tarikhs });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //create new tarikh
  app.post("/tarikh/:clientId", authenticate, (req, res) => {
    const _client = req.params.clientId;
    if (!ObjectID.isValid(_client)) {
      return res.status(404).send();
    }
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    const body = _.pick(req.body, ["reason", "date", "startTime", "endTime"]);
    body._creator = _creator;
    body._client = _client;
    new Tarikh(body)
      .save()
      .then(tarikh => {
        return Tarikh.populate(tarikh, { path: "_client", select: "caseId" });
      })
      .then(tarikh => {
        res.send({ tarikh });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //tarikh update (client appeared or not appeared)
  app.patch("/tarikh/:id", authenticate, (req, res) => {
    const id = req.params.id;
    if (!ObjectID.isValid(id)) {
      return res.status(404).send();
    }
    const body = _.pick(req.body, [
      "appeared",
      "conclusion",
      "startTime",
      "endTime",
      "reason",
      "date"
    ]);
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    Tarikh.findOneAndUpdate(
      { _id: id, _creator },
      { $set: body },
      { new: true }
    )
      .then(tarikh => {
        if (!tarikh) {
          return res.status(404).send();
        }
        res.send({ tarikh });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send(e);
      });
  });
};
