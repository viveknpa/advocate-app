const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
const { TemplateFile } = require("../models/templateFile");
const moment = require("moment");
var { ObjectID } = require("mongodb");
module.exports = app => {
  //get templatefile of an advocate
  app.get("/templatefile", authenticate, (req, res) => {
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    TemplateFile.find({ _creator })
      .then(files => {
        res.send({ files });
      })
      .catch(e => {
        res.status(400).send();
      });
  });

  //create new template file
  app.post("/templatefile", authenticate, (req, res) => {
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    const body = _.pick(req.body, ["name", "text"]);
    body.createdAt = moment().valueOf();
    body._creator = _creator;
    new TemplateFile(body)
      .save()
      .then(file => {
        res.send({ file });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //edit a template file
  app.patch("/templatefile/:id", authenticate, (req, res) => {
    const _id = req.params.id;
    if (!ObjectID.isValid(_id)) {
      return res.status(404).send();
    }
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    const body = _.pick(req.body, ["name", "text"]);
    TemplateFile.findOneAndUpdate(
      { _id, _creator },
      { $set: body },
      { new: true }
    )
      .then(file => {
        if (!file) {
          return res.status(404).send();
        }
        res.send({ file });
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });

  //delete template file
  app.delete("/templatefile/:id", authenticate, (req, res) => {
    const _id = req.params.id;
    if (!ObjectID.isValid(_id)) {
      return res.status(404).send();
    }
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    TemplateFile.findOneAndRemove({ _creator, _id })
      .then(file => {
        if (!file) {
          return res.status(404).send();
        }
        res.send({ file });
      })
      .catch(e => {
        res.status(400).send();
      });
  });
};
