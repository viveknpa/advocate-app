const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
var { ObjectID } = require("mongodb");

const { User } = require("../models/user");

const { Passbook } = require("../models/passbook");
const { Transaction } = require("../models/transaction");
const { Tarikh } = require("../models/tarikh");
const { ClientFile } = require("../models/clientFile");

const { ClientDoc } = require("../models/clientDoc");

const { Client } = require("../models/client");
require("../misc/cron");

module.exports = app => {
  //transfer case
  app.post("/transfercase/:clientId", authenticate, (req, res) => {
    const _client = req.params.clientId;
    if (!ObjectID.isValid(_client)) {
      return res.status(404).send();
    }
    const _creator = req.user._creator ? req.user._creator : req.user._id;
    User.findOne({ email: req.body.email })
      .then(user => {
        if (!user) {
          return res.status(404).send();
        }
        return Client.findOne({ _id: _client, _creator }).then(client => {
          if (!client) {
            return res.status(404).send();
          }
          client = client.update({
            $set: { _creator: user._id },
            $unset: { assignTo: 1 }
          });
          const passbook = Passbook.findOneAndUpdate(
            { _client, _creator },
            { $set: { _creator: user._id } }
          );
          const transaction = Transaction.update(
            { _client, _creator },
            { $set: { _creator: user._id } },
            { multi: true }
          );
          const tarikh = Tarikh.findOneAndUpdate(
            { _client, _creator },
            { $set: { _creator: user._id } }
          );
          const clientFile = ClientFile.findOneAndUpdate(
            { _client, _creator },
            { $set: { _creator: user._id } }
          );
          const clientDoc = ClientDoc.findOneAndUpdate(
            { _client, _creator },
            { $set: { _creator: user._id } }
          );
          return Promise.all([
            client,
            passbook,
            transaction,
            tarikh,
            clientFile,
            clientDoc
          ]);
        });
      })
      .then(() => {
        res.status(200).send();
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });
};
