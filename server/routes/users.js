const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
const { User } = require("../models/user");
const { UserType } = require("../models/userType");
const { Client } = require("../models/client");
const { sendVerifyMail } = require("../misc/sendgrid");
const { Employee } = require("../models/employee");
const { Tarikh } = require("../models/tarikh");
const moment = require("moment");
require("../misc/cron");
module.exports = app => {
  //admin to get list of all users
  app.get("/users/list", authenticate, (req, res) => {
    User.find()
      .then(users => {
        res.send({ users });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //admin to activate or assign usertype to a user
  app.post("/users/adminupdate/:id", authenticate, (req, res) => {
    const id = req.params.id;
    const body = _.pick(req.body, [
      "active",
      "accountLocked",
      "userType",
      "accountExpiresOn",
      "sentMessages"
    ]);
    User.findById(id)
      .then(user => {
        if (!user) {
          return res.status(400).send();
        }

        //create default profile with basic data, if exists will update
        //will be useful if user signUp as normal user and admin change it to advocate-basic
        if (user.userType !== body.userType && body.userType === 9) {
          const query = { _creator: user._id };
          const update = {
            $set: {
              fullName: user.fullName,
              contactInfo: {
                mobile: user.mobile
              }
            }
          };
          const options = {
            upsert: true,
            new: true,
            setDefaultsOnInsert: true
          };
          Profile.findOneAndUpdate(query, update, options).catch(e =>
            console.log(e, "error in saving profile!")
          );
        }

        return user.update({
          $set: body
        });
      })
      .then(() => {
        res.status(200).send();
      })
      .catch(e => {
        res.status(400).send();
      });
  });

  //SignUp route
  app.post("/users", (req, res) => {
    var body = _.pick(req.body, ["fullName", "email", "mobile", "password"]);
    body.accountExpiresOn = moment()
      .add(7, "day")
      .valueOf();
    body.secretToken = randomstring.generate(); //to verify email
    if (req.body.userType === 9) {
      //signup as advocate basic
      body.userType = 9;
    }
    var user = new User(body);

    const to = body.email;
    const text = `copy and paste the following code: ${body.secretToken}`;
    const subject = "Verify your account on npaGuru";

    sendVerifyMail(to, subject, text);

    user
      .save()
      .then(user => {
        res.status(200).send({ email: user.email });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  // POST /users/login {email, password}
  app.post("/users/login", (req, res) => {
    var body = _.pick(req.body, ["email", "password"]);

    User.findByCredentials(body.email, body.password)
      .then(user => {
        const accountExpired = new Date().getTime() > user.accountExpiresOn;
        if (!user.active) {
          return res
            .status(400)
            .send({ accountActive: false, email: user.email });
        } else if (user.accountLocked) {
          return res.status(400).send({ accountLocked: true });
        } else if (accountExpired) {
          return res.status(400).send({ accountExpired: true });
        }
        return user.generateAuthToken().then(token => {
          res.header("x-auth", token).send(user);
        });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send(e);
      });
  });

  //update profile status to true
  app.get("/user/profilestatus", authenticate, (req, res) => {
    User.findByIdAndUpdate(req.user._id, { profileStatus: true })
      .then(() => {
        res.status(200).send();
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //this route will return a individual authenticated user
  //need to write more clear code here, will do it later
  app.get("/users/me", authenticate, (req, res) => {
    UserType.findOne({ type: req.user.userType })
      .then(userType => {
        const data = {
          user: req.user,
          authority: userType.authority,
          dashboardInfo: {}
        };

        data.dashboardInfo.userId = req.user._id;
        const allPromises = [];
        const loginDetailsUpadte = User.findByIdAndUpdate(
          req.user._id,
          { $push: { loginDetail: new Date().getTime() } },
          { new: true }
        );
        allPromises.push(loginDetailsUpadte);
        if (userType.type === 9) {
          const totalAssociates = Employee.find({ _creator: req.user._id })
            .count()
            .then(count => {
              data.dashboardInfo.totalAssociates = count;
            });
          const totalCases = Client.find({ _creator: req.user._id })
            .count()
            .then(count => {
              data.dashboardInfo.totalCases = count;
            });
          const tarikhs = Tarikh.find({ _creator: req.user._id })
            .select("date appeared reason")
            .then(tarikhs => {
              data.dashboardInfo.tarikhs = tarikhs;
              data.dashboardInfo.tarikhsCount = tarikhs.length;
              let tarikhsToday = 0,
                tarikhsWeek = 0,
                tarikhsMonth = 0,
                tarikhsYear = 0;
              tarikhs.forEach(tarikh => {
                if (moment(tarikh.date).isAfter(moment(), "day")) {
                  if (moment().isSame(moment(tarikh.date), "day"))
                    ++tarikhsToday;
                  if (moment().isSame(moment(tarikh.date), "week"))
                    ++tarikhsWeek;
                  if (moment().isSame(moment(tarikh.date), "month"))
                    ++tarikhsMonth;
                  if (moment().isSame(moment(tarikh.date), "year"))
                    ++tarikhsYear;
                }
              });
              data.dashboardInfo.tarikhsToday = tarikhsToday;
              data.dashboardInfo.tarikhsWeek = tarikhsWeek;
              data.dashboardInfo.tarikhsMonth = tarikhsMonth;
              data.dashboardInfo.tarikhsYear = tarikhsYear;
            });

          allPromises.push(totalAssociates);
          allPromises.push(totalCases);
          allPromises.push(tarikhs);
        }

        return Promise.all(allPromises).then(() => data);
      })
      .then(data => {
        res.send(data);
      })
      .catch(e => {
        console.log(e);
        res.status(400).send();
      });
  });

  //fetch current user

  app.get("/users/current_user", authenticate, (req, res) => {
    res.send(req.user._id);
  });

  //logout
  app.delete("/users/me/token", authenticate, (req, res) => {
    req.user.removeToken(req.token).then(
      () => {
        res.status(200).send();
      },
      () => {
        res.status(400).send();
      }
    );
  });
};
