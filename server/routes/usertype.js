const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
const { UserType } = require("../models/userType");
module.exports = app => {
  //admin to get list of userTypes as array
  app.get("/usertype/list", authenticate, (req, res) => {
    // need some validation here to allow only admin to get list, do it later
    UserType.find()
      .then(userTypes => {
        res.send({ userTypes });
      })
      .catch(e => {
        res.status(400).send(e);
      });
  });

  //admin to create a new user Type
  app.post("/usertype/create", (req, res) => {
    const body = _.pick(req.body, ["name", "description"]);
    UserType.find()
      .distinct("type")
      .then(types => {
        let newType;
        if (types.length) {
          newType = Math.max(...types) + 1;
        } else {
          newType = 1;
        }
        body.type = newType;
        new UserType(body).save().then(doc => {
          res.send({ userType: doc });
        });
      })
      .catch(e => {
        res.status(400).send(e);
      });
  });

  // edit userType's name and description
  app.patch("/userType/edit/:id", (req, res) => {
    const id = req.params.id;
    const body = _.pick(req.body, ["description", "name", "authority"]);
    UserType.findOneAndUpdate({ _id: id }, { $set: body }, { new: true })
      .then(type => {
        res.send({ body, userType: type });
      })
      .catch(e => {
        console.log(e);
        res.status(400).send(e);
      });
  });

  //admin to remove a user Type
  app.delete("/usertype/remove/:id", authenticate, (req, res) => {
    const id = req.params.id;
    if (!ObjectID.isValid(id)) {
      return res.status(404).send();
    }
    UserType.findByIdAndRemove(id)
      .then(userType => {
        if (!userType) {
          return res.status(404).send({ userType });
        }
        User.update(
          { userType: userType.type },
          { userType: 16 },
          { multi: true }
        ).then(data => {
          console.log(data);
        });
        res.status(200).send();
      })
      .catch(e => {
        res.status(400).send();
        console.log(e);
      });
  });
};
