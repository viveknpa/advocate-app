const { authenticate } = require("../middleware/authenticate");
const _ = require("lodash");
const { User } = require("../models/user");
const { Profile } = require("../models/profile");
module.exports = app => {
  //verify account by sending an email with randomString
  app.post("/verifyaccount", (req, res) => {
    const secretToken = req.body.secretToken.trim();
    User.findOne({ secretToken })
      .then(user => {
        if (!user) {
          return res.status(404).send();
        }

        //create default profile with basic data
        const query = { _creator: user._id };
        const update = {
          $set: {
            fullName: user.fullName,
            contactInfo: {
              mobile: user.mobile
            }
          }
        };
        const options = { upsert: true, new: true, setDefaultsOnInsert: true };
        Profile.findOneAndUpdate(query, update, options).catch(e =>
          console.log(e, "error in saving profile!")
        );

        return user.update({
          $set: {
            secretToken: "",
            active: true
          }
        });
      })
      .then(() => {
        res.status(200).send();
      })
      .catch(e => {
        res.status(400).send();
      });
  });
};
