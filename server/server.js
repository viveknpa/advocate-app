require("./config/config");

const path = require("path");
const _ = require("lodash");
var express = require("express");
var bodyParser = require("body-parser");
var randomstring = require("randomstring");
var { ObjectID } = require("mongodb");
const moment = require("moment");
const axios = require("axios");

const { mongoose } = require("./db/mongoose");

const { UserType } = require("./models/userType");

const { Passbook } = require("./models/passbook");
const { Transaction } = require("./models/transaction");
const { Tarikh } = require("./models/tarikh");
const { ClientFile } = require("./models/clientFile");

const { ClientDoc } = require("./models/clientDoc");

require("./misc/cron");

const app = express();
const server = require("http").createServer(app);
const port = process.env.PORT;
const publicPath = path.join(__dirname, "..", "public");
app.use(express.static(publicPath));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

require("./routes/app")(app);
require("./routes/profile")(app);
require("./routes/case")(app);
require("./routes/sms")(app);
require("./routes/employee")(app);
require("./routes/client")(app);
require("./routes/passbook")(app);
require("./routes/tarikh")(app);
require("./routes/state")(app);
require("./routes/event")(app);
require("./routes/clientfile")(app);
require("./routes/templatefile")(app);
require("./routes/clientdoc")(app);
require("./routes/transfercase")(app);
require("./routes/advertise")(app);
require("./routes/support")(app);
require("./routes/usertype")(app);
require("./routes/users")(app);
require("./routes/verifyaccount")(app);
require("./routes/sendVerificationCode")(app);
require("./routes/reset")(app);
require("./routes/appointment")(app);
require("./routes/dashboard")(app);
require("./routes/causeList")(app);

app.get("*", (req, res) => {
  res.sendFile(path.join(publicPath, "index.html"));
});

server.listen(port, () => {
  console.log(`started up at port ${port}`);
});

module.exports = { app };
