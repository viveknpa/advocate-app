export const selectClient = (client) => ({
    type: 'SELECT_CLIENT',
    client
})