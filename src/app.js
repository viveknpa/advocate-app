import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import AppRouter, { history } from "./routers/AppRouter";
import configureStore from "./store/configureStore";
import { login, logout } from "./actions/auth";
import { selectClient } from "./actions/selectedClient";
import "./styles/styles.scss";
import "react-table/react-table.css";
import "react-dates/lib/css/_datepicker.css";
import LoadingPage from "./components/LoadingPage";
import axios from "axios";
window.axios = axios;
export const store = configureStore();

const jsx = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);

let hasRendered = false;
const renderApp = () => {
  if (!hasRendered) {
    ReactDOM.render(jsx, document.getElementById("app"));
    hasRendered = true;
  }
};

ReactDOM.render(<LoadingPage />, document.getElementById("app"));

const onAuthStateChanged = () => {
  axios({
    method: "get",
    url: "/users/me",
    headers: { "x-auth": localStorage.getItem("x-auth") }
  })
    .then(response => {
      const {
        _id,
        fullName,
        email,
        mobile,
        dpUrl,
        userType,
        profileStatus,
        accountExpiresOn
      } = response.data.user;
      const authority = response.data.authority || {};
      const dashboardInfo = response.data.dashboardInfo || {};
      store.dispatch(
        login({
          uid: _id,
          fullName,
          email,
          mobile,
          dpUrl,
          userType,
          accountExpiresOn,
          profileStatus,
          authority,
          dashboardInfo
        })
      );
      renderApp();
      if (authority["profile"] && !profileStatus) {
        // push to create profile after first login
        history.push(`/advocate-profile/${_id}`);
      }
      if (history.location.pathname === "/") {
        history.push("/dashboard");
      }
    })
    .catch(e => {
      store.dispatch(logout());
      store.dispatch(selectClient({}));
      renderApp();
    });
};

onAuthStateChanged();
export default onAuthStateChanged;
