import React from "react";
import ReactDOM from "react-dom";
import axios from 'axios';

import {
  Table,
  Label,
  Form,
  Dropdown,
  Button,
  Radio,
  Icon
} from "semantic-ui-react";

class AddCourtPage extends React.Component {
  state = {
    stateInfo: [
      // {
      //   _id: 1,
      //   state: "delhi",
      //   cities: [
      //     "New Delhi",
      //     "North Delhi",
      //     "North West Delhi",
      //     "West Delhi",
      //     "South West Delhi",
      //     "South Delhi",
      //     "South East Delhi",
      //     "Central Delhi",
      //     "North East Delhi",
      //     "Shahdara",
      //     "East Delhi"
      //   ]
      // },
      // {
      //   _id: 2,
      //   state: "Madhya Pradesh",
      //   cities: ["Bhopal", "vidisha"]
      // }
    ],
    selectedStateInfo: {},
    selectedState: "",
    selectedCity: "",
    mode: "" //edit, delete or add
  };

  config = { headers: { 'x-auth': localStorage.getItem('x-auth') } }

  changeMode = mode =>
    this.setState(prevState => ({
      mode: prevState.mode === mode ? "" : mode
    }));

  onRowSelect = (e, { value }) => {
    const selectedStateInfo =
      this.state.stateInfo.filter(info => info._id === value)[0] || {};
    this.setState({
      selectedStateInfo: selectedStateInfo
    });
  };

  saveEditedState = (id, editedState) => {
      let newInfo;
      let newStateInfo = this.state.stateInfo.map(info => {
        if (info._id === id) {
          info.state = editedState;
          newInfo = info;
          return info;
        }
        return info;
      })

      if(!newInfo) { return  }

      axios.patch(`/state/${id}`, newInfo, this.config).then(() => {
        this.setState({ stateInfo: newStateInfo });
        alert("Name changed to " + editedState);
      }).catch(() => {
        alert("Unable to rename state");
      })
  };

  saveEditedCity = (id, oldCity, editedCity) => {
    let newInfo;
    let newStateInfo = this.state.stateInfo.map(info => {
      if (info._id === id) {
        info.cities = info.cities.map(city => {
          if (city === oldCity) {
            return editedCity;
          }
          return city;
        })
        newInfo = info;
      }
      return info;
    })

    if(!newInfo) { return  }

    axios.patch(`/state/${id}`, newInfo, this.config).then(() => {
      this.setState({ stateInfo: newStateInfo,selectedStateInfo: newInfo });
      alert("Name changed to " + editedCity);
    }).catch(() => {
      alert("Unable to rename City");
    })
  };

  onDeleteCity = (id, cityToRemove) => {
    if (!confirm("You want to delete city " + cityToRemove)) {
      return;
    }
    let newInfo;
    let newStateInfo = this.state.stateInfo.map(info => {
      if (info._id === id) {
        info.cities = info.cities.filter(city => city !== cityToRemove);
        newInfo = info;
      }
      return info;
    })

    if(!newInfo) { return; }

    axios.patch(`/state/${id}`, newInfo, this.config).then(() => {
      this.setState({ stateInfo: newStateInfo, selectedStateInfo: newInfo });
      alert("city Removed!");
    }).catch(() => {
      alert("Unable to remove City");
    })
  };

  onAddNewCity = (id, newCity) => {
    let newInfo;
     let newStateInfo = this.state.stateInfo.map(info => {
      if (info._id === id) {
        let matched = 0;
        info.cities.forEach(city => {
          if (city.toLowerCase() === newCity.toLowerCase()) {
            matched = 1;
          }
        });
        if (!matched) {
          info.cities.unshift(newCity);
          newInfo = info;
          return info;
        }
        alert("City with this name already exist");
      }
      return info;
    })

    if(!newInfo) { return  }

    axios.patch(`/state/${id}`, newInfo, this.config).then(() => {
      this.setState({ stateInfo: newStateInfo, selectedStateInfo: newInfo });
      alert("New City added!");
    }).catch(() => {
      alert("Unable to add City");
    })

  };

  onDeleteRow = () => {
    const id = this.state.selectedStateInfo._id;
    if ( id && confirm("Are you sure you want to delete selected row") ) {

        let newStateInfo = this.state.stateInfo.filter(
          info => info._id !== id
        )
        axios.delete(`/state/${id}`, this.config).then(() => {
          this.setState({ stateInfo: newStateInfo, selectedState: {} })
        }).catch((e) => {
          alert('Unable to delete state informations');
        })
    }
  };

  handleAddNewState = e => {
    e.preventDefault();
    const state = e.target.state.value;
    const city = e.target.city.value;

    axios.post('/state', { state, cities: [city] }, this.config).then((response) => {
      this.setState(prevState => ({
        stateInfo: [...prevState.stateInfo, response.data.info],
        selectedStateInfo: {}
      }))
    }).catch((e) => {
      console.log('Unable to add state informations');
    })
  };

  componentDidMount() {
    axios.get('/state', this.config).then((response) => {
      this.setState({ stateInfo: response.data.infos })
    }).catch((e) => {
      console.log(e);
      alert('error: Unable to get state inoformations');
    })
  }

  render() {
    const { selectedStateInfo, mode, selectedCity, selectedState } = this.state;
    return (
      <div>
        <Button
          onClick={() => this.changeMode("add")}
          style={{ background: mode === "add" ? "orange" : "" }}
        >
          Add new State
        </Button>
        <br />
        <br />
        {mode === "add" && (
          <Form onSubmit={this.handleAddNewState}>
            <Form.Group>
              <Form.Input placeholder="State" name="state" required/>
              <Form.Input placeholder="City" name="city" />
              <Form.Button content="Add new State" color="green"/>
            </Form.Group>
          </Form>
        )}
        <Table celled>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Select</Table.HeaderCell>
              <Table.HeaderCell>Index</Table.HeaderCell>
              <Table.HeaderCell>State</Table.HeaderCell>
              <Table.HeaderCell>District</Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            {this.state.stateInfo.map((info, index) => (
              <StateItem
                key={index}
                onRowSelect={this.onRowSelect}
                selectedStateInfo={selectedStateInfo}
                {...info}
                index={index + 1}
                mode={mode}
                saveEditedState={this.saveEditedState}
                saveEditedCity={this.saveEditedCity}
                onAddNewCity={this.onAddNewCity}
                onDeleteCity={this.onDeleteCity}
              />
            ))}
          </Table.Body>

          <Table.Footer fullWidth>
            <Table.Row>
              <Table.HeaderCell />
              <Table.HeaderCell colSpan="4">
                <Button
                  floated="right"
                  color="red"
                  onClick={this.onDeleteRow}
                  style={{ background: mode === "delete" ? "orange" : "" }}
                >
                  Delete State
                </Button>
                <Button
                  floated="right"
                  onClick={() => this.changeMode("edit")}
                  style={{ background: mode === "edit" ? "orange" : "" }}
                >
                  Edit State
                </Button>
              </Table.HeaderCell>
            </Table.Row>
          </Table.Footer>
        </Table>
      </div>
    );
  }
}

class StateItem extends React.Component {
  state = {
    selectedStateInfo: this.props.selectedStateInfo,
    selectedState: this.props.selectedStateInfo.state,
    selectedCity: this.props.selectedStateInfo.cities
      ? selectedStateInfo.cities[0]
      : "",
    editedState: this.props.selectedStateInfo.state,
    editedCity: this.props.selectedStateInfo.cities
      ? selectedStateInfo.cities[0]
      : ""
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      selectedStateInfo: nextProps.selectedStateInfo,
      selectedState: nextProps.selectedStateInfo.state,
      selectedCity: nextProps.selectedStateInfo.cities
        ? nextProps.selectedStateInfo.cities[0]
        : "",
      editedState: nextProps.selectedStateInfo.state,
      editedCity: nextProps.selectedStateInfo.cities
        ? nextProps.selectedStateInfo.cities[0]
        : ""
    });
  }

  onStateEdit = e => this.setState({ editedState: e.target.value });
  onCityEdit = e => this.setState({ editedCity: e.target.value });
  onNewCityChange = e => this.setState({ newCity: e.target.value });

  onCityChange = (e, { value }) => {
    this.setState({
      selectedCity: value,
      editedCity: value
    });
  };

  render() {
    const { selectedStateInfo, mode, _id, index, state, cities } = this.props;
    const {
      selectedCity,
      selectedState,
      editedState,
      editedCity,
      newCity
    } = this.state;
    return (
      <Table.Row
        style={{ background: selectedStateInfo._id === _id ? "#F9FAFB" : "" }}
      >
        <Table.Cell>
          <Radio
            name="radioGroup"
            value={_id}
            checked={selectedStateInfo._id === _id}
            onChange={this.props.onRowSelect}
          />
        </Table.Cell>
        <Table.Cell>{index}</Table.Cell>
        <Table.Cell>
          {selectedStateInfo._id === _id && mode == "edit" ? (
            <div style={{ display: "flex" }}>
              <Form.Input 
                placeholder="Edit State"
                value={editedState} 
                onChange={this.onStateEdit} 
              />
              <Button
                primary
                size="tiny"
                style={{ marginLeft: "5px" }}
                onClick={() => this.props.saveEditedState(_id, editedState)}
              >
                Save
              </Button>
            </div>
          ) : (
            state
          )}
        </Table.Cell>
        <Table.Cell>
          <Dropdown
            options={cities.map((city, index) => ({
              key: index,
              text: city,
              value: city
            }))}
            placeholder="Select"
            selection
            value={selectedCity}
            onChange={this.onCityChange}
          />
          {selectedStateInfo._id === _id &&
            mode == "edit" && (
              <div>
                <div style={{ display: "flex", margin: "5px 0" }}>
                  <Form.Input value={editedCity} onChange={this.onCityEdit} placeholder="edit city"/>
                  <Button
                    size="tiny"
                    primary
                    style={{ marginLeft: "5px" }}
                    onClick={() =>
                      this.props.saveEditedCity(_id, selectedCity, editedCity)
                    }
                  >
                    Save
                  </Button>
                  <Button
                    color="red"
                    size="tiny"
                    style={{ marginLeft: "5px" }}
                    onClick={() => this.props.onDeleteCity(_id, selectedCity)}
                  >
                    delete
                  </Button>
                </div>
                <div style={{ display: "flex" }}>
                  <Form.Input value={newCity} onChange={this.onNewCityChange} placeholder="New city"/>
                  <Button
                    size="tiny"
                    primary
                    style={{ marginLeft: "5px" }}
                    onClick={() => this.props.onAddNewCity(_id, newCity)}
                  >
                    Add new City
                  </Button>
                </div>
              </div>
            )}
        </Table.Cell>
      </Table.Row>
    );
  }
}

export default AddCourtPage;