import React from "react";
import axios from "axios";
import { Grid, Form } from "semantic-ui-react";
import Calendar from "react-big-calendar";
import moment from "moment";

import "react-big-calendar/lib/css/react-big-calendar.css";

Calendar.setLocalizer(Calendar.momentLocalizer(moment));

class AddCourtWiseEventPage extends React.Component {
  state = {
    options: [
      {
        //demo options
        _id: 1,
        state: "delhi",
        cities: [
          "New Delhi",
          "North Delhi",
          "North West Delhi",
          "West Delhi",
          "South West Delhi",
          "South Delhi",
          "South East Delhi",
          "Central Delhi",
          "North East Delhi",
          "Shahdara",
          "East Delhi"
        ]
      },
      {
        _id: 2,
        state: "Madhya Pradesh",
        cities: ["Bhopal", "vidisha"]
      }
    ],
    allEvents: [], //all events of a type of court
    selectedCourt: "SC",
    selectedState: "",
    selectedCity: "",
    events: [] //filtered events by city and state
  };
  config = { headers: { "x-auth": localStorage.getItem("x-auth") } };

  loadCourtEvents = court => {
    axios
      .get(`/event/${court}`, this.config)
      .then(response => {
        this.setState(prevState => ({
          allEvents: response.data.events,
          events: response.data.events
        }));
      })
      .catch(e => {
        alert("Unable to get events");
        console.log(e);
      });
  };

  handleCourtChange = (e, { value }) =>
    this.setState(() => {
      this.loadCourtEvents(value);
      return { selectedCourt: value };
    });
  handleStateChange = (e, { value }) =>
    this.setState({
      selectedState: value,
      selectedCity: "",
      events: this.state.allEvents.filter(event => event.state === value)
    });
  handleCityChange = (e, { value }) =>
    this.setState({
      selectedCity: value,
      events: this.state.allEvents.filter(
        event =>
          event.city === value && event.state === this.state.selectedState
      )
    });

  componentDidMount() {
    this.loadCourtEvents("SC");
  }

  componentWillMount() {
    axios
      .get("/state", this.config)
      .then(response => {
        this.state.options = response.data.infos;
      })
      .catch(e => {
        console.log(e);
        alert("error: Unable to get state inoformations");
      });
  }

  onCreateEvent = slotInfo => {
    if (slotInfo.action !== "select") {
      return;
    }
    const title = prompt("Please enter event title", "Title");
    if (!title) {
      return;
    }
    const newEvent = {
      title,
      start: moment(slotInfo.start).format(),
      end: moment(slotInfo.end).format(),
      state: this.state.selectedState,
      city: this.state.selectedCity
    };
    axios
      .post(`/event/${this.state.selectedCourt}`, newEvent, this.config)
      .then(response => {
        this.setState(prevState => ({
          allEvents: [...prevState.allEvents, newEvent],
          events: [...prevState.events, newEvent]
        }));
      })
      .catch(e => {
        alert("Unable to add event");
        console.log(e);
      });
  };

  render() {
    let cityOptions = [];
    if (this.state.selectedState) {
      cityOptions = this.state.options
        .filter(option => option.state === this.state.selectedState)[0]
        .cities.map((city, index) => {
          return {
            key: index + 1,
            text: city,
            value: city
          };
        });
    }

    let stateOptions = this.state.options.map((option, index) => {
      return {
        key: index + 1,
        text: option.state,
        value: option.state
      };
    });
    return (
      <div>
        <Form>
          <Grid>
            <Grid.Column mobile={16} tablet={8} computer={4}>
              <Form.Select
                placeholder="Select court"
                fluid
                label="Select Court"
                selection
                onChange={this.handleCourtChange}
                options={[
                  { key: 1, text: "Supreme court", value: "SC" },
                  { key: 2, text: "High Court", value: "HC" },
                  { key: 3, text: "District Court", value: "DC" }
                ]}
                value={this.state.selectedCourt}
              />
            </Grid.Column>
            <Grid.Column mobile={16} tablet={8} computer={4}>
              <Form.Select
                placeholder="Select state"
                fluid
                label="Select State"
                selection
                onChange={this.handleStateChange}
                options={stateOptions}
                value={this.state.selectedState}
              />
            </Grid.Column>
            <Grid.Column mobile={16} tablet={8} computer={4}>
              <Form.Select
                placeholder="Select city"
                fluid
                label="Select District"
                selection
                onChange={this.handleCityChange}
                options={cityOptions}
                value={this.state.selectedCity}
              />
            </Grid.Column>
          </Grid>
          <br />
          <br />
        </Form>
        <Calendar
          style={{ height: "100vh" }}
          popup
          selectable
          defaultView="month"
          defaultDate={new Date()}
          events={this.state.events}
          onSelectSlot={this.onCreateEvent}
        />
      </div>
    );
  }
}

export default AddCourtWiseEventPage;
