import React from "react";
import { connect } from "react-redux";
import { Message, Grid, Select } from "semantic-ui-react";
import { Bar, Doughnut, Line, Pie, Polar, Radar } from "react-chartjs-2";
import axios from "axios";
import moment from "moment";

class AdvocateDashboard extends React.Component {
  state = {
    data: {
      clientCount: 0,
      names: [],
      totalAssociates: 0,
      tarikhs: [],
      tarikhsCount: 0,
      tarikhsToday: [],
      tarikhsWeek: [],
      tarikhsMonth: [],
      tarikhsYear: [],
      modalOpen: false
    },
    countTarikh: [],
    selectedYear: parseInt(moment().format("YYYY")),
    yearSelected: parseInt(moment().format("YYYY")),
    monthSelected: parseInt(moment().format("M")),
    countWeekTarikh: []
  };
  config = { headers: { "x-auth": localStorage.getItem("x-auth") } };

  componentDidMount() {
    axios
      .get(`/dashboard/advocate`, this.config)
      .then(response => {
        this.setState({ data: response.data.dashboardData });
      })
      .catch(e => {
        console.log(e);
      });

    axios
      .get(
        `/dashboard/advocate/tarikhs/${parseInt(moment().format("YYYY"))}`,
        this.config
      )
      .then(response => {
        this.setState({ countTarikh: response.data.countTarikh });
      })
      .catch(e => console.log(e));
    axios
      .get(
        `/dashboard/advocate/tarikhs/${parseInt(
          moment().format("YYYY")
        )}/${parseInt(moment().format("M"))}`,
        this.config
      )
      .then(response => {
        this.setState({ countWeekTarikh: response.data.countWeekTarikh });
      })
      .catch(e => console.log(e));
  }

  monthlyCountCalc = year => {
    axios
      .get(`/dashboard/advocate/tarikhs/${year}`, this.config)
      .then(response => {
        this.setState({ countTarikh: response.data.countTarikh });
      });
  };
  weekyCountCalc = (year, month) => {
    axios
      .get(`/dashboard/advocate/tarikhs/${year}/${month}`, this.config)
      .then(response => {
        this.setState({ countWeekTarikh: response.data.countWeekTarikh });
      })
      .catch(e => console.log(e));
  };
  handleChangeMonth = (e, prop) => {
    this.setState({ monthSelected: prop.value });
    this.weekyCountCalc(this.state.yearSelected, this.state.monthSelected);
  };
  handleYear = (e, prop) => {
    this.setState({ yearSelected: prop.value });
    this.weekyCountCalc(this.state.yearSelected, this.state.monthSelected);
  };

  handleChangeYear = (e, prop) => {
    this.setState({ selectedYear: prop.value });
    this.monthlyCountCalc(prop.value);
  };

  render() {
    const options = {
      tooltips: {
        enabled: false
      },
      maintainAspectRatio: true
    };

    const bar = {
      labels: [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
      ],
      datasets: [
        {
          label: "No of tarikhs Yearwise",
          backgroundColor: "rgba(255,99,132,0.2)",
          borderColor: "rgba(255,99,132,1)",
          borderWidth: 1,
          hoverBackgroundColor: "rgba(255,99,132,0.4)",
          hoverBorderColor: "rgba(255,99,132,1)",
          data: this.state.countTarikh
        }
      ]
    };
    const bar1 = {
      labels: ["Week 1", "Week 2", "Week 3", "Week 4", "Week 5"],
      datasets: [
        {
          label: "No of weekly tarikhs  MonthWise",
          backgroundColor: "rgba(255,99,132,0.2)",
          borderColor: "rgba(255,99,132,1)",
          borderWidth: 1,
          hoverBackgroundColor: "rgba(255,99,132,0.4)",
          hoverBorderColor: "rgba(255,99,132,1)",
          data: this.state.countWeekTarikh
        }
      ]
    };
    const monthOptions = [
      {
        key: 0,
        value: 1,
        text: "January"
      },
      {
        key: 1,
        value: 2,
        text: "February"
      },
      {
        key: 2,
        value: 3,
        text: "March"
      },
      {
        key: 3,
        value: 4,
        text: "April"
      },
      {
        key: 4,
        value: 5,
        text: "May"
      },
      {
        key: 5,
        value: 6,
        text: "June"
      },
      {
        key: 6,
        value: 7,
        text: "July"
      },
      {
        key: 7,
        value: 8,
        text: "August"
      },
      {
        key: 8,
        value: 9,
        text: "September"
      },
      {
        key: 9,
        value: 10,
        text: "October"
      },
      {
        key: 10,
        value: 11,
        text: "November"
      },
      {
        key: 11,
        value: 12,
        text: "December"
      }
    ];
    const yearOptions = [
      {
        key: 0,
        value: 2018,
        text: 2018
      },
      {
        key: 1,
        value: 2019,
        text: 2019
      },
      {
        key: 3,
        value: 2020,
        text: 2020
      },
      {
        key: 4,
        value: 2021,
        text: 2021
      },
      {
        key: 5,
        value: 2022,
        text: 2022
      },
      {
        key: 6,
        value: 2023,
        text: 2023
      },
      {
        key: 7,
        value: 2024,
        text: 2024
      },
      {
        key: 8,
        value: 2025,
        text: 2025
      },
      {
        key: 9,
        value: 2026,
        text: 2026
      },
      {
        key: 10,
        value: 2027,
        text: 2027
      }
    ];

    const {
      totalAssociates,
      totalCases,
      tarikhsToday,
      tarikhsWeek,
      tarikhsMonth,
      tarikhsYear,
      tarikhsCount
    } = this.props.dashboardInfo;

    return (
      <div>
        <Grid divided="vertically">
          <Grid.Row>
            <Grid.Column width={3}>
              Total Number of Associates - {totalAssociates}
            </Grid.Column>
            <Grid.Column width={3}>
              Total Number of Clients - {totalCases}
            </Grid.Column>
            <Grid.Column width={3}>
              Total Number of Tarikhs - {tarikhsCount}
            </Grid.Column>
            <Grid.Column width={3}>
              Total Number of Tarikhs today - {tarikhsToday}
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={4}>
              Total Number of Tarikhs this week -{tarikhsWeek}
            </Grid.Column>
            <Grid.Column width={4}>
              Total Number of Tarikhs this month -{tarikhsMonth}
            </Grid.Column>
            <Grid.Column width={4}>
              Total Number of Tarikhs this year -{tarikhsYear}
            </Grid.Column>
          </Grid.Row>
        </Grid>

        <Grid divided="vertically">
          <Grid.Row>
            <Grid.Column width={6}>
              <Select
                placeholder="Select Year"
                options={yearOptions}
                onChange={this.handleChangeYear}
                value={this.state.selectedYear}
              />
              <Bar data={bar} options={options} />
            </Grid.Column>
            <Grid.Column width={6}>
              <Select
                placeholder="Select Year"
                options={yearOptions}
                onChange={this.handleYear}
                value={this.state.yearSelected}
              />
              <Select
                placeholder="Select Month"
                options={monthOptions}
                onChange={this.handleChangeMonth}
                value={this.state.monthSelected}
              />
              <Bar data={bar1} options={options} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  dashboardInfo: state.auth.dashboardInfo
});

export default connect(mapStateToProps)(AdvocateDashboard);
