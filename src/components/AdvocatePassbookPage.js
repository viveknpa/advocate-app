import React from 'react';
import { connect } from 'react-redux';
import { Button, Grid, Icon } from 'semantic-ui-react';
import ReactTable from "react-table";
import printJS from 'print-js';
import moment from 'moment';
import axios from 'axios';

import ViewComment from './common/ViewComment'

class AdvocatePassbookPage extends React.Component {
    state = {
        passbook: [],
        commentModalShow: false,
        comment: ''
    }

    componentWillMount() {
        axios.get('/passbook-advocate', {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            this.setState({ passbook: response.data.passbook })
        }).catch((e) => {
            alert('Unable to get advocate passbook');
            console.log(e);
        })
    }
    showComment = (comment) => this.setState({ commentModalShow: true, comment })
    hideComment = () => this.setState({ commentModalShow: false, comment: '' })
    handlePrintPassbook = () => {
        printJS({
            printable: this.state.passbook.map((transaction) => {
                transaction.date = moment(transaction.createdAt).format('LLL');
                transaction.clientName = transaction._client.fullName;
                transaction.caseId = transaction._client.caseId;
                return transaction;
            }),
            header: `Passbook  (${this.props.userName})`,
            properties: ['date', 'action', 'amount', 'reason', 'balance'],
            type: 'json',
            gridHeaderStyle: 'color: gray;  border: 1px solid #3971A5;padding: 8px; font-size: 20px; color: blue',
            gridStyle: 'border: 1px solid #3971A5; text-align: center; padding: 5px'
        })
    }
    render() {
        return (
            <Grid>
                <Grid.Column mobile={16} computer={14}>
                    <Button type="button" onClick={this.handlePrintPassbook} color="teal" size="small" style={{ marginBottom: '10px' }}>
                        Print passbook
                    </Button>

                    <ReactTable
                        data={this.state.passbook.map((transaction) => {
                            transaction.createdAt = moment(transaction.createdAt).format('LLL');
                            transaction.clientName = transaction._client.fullName;
                            transaction.caseId = transaction._client.caseId;
                            return transaction;
                        })}
                        columns={[
                            {
                                Header: "Client Detail",
                                columns: [{
                                    Header: "Case Id",
                                    accessor: "caseId"
                                },
                                {
                                    Header: "Full name",
                                    accessor: "clientName"
                                }]
                            },
                            {
                                Header: "Transaction Detail",
                                columns: [{
                                    Header: "Date",
                                    accessor: "createdAt",
                                    minWidth: 160
                                },
                                {
                                    Header: "Action",
                                    accessor: "action"
                                },
                                {
                                    Header: "Amount",
                                    accessor: "amount"
                                },
                                {
                                    Header: "Reason",
                                    accessor: "reason",
                                    Cell: ({ value }) =>
                                        <Icon
                                            color="violet"
                                            name="comment outline"
                                            onClick={() => this.showComment(value)}
                                        />
                                },
                                {
                                    Header: "Balance",
                                    accessor: "balance"
                                }]
                            }
                        ]}
                        defaultSorted={[
                            {
                                id: "createdAt",
                                desc: true
                            }
                        ]}
                        defaultPageSize={5}
                        className="-striped -highlight"
                    />
                </Grid.Column>
                <ViewComment
                    show={this.state.commentModalShow}
                    comment={this.state.comment}
                    hideComment={this.hideComment}
                />
            </Grid>
        )
    }
}

const mapStateToProps = (state) => ({
    userName: state.auth.fullName
})

export default connect(mapStateToProps)(AdvocatePassbookPage);