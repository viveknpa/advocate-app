import React from 'react';
import { Button, Modal, Icon } from 'semantic-ui-react'

export default class AlertModel extends React.Component {
  onClose = () => this.props.onHideAlertModel();
  render() {
    let iconName;
    let iconColor;
    if(this.props.alertType === 'success') {
      iconName='info circle'
      iconColor="blue"
    } else if(this.props.alertType === 'error') {
      iconName='times circle outline'
      iconColor="red"
    }
    return (
      <Modal 
        size="mini" 
        open={this.props.open} 
        onClose={this.onClose} 
        centered={false}
      >
        <Modal.Content style={{ padding: '40px' }}>
          <h4 style={{ display: 'flex', alignItems: 'center' }}>
            <div style={{ marginRight: '10px' }}>
              <Icon 
                size="large" 
                name={iconName}
                color={iconColor}
              />
            </div>
            <span>{this.props.message}</span>
          </h4>
          <div style={{ display: 'flex', justifyContent: 'flex-end', paddingRight: '35px' }}>
            <Button 
              size="tiny" 
              onClick={this.onClose} 
              primary
            >
              OK
            </Button>
          </div>
        </Modal.Content>
      </Modal>
    )
  }
}