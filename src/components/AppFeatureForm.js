import React from 'react';
import axios from 'axios';
import { Button, Form, Input, TextArea } from 'semantic-ui-react';

class AppFeatureForm extends React.Component {
    state = {
        title: this.props.title ? this.props.title : '',
        description: this.props.description ? this.props.description : '',
        usesTitle: this.props.usesTitle ? this.props.usesTitle : '',
        usesPoints: this.props.usesPoints ? this.props.usesPoints : [{
            text: '',
            _id: 1
        }]
    }

    onTitleChange = (e) => this.setState({ title: e.target.value });
    onDescription = (e) => this.setState({ description: e.target.value });
    onUsesTitleChange = (e) => this.setState({  usesTitle: e.target.value });
    onUsesPointTextChange = (e, _id) => {
        const text = e.target.value;
        this.setState((prevState) => ({
            usesPoints: prevState.usesPoints.map((point) => {
                if(point._id === _id) {
                    point.text = text;
                }
                return point;
            })
        }))
    }
    onAddNewPoint = () => this.setState((prevState) => ({
        usesPoints: [...prevState.usesPoints, { _id: new Date().getTime(), text: '' }]
    }))

    config = { headers: { 'x-auth': localStorage.getItem('x-auth') } }
    onSubmit = () => {
        const { title, description, usesTitle, usesPoints } = this.state;
        const body = { title, description, usesTitle, usesPoints };
        console.log(body);
        if(!this.props.title) { //means add mode
            axios.post('/app/feature', body, this.config).then((res) => {
                alert('New feature added!')
                if(res.data.feature) {
                    this.props.handleNewFeature(res.data.feature);
                }
            }).catch((e) => {
                alert('Unable to add new feature');
                console.log(e);
            })
        } else {
            axios.patch(`/app/feature/${this.props._id}`, body, this.config).then((res) => {
                alert('feature edited!')
                if(res.data.feature) {
                    this.props.handleEditFeature(res.data.feature);
                }
            }).catch((e) => {
                alert('Unable to add new feature');
                console.log(e);
            })
        }
    }
    render() {
        return (
            <Form size="large" onSubmit={this.onSubmit}>
                <Form.Field>
                    <label>Feature title</label>
                    <input 
                        placeholder="Feature title" 
                        value={this.state.title}
                        onChange={this.onTitleChange}
                        required 
                    />
                </Form.Field>
                <Form.Field inline>
                    <label>Feature description</label>
                    <TextArea 
                        autoHeight 
                        placeholder='Description About feature' 
                        value={this.state.description}
                        onChange={this.onDescription}
                    />
                </Form.Field>
                <Form.Field>
                    <label>Uses Title</label>
                    <input 
                        type="text" 
                        placeholder="Uses Title" 
                        value={this.state.usesTitle}
                        onChange={this.onUsesTitleChange}
                    />
                </Form.Field>
                <Form.Field style={{marginLeft: '10px'}}>
                    <label style={{color: 'blue', cursor: 'pointer'}} onClick={this.onAddNewPoint}>Add Points</label>
                    {
                        this.state.usesPoints.map((point) => (
                            <Form.Input 
                                key={point._id} 
                                placeholder="Uses point"
                                value={point.text}
                                onChange={(e) => this.onUsesPointTextChange(e, point._id)}
                            />
                        ))
                    }
                </Form.Field>
                <Button color="violet">Submit</Button>
            </Form>
        )
    }
}

export default AppFeatureForm;