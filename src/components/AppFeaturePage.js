import React from 'react';
import axios from 'axios';
import { Button, Checkbox, Form, Icon, Input, Grid, TextArea, List, Modal, Header } from 'semantic-ui-react';

import AppFeatureForm from './AppFeatureForm';

class AppFeaturePage extends React.Component {
    state = {
        features: [],
        modalOpen: false,
        mode: null, //edit or add feature mode
        featureToEdit: null
    }

    handleOpen = (mode) => this.setState({ modalOpen: true, mode })
    handleClose = () => this.setState({ modalOpen: false })

    config = { headers: { 'x-auth': localStorage.getItem('x-auth') } }
    componentDidMount() {
        axios.get('/app/feature', this.config).then((res) => {
            this.setState({ features: res.data.features })
        }).catch((e) => {
            console.log('Unable to get App features');
            console.log(e);
        })
    }

    handleNewFeature = (newFeature) => {
        this.setState((prevState) => ({
            features: [newFeature, ...prevState.features],
            modalOpen: false,
            mode: null
        }))
    }

    handleFeatureToEdit = (featureToEdit) => this.setState({ featureToEdit, mode: 'edit', modalOpen: true })

    handleEditFeature = (editedFeature) => {
        this.setState((prevState) => ({
            features: prevState.features.map((feature) => {
                if(feature._id === editedFeature._id) {
                    return editedFeature;
                }
                return feature;
            }),
            modalOpen: false,
            mode: ''
        }))
    }

    render() {
        return (
            <Grid>
                <Grid.Column mobile={16} tablet={8} computer={10}>
                    <Button onClick={() => this.handleOpen('add')}>Add new Feature</Button>
                    <List celled>
                        {
                            this.state.features.map((feature, index) => 
                                <List.Item key={index} onClick={() => this.handleFeatureToEdit(feature)}>
                                    <List.Content>
                                        <List.Header>{feature.title}</List.Header>
                                        {feature.description.substring(0, 100)}...
                                    </List.Content>
                                </List.Item>
                        )}
                    </List>
                    <Modal
                        open={this.state.modalOpen}
                        onClose={this.handleClose}
                    >
                        <Modal.Content scrolling>
                            <Modal.Description>
                                <Header></Header>
                                {this.state.mode === 'add' ? 
                                    <AppFeatureForm 
                                        handleNewFeature={this.handleNewFeature}
                                    /> : (
                                        <AppFeatureForm 
                                            { ...this.state.featureToEdit }
                                            handleEditFeature={this.handleEditFeature}
                                        />
                                    )
                                }

                            </Modal.Description>
                        </Modal.Content>
                    </Modal>
                </Grid.Column>
            </Grid>
        )
    }
}

export default AppFeaturePage;