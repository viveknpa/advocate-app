import React from 'react';
import axios from 'axios';
import { Form, Radio, Segment, Button } from 'semantic-ui-react'

class ApproveAdsPage extends React.Component {
    state = {
        ads: [],
        selectedAdId: '',
        selectedAd: {},
    }

    config = { headers: { 'x-auth': localStorage.getItem('x-auth') } };
    handleAdSelect = (e, { value }) => {
        this.setState((prevState) => ({
            selectedAdId: value,
            selectedAd: prevState.ads.filter((ad) => ad._id === value)[0]
        }))
    }

    onActiveChange = (e) => {
        this.setState((prevState) => ({
            selectedAd: { ...prevState.selectedAd, active: !prevState.selectedAd.active }
        }))
    }

    onApproveChange = (e) => {
        this.setState((prevState) => ({
            selectedAd: { ...prevState.selectedAd, approve: !prevState.selectedAd.approve }
        }))    
    }
    onDurationChange = (e) => {
        const duration = e.target.value;
        this.setState((prevState) => ({
            selectedAd: { ...prevState.selectedAd, duration }
        }))    
    }
    componentWillMount() {
        axios.get('/advertise', this.config).then((response) => {
            this.setState({
                ads: response.data.ads,
                selectedAd: response.data.ads[0] || {},
                selectedAdId: response.data.ads[0] ? response.data.ads[0]._id : ''
            })
        }).catch((e) => {
            alert('Unable to get Ads');
            console.log(e);
        })
    }

    onSubmit = (e) => {
        e.preventDefault();
        const { selectedAd, selectedAdId } = this.state;
        const { duration, approve, active } = selectedAd;
        axios.patch(`/advertise/${selectedAdId}`, {duration, approve, active}, this.config).then((res) => {
            alert('Changes applied!')
            this.setState((prevState) => ({
                ads: prevState.ads.map((ad) => {
                    if(ad._id === selectedAdId) {
                        return selectedAd
                    }
                    return ad;
                })
            }))
        }).catch((e) => {
            console.log(e);
            alert('Unable to apply changes!')
        })
        
    }

    render() {
        const {ads, selectedAd} = this.state;
        return (
            <div>
                <Segment inverted>
                    <Form inverted onSubmit={this.onSubmit}>
                        <Form.Group widths='equal'>
                            <Form.Input 
                                fluid 
                                label='Set duration in Second' 
                                type="number" 
                                style={{ width: '200px' }}
                                value={selectedAd.duration}
                                onChange={this.onDurationChange} 
                            />
                        </Form.Group>
                        <Form.Group inline>
                            <Form.Checkbox 
                                label='approve'
                                checked={selectedAd.approve}
                                onChange={this.onApproveChange}
                            />
                            <Form.Checkbox 
                                label='active'
                                checked={selectedAd.active}
                                onChange={this.onActiveChange}
                            />
                        </Form.Group>
                        <Button color="blue" size="tiny">Save</Button>
                    </Form>
                </Segment>
                { ads.map((ad) => (
                    <div className="ads" key={ad._id}>
                        <div className="ads-heading">
                            <h3>
                                <Radio
                                    name="radioGroup"
                                    value={ad._id}
                                    checked={this.state.selectedAdId === ad._id}
                                    onChange={this.handleAdSelect}
                                    style={{ marginRight: "7px" }}
                                />
                                <span>{ad.title}</span>
                            </h3>
                            <h3>{ad.description}</h3>
                        </div>
                        {ad.fileName &&
                            <img
                                src="https://avatars0.githubusercontent.com/u/26858253?v=4"
                                alt="img"
                                width="90"
                            />
                        }
                    </div>
                ))}
            </div>
        );
    }
}

export default ApproveAdsPage;