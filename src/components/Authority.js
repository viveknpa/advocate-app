import React from 'react';
import axios from 'axios';
import { Form, Button } from 'semantic-ui-react';

class Authority extends React.Component {
    state = {
        userTypes: this.props.userTypes,
        selectedTypeId: this.props.userTypes[0]._id,
        selectedType: this.props.userTypes[0],
        authority: this.props.userTypes[0].authority || {}
    }

    onTypeChange = (e) => {
        const selectedTypeId = e.target.value;
        const selectedType = this.state.userTypes.filter(({ _id }) => _id === selectedTypeId)[0];
        this.setState({
            selectedTypeId,
            selectedType,
            authority: selectedType.authority || {}
        });
    }
    onUsersChange = (e) => {
        const users = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, users }
        }))
    }
    onBulkEmailChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'bulk-email': checked }
        }))
    }
    onCourtChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'add-court': checked }
        }))
    }
    onCourtEventChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'court-event': checked }
        }))
    }
    onProfileChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'profile': checked }
        }))
    }
    onApproveAdsChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'approve-ads': checked }
        }))
    }
    onSupportChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'support-reply': checked }
        }))
    }
    onClientDahsboardChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'ClientDashboard': checked }
        }))
    }
    onEmployeeChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'advocate-employee': checked }
        }))
    }
    onClientChange = (e) => {
        const checked = e.target.checked
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'advocate-client': checked }
        }))
    }
    onCauseListChange = (e) => {
        const checked = e.target.checked
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'CauseList': checked }
        }))
    }
    onPassbookChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'passbook': checked }
        }))
    }
    onTarikhChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'advocate-tarikh': checked }
        }))
    }
    onCalenderChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'advocate-calender': checked }
        }))
    }
    onClientFileChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'advocate-clientfile': checked }
        }))
    }
    onTemplateFileChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'advocate-templatefile': checked }
        }))
    }
    onDocChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'advocate-clientDoc': checked }
        }))
    }
    onMessageChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'advocate-messageClient': checked }
        }))
    }
    onTransferChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'advocate-transferClient': checked }
        }))
    }
    onRequestAdsChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'advocate-ads': checked }
        }))
    }
    onAppFeaturesChange = (e) => {
        const checked = e.target.checked;
        this.setState((prevState) => ({
            authority: { ...prevState.authority, 'app-features': checked }
        }))
    }
    applyAuthority = (e) => {
        e.preventDefault();
        const authority = this.state.authority;
        axios.patch(`/userType/edit/${e.target.userTypeId.value}`, { authority }, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            alert('User type authorised for the choosen fileds');
            this.setState((prevState) => ({
                userTypes: prevState.userTypes.map((userType) => {
                    if (userType._id === this.state.selectedTypeId) {
                        userType.authority = authority;
                        return userType;
                    }
                    return userType;
                })
            }))
        }).catch((e) => {
            console.log(e);
        })
    }
    render() {
        const authority = this.state.authority;
        return (
            <Form onSubmit={this.applyAuthority}>
                <Form.Field inline>
                    <label>Select User Type</label>
                    <select name="userTypeId" value={this.state.selectedTypeId} onChange={this.onTypeChange}>
                        {this.state.userTypes.map((element) =>
                            <option key={element._id} value={element._id}>{element.name}</option>
                        )}
                    </select>
                </Form.Field>
                Manage Users: <input type="checkbox" checked={authority.users} onChange={this.onUsersChange} /><br />
                Bulk Emails: <input type="checkbox" checked={authority['bulk-email']} onChange={this.onBulkEmailChange} /><br />
                Add courts: <input type="checkbox" checked={authority['add-court']} onChange={this.onCourtChange} /><br />
                Court event: <input type="checkbox" checked={authority['court-event']} onChange={this.onCourtEventChange} /><br />
                Create profile: <input type="checkbox" checked={authority['profile']} onChange={this.onProfileChange} /><br />
                Approve advertise: <input type="checkbox" checked={authority['approve-ads']} onChange={this.onApproveAdsChange} /><br />
                Support Reply: <input type="checkbox" checked={authority['support-reply']} onChange={this.onSupportChange} /><br />
                App features: <input type="checkbox" checked={authority['app-features']} onChange={this.onAppFeaturesChange} /><br /><br />
                Select authority Under advocate section: <br />
                Client Dashbaord: <input type="checkbox" checked={authority['ClientDashboard']} onChange={this.onClientDahsboardChange} /><br />
                Associates: <input type="checkbox" checked={authority['advocate-employee']} onChange={this.onEmployeeChange} /><br />
                Clients: <input type="checkbox" checked={authority['advocate-client']} onChange={this.onClientChange} /><br />
                Cause List: <input type="checkbox" checked={authority['CauseList']} onChange={this.onCauseListChange} /><br />
                Passbook: <input type="checkbox" checked={authority['passbook']} onChange={this.onPassbookChange} /><br />
                Tarikhs: <input type="checkbox" checked={authority['advocate-tarikh']} onChange={this.onTarikhChange} /><br />
                Calender events: <input type="checkbox" checked={authority['advocate-calender']} onChange={this.onCalenderChange} /><br />
                Case file: <input type="checkbox" checked={authority['advocate-clientfile']} onChange={this.onClientFileChange} /><br />
                File template: <input type="checkbox" checked={authority['advocate-templatefile']} onChange={this.onTemplateFileChange} /><br />
                Client docs: <input type="checkbox" checked={authority['advocate-clientDoc']} onChange={this.onDocChange} /><br />
                Message client: <input type="checkbox" checked={authority['advocate-messageClient']} onChange={this.onMessageChange} /><br />
                Tranfer Client: <input type="checkbox" checked={authority['advocate-transferClient']} onChange={this.onTransferChange} /><br />
                Request advertise: <input type="checkbox" checked={authority['advocate-ads']} onChange={this.onRequestAdsChange} /><br /><br />

                <Button color="blue" size="tiny">Apply</Button>
            </Form>
        )
    }
}

export default Authority;