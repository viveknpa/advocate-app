import React from 'react';
import { Button, Form, Accordion, Icon, Label } from 'semantic-ui-react';
import { SingleDatePicker } from 'react-dates';
import moment from 'moment';
import axios from 'axios';

class BookAppointment extends React.Component {
    state = {
        activeIndex: 1,
        calenderFocused: false,
        date: moment(),
        startTime: '',
        endTime: '',
        description: ''
    }
    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index
        this.setState({ activeIndex: newIndex })
    }
    onDateChange = (date) => {
        if (date) {
            this.setState(() => ({ date }))
        }
    }
    onFocusChange = ({ focused }) => {
        this.setState(() => ({ calenderFocused: focused }))
    }
    onDescriptionChange = (e) => {
        this.setState({ description: e.target.value })
    }
    onStartTimeChange = (e) => {
        this.setState({ startTime: e.target.value })
    }
    onEndTimeChange = (e) => {
        this.setState({ endTime: e.target.value })
    }
    onSubmit = () => {
        const { date, startTime, endTime, description } = this.state;
        const data = { date, startTime, endTime, description };
        data.date = moment(date).valueOf();
        axios.post(`/appointment/${this.props.appointmentTo}`, data, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then(() => {
            alert(" Appointment Scheduled Successfully! ")
            this.setState({ date: null, startTime: '', endTime: '', description: '' });
        }).catch((e) => {
            alert("Error: Unable to book appointment!")
            console.log(e);
        })
    }
    render() {
        const { activeIndex } = this.state;
        return (
            <Accordion style={{ margin: '10px 0' }}>
                <Accordion.Title active={activeIndex === 0} index={0} onClick={this.handleClick}>
                    <Icon name='dropdown' />
                    <Label color="blue">Schedule an Appointment</Label>
                </Accordion.Title>
                <Accordion.Content active={activeIndex === 0}>
                    <Form style={{ maxWidth: '408px' }} onSubmit={this.onSubmit}>
                        <Form.Group>
                            <Form.Field inline>
                                <label>Pick Date</label>
                                <SingleDatePicker
                                    date={this.state.date}
                                    onDateChange={this.onDateChange}
                                    focused={this.state.calenderFocused}
                                    onFocusChange={this.onFocusChange}
                                    isOutsideRange={() => false}
                                    numberOfMonths={1}
                                />
                            </Form.Field>
                            <Form.Group width="equal">
                                <Form.Input label="Start time" type="time" value={this.state.startTime} onChange={this.onStartTimeChange} style={{ marginBottom: '3px' }} />
                                <Form.Input label="End time" type="time" value={this.state.endTime} onChange={this.onEndTimeChange} />
                            </Form.Group>
                        </Form.Group>
                        <Form.TextArea
                            rows={2}
                            placeholder="write a Description..."
                            value={this.state.description}
                            onChange={this.onDescriptionChange}
                            style={{ marginBottom: '5px' }}
                        />
                        <div style={{ display: 'flex', justifyContent: 'flex-end' }}>
                            <Button
                                color="orange"
                                size="tiny"
                                disabled={this.state.btnDisabled}
                            >
                                Schedule New Tarikh
                        </Button>
                        </div>
                    </Form>
                </Accordion.Content>
            </Accordion>
        )
    }
}

export default BookAppointment;