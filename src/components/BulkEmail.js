import React from 'react';
import { Segment, Form, Label, TextArea } from 'semantic-ui-react'

class BulkEmail extends React.Component {
    state = {
        xtraEmails: ['zxcvbn@de.com', 'yguhnk@ghjk.com'],
        newEmail: ''
    }
    handleChange = (e, { name, value }) => this.setState({ [name]: value })
    handleAddNewEmail = () => {
        this.setState((prevState) => ({
            xtraEmails: [...prevState.xtraEmails, prevState.newEmail],
            newEmail: ''
        }))
    }
    handleSendEmail = (e) => {
        e.preventDefault();
        const heading = e.target.heading.value;
        const content = e.target.content.value;
        const toExtraMails = e.target.toExtraMails.checked;
        const toAdvocates = e.target.toAdvocates.checked;
        if(toExtraMails || toAdvocates) {
            alert('email sent!')
        } else {
            alert('select at least one checkbox')
        }
    }
    render() {
        const { xtraEmails, newEmail } = this.state;
        return (
            <div>
                <Segment>
                    <div style={{ marginBottom: '10px' }}>
                        {
                            xtraEmails.map((email, index) =>
                                <Label
                                    basic
                                    key={index}
                                    style={{ margin: '0 5px 5px 0' }}
                                >
                                    {email}
                                </Label>
                            )}
                    </div>
                    <Form onSubmit={this.handleAddNewEmail}>
                        <Form.Group>
                            <Form.Input
                                placeholder='Add Email'
                                name='newEmail'
                                value={newEmail}
                                onChange={this.handleChange}
                            />
                            <Form.Button
                                primary
                                content='Add Email'
                                disabled={!newEmail}
                            />
                        </Form.Group>
                    </Form>
                </Segment>
                <Segment>
                    <Form onSubmit={this.handleSendEmail}>
                        <Form.Input 
                            label='Heading' 
                            placeholder='Heading' 
                            name='heading'
                            required
                        />
                        <Form.TextArea 
                            label='Content' 
                            placeholder='Content' 
                            name="content"
                            autoHeight
                            required
                        />
                        <Form.Checkbox label="Advocates" name="toAdvocates"/>
                        <Form.Checkbox label="Extra emails" name="toExtraMails"/>
                        <Form.Button>Send</Form.Button>
                    </Form>
                </Segment>
            </div>
        )
    }
}

export default BulkEmail;