import React, { Component } from "react";
import Calendar from "react-big-calendar";
import moment from "moment";
import axios from 'axios';
import { Button, Header, Image, Form, Dropdown, Grid } from 'semantic-ui-react';

import "react-big-calendar/lib/css/react-big-calendar.css";

Calendar.setLocalizer(Calendar.momentLocalizer(moment));

class CalenderPage extends Component {
  state = {
    options: [{
      state: 'delhi',
      cities: ['New Delhi', 'North Delhi']
    }],
    courtEvents: [], //court events from db
    events: [   //contains custom events, case avents and filtered court events
      {
        title: "caseId",
        start: new Date(new Date().setHours(3, 10)),
        end: new Date(new Date().setHours(5, 10))
      }
    ],
    selectedCourt: 'select',
    selectedState: '',
    selectedCity: ''
  };
  config = { headers: { 'x-auth': localStorage.getItem('x-auth') } }

  loadCourtEvents = (court) => {
    axios.get(`/event/${court}`, this.config).then((response) => {
      this.setState((prevState) => ({
        courtEvents: response.data.events,
        events: [...prevState.events.filter((event) => !event.court), ...response.data.events]
      }))
    }).catch((e) => {
      alert('Unable to get events');
      console.log(e);
    })
  }

  handleCourtChange = (e, { value }) => {
    if(value === 'select') {
      return this.setState((prevState) => ({
          events: prevState.events.filter((event) => !event.court),
          selectedCourt: 'select', 
          selectedCity: '', 
          selectedState: '' 
      }))
    }
    this.setState(() => {
      this.loadCourtEvents(value);
      return { selectedCourt: value, selectedCity: '', selectedState: '' }
    });
  }
  handleStateChange = (e, { value }) => {
    if(!value) { return this.setState({ selectedState: '', selectedCity: '' }) };

    this.setState((prevState) => ({
      selectedState: value,
      selectedCity: '',
      events: [...prevState.events.filter((event) => !event.court), ...this.state.courtEvents.filter((event) => event.state === value)]
    }));
  }
  handleCityChange = (e, { value }) => {
    if(!value) { return this.setState({ selectedCity: '' }) };

    this.setState((prevState) => ({
      selectedCity: value,
      events: [...prevState.events.filter((event) => !event.court), ...this.state.courtEvents.filter((event) => event.city === value && event.state === this.state.selectedState)]
    }));
  }

  setEvents = (tarikhs) => {
    let events = [];
    tarikhs.forEach((tarikh) => {
        const { date, startTime, endTime } = tarikh;
        let startTimeArr = startTime.split(":");
        let startHrStr = startTimeArr[0];
        let startMinStr = startTimeArr[1];
        let endTimeArr = endTime.split(":");
        let endHrStr = endTimeArr[0];
        let endMinStr = endTimeArr[1];
        if (startHrStr.length == 2) {
          if (startHrStr.substring(0, 1) === '0') {
            startHrStr = startHrStr.substring(1, 2)
          }
        }
        if (startMinStr.length == 2) {
          if (startMinStr.substring(0, 1) === '0') {
            startMinStr = startMinStr.substring(1, 2)
          }
        }
        if (endHrStr.length == 2) {
          if (endHrStr.substring(0, 1) === '0') {
            endHrStr = endHrStr.substring(1, 2)
          }
        }
        if (endMinStr.length == 2) {
          if (endMinStr.substring(0, 1) === '0') {
            endMinStr = endMinStr.substring(1, 2)
          }
        }
        const startHr = parseInt(startHrStr, 10);
        const startMin = parseInt(startMinStr, 10);
        const endHr = parseInt(endHrStr, 10);
        const endMin = parseInt(endMinStr, 10);
        let event = {
          title: `case no - ${tarikh._client.caseId}`,
          id: tarikh._id,
          start: new Date(new Date(tarikh.date).setHours(startHr, startMin)),
          end: new Date(new Date(tarikh.date).setHours(endHr, endMin)),
        }
        events.push(event);
    })
    this.setState({ events });
  }

  componentDidMount() {
    axios.get('/event', this.config).then((response) => {
      this.setState((prevState) => ({
        events: [...prevState.events, ...response.data.events]
      }))
    }).catch((e) => {
      alert('Unable to get events');
      console.log(e);
    })
  }

  componentWillMount() {
    axios.get(`/tarikh`, this.config).then((response) => {
        this.setEvents(response.data.tarikhs)
    }).catch((e) => {
        alert('Unable to get tarikhs list');
        console.log(e);
    })
    axios.get('/state', this.config).then((response) => {
      this.state.options = response.data.infos;
    }).catch((e) => {
      console.log(e);
      alert('error: Unable to get state inoformations');
    })
  }

  onCreateEvent = (slotInfo) => {
    if (slotInfo.action !== 'select') {
      return;
    }
    const title = prompt("Please enter event title", "Title");
    if(!title) { return }
    const newEvent = {
      title,
      start: moment(slotInfo.start).format(),
      end: moment(slotInfo.end).format()
    }
    axios.post('/event', newEvent, this.config).then((response) => {
      this.setState((prevState) => ({
        events: [...prevState.events, newEvent]
      }))
    }).catch((e) => {
      alert('Unable to add event');
      console.log(e);
    })
  }

  render() {
    let cityOptions = [];
    if(this.state.selectedState) {
      cityOptions = this.state.options.filter((option) => option.state === this.state.selectedState)[0].cities.map((city, index) => {
        return {
          key: index + 1,
          text: city,
          value: city
        }
      }) 
    }

    let stateOptions = this.state.options.map((option, index) => {
      return {
        key: index + 1,
        text: option.state,
        value: option.state
      }
    })
    return (
      <div>
        <Form>
          <Grid>
            <Grid.Column mobile={16} tablet={8} computer={4}>
              <Form.Select
                fluid label='Select Court'
                selection
                onChange={this.handleCourtChange}
                options={[
                  { key: 0, text: 'Select', value: 'select' },
                  { key: 1, text: 'Supreme court', value: 'SC' },
                  { key: 2, text: 'High Court', value: 'HC' },
                  { key: 3, text: 'District Court', value: 'DC' },
                ]}
                value={this.state.selectedCourt}
              />
            </Grid.Column>
            <Grid.Column mobile={16} tablet={8} computer={4}>
              <Form.Select
                placeholder="Select state"
                fluid label='Select State'
                selection
                onChange={this.handleStateChange}
                options={stateOptions}
                value={this.state.selectedState}
              />
            </Grid.Column>
            <Grid.Column mobile={16} tablet={8} computer={4}>
              <Form.Select
                placeholder="Select city"
                fluid label='Select District'
                selection
                onChange={this.handleCityChange}
                options={cityOptions}
                value={this.state.selectedCity}
              />
            </Grid.Column>
          </Grid><br/><br/>
        </Form>
        <Calendar
          style={{ height: "100vh" }}
          popup
          selectable
          defaultView="month"
          defaultDate={new Date()}
          events={this.state.events}
          onSelectSlot={this.onCreateEvent}
        />
      </div>
    );
  }
}

export default CalenderPage;
