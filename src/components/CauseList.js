import React from 'react';
import axios from 'axios';
import moment from 'moment';
import ReactTable from "react-table";
import { SingleDatePicker } from 'react-dates';
import { Icon } from 'semantic-ui-react';

import ViewComment from './common/ViewComment'

export class CauseList extends React.Component {
    state = {
        date: moment(),
        calenderFocused: false,
        causeList: [],
        commentModalShow: false,
        comment: ''
    }
    getCauseList = () => {
        axios.post('/causeList', { date: this.state.date.valueOf() }, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            console.log(response.data.causeList)
            this.setState({ causeList: response.data.causeList })
        }).catch((e) => {
            alert('Unable to get data!')
            console.log(e);
        })
    }
    componentDidMount() {
        this.getCauseList();
    }
    onDateChange = (date) => {
        if (date) {
            this.setState({ date }, this.getCauseList)
        }
    }
    onFocusChange = ({ focused }) => {
        this.setState(() => ({ calenderFocused: focused }))
    }
    showComment = (comment) => this.setState({ commentModalShow: true, comment })
    hideComment = () => this.setState({ commentModalShow: false, comment: '' })
    render() {
        return (
            <div>
                <label style={{ marginRight: '5px', fontWeight: 600 }}>Pick date</label>
                <SingleDatePicker
                    date={this.state.date}
                    onDateChange={this.onDateChange}
                    focused={this.state.calenderFocused}
                    onFocusChange={this.onFocusChange}
                    isOutsideRange={() => false}
                    numberOfMonths={1}
                />
                <br /><br />
                <ReactTable
                    data={this.state.causeList}
                    columns={[
                        {
                            Header: "Case Details",
                            columns: [{
                                Header: "Case Id",
                                accessor: "_client",
                                Cell: ({ value }) => value ? value.caseId : ''
                            }, {
                                Header: "Party 1",
                                accessor: "_client",
                                Cell: ({ value }) => value ? value.fullName : ''
                            }, {
                                Header: "Party 2",
                                accessor: "_client",
                                Cell: ({ value }) => value ? value.antiClient_fullName : ''
                            }]
                        },
                        {
                            Header: "Last Date Details",
                            columns: [{
                                Header: "Date",
                                accessor: "lastDate",
                                Cell: ({ value }) => value ? moment(value.date).format('LL') : ''
                            }, {
                                Header: "Purpose",
                                accessor: "lastDate",
                                Cell: ({ value }) =>
                                    value ?
                                        <Icon
                                            color="violet"
                                            name="comment outline"
                                            onClick={() => this.showComment(value.reason)}
                                        /> : ''
                            }, {
                                Header: "Conclusion",
                                accessor: "lastDate",
                                Cell: ({ value }) =>
                                    value ?
                                        <Icon
                                            color="violet"
                                            name="comment outline"
                                            onClick={() => this.showComment(value.conclusion)}
                                        />
                                        : ''
                            }]
                        },
                        {
                            Header: "Next Date Details",
                            columns: [{
                                Header: "Date",
                                accessor: "nextDate",
                                Cell: ({ value }) => value ? moment(value.date).format('LL') : ''
                            },
                            {
                                Header: "Purpose",
                                accessor: "nextDate",
                                Cell: ({ value }) =>
                                    value ?
                                        <Icon
                                            color="violet"
                                            name="comment outline"
                                            onClick={() => this.showComment(value.reason)}
                                        />
                                        : ''
                            }, {
                                Header: "Conclusion",
                                accessor: "nextDate",
                                Cell: ({ value }) =>
                                    value ?
                                        <Icon
                                            color="violet"
                                            name="comment outline"
                                            onClick={() => this.showComment(value.conclusion)}
                                        />
                                        : ''
                            }]
                        }
                    ]}
                    defaultSorted={[
                        {
                            id: "createdAt",
                            desc: true
                        }
                    ]}
                    defaultPageSize={5}
                    className="-striped -highlight"
                />
                <ViewComment
                    show={this.state.commentModalShow}
                    comment={this.state.comment}
                    hideComment={this.hideComment}
                />
            </div>
        )
    }
}

export default CauseList;