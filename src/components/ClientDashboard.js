import React from "react";
import axios from "axios";
import { Grid, Segment } from "semantic-ui-react";
import { selectClient } from "../actions/selectedClient";
import { connect } from "react-redux";
import moment from "moment";

class ClientDashboard extends React.Component {
  state = {
    data: {
      docs: [],
      files: [],
      messageCount: undefined,
      notAppearedCount: undefined,
      appearedCount: undefined,
      tarikhs: [],
      tarikhsCount: [],
      tarikhsToday: [],
      tarikhsWeek: [],
      tarikhsMonth: [],
      tarikhsYear: []
    }
  };
  config = { headers: { "x-auth": localStorage.getItem("x-auth") } };
  componentDidMount() {
    axios
      .get(
        `/dashboard/client/${this.props.location.state.clientId}`,
        this.config
      )
      .then(response => {
        this.setState({ data: response.data.dashboardData });
        console.log(response);
      })
      .catch(e => {
        console.log(e);
      });
  }
  selectClientAndRedirect = (fileId, filename) => {
    console.log(this.props.location.state.client);
    this.props.selectClient(this.props.location.state.client);
    this.props.history.push({
      pathname: "/dashboard/advocate-clientfile",
      state: {
        fileId,
        filename
      }
    });
  };
  render() {
    const {
      docs,
      files,
      messageCount,
      notAppearedCount,
      appearedCount,
      tarikhs,
      tarikhsCount,
      balance,
      tarikhsToday,
      tarikhsWeek,
      tarikhsMonth,
      tarikhsYear
    } = this.state.data;
    return (
      <Grid>
        <Grid.Column mobile={16} tablet={8} computer={8}>
          <Segment>
            <ul>
              {files.map(({ _id, name }) => (
                <li key={_id}>
                  <span
                    onClick={() => this.selectClientAndRedirect(_id, name)}
                    style={{ color: "blue", cursor: "pointer" }}
                  >
                    {name}
                  </span>
                </li>
              ))}
            </ul>
          </Segment>
        </Grid.Column>
        <Grid.Column mobile={16} tablet={8} computer={8}>
          <Segment>
            <table>
              <th>
                <td>Date</td>
                <td>Appeared</td>
              </th>
              {tarikhs.map(({ date, appeared }) => (
                <tr>
                  <td>{moment(date).format("L")}</td>
                  <td>{appeared ? "YES" : "NO"}</td>
                </tr>
              ))}
            </table>
          </Segment>
        </Grid.Column>
        <Grid.Column mobile={16} tablet={8} computer={8}>
          <Segment>
            <ul>
              {docs.map(({ _id, name, url }) => (
                <li key={_id}>
                  <a href={`/files/${url}`} target="_blank">
                    {name}
                  </a>
                </li>
              ))}
            </ul>
          </Segment>
        </Grid.Column>
        <Grid.Column mobile={16} tablet={8} computer={8}>
          <Segment>
            <ul>
              <li>Total Amount - {balance}</li>
              <li>No of Tarikhs - {tarikhsCount}</li>
              <li>No of Times client appeared - {appearedCount}</li>
              <li>No of Times client not appeared - {notAppearedCount}</li>
              <li>Total SMS Sent - {messageCount}</li>
              <li>Total tarikhs today - {tarikhsToday.length}</li>
              <li>Total tarikhs this week-{tarikhsWeek.length}</li>
              <li>Total tarikhs this month-{tarikhsMonth.length}</li>
              <li>Total tarikhs this year-{tarikhsYear.length}</li>
            </ul>
          </Segment>
        </Grid.Column>
      </Grid>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  selectClient: client => dispatch(selectClient(client))
});

export default connect(
  undefined,
  mapDispatchToProps
)(ClientDashboard);
