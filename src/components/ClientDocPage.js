import React from "react";
import axios from "axios";
import { connect } from "react-redux";
import {
  Button,
  Form,
  Grid,
  Message,
  Icon,
  Tab,
  Loader,
  Modal,
  Header
} from "semantic-ui-react";
import { Link } from "react-router-dom";
import moment from "moment";
import ReactTable from "react-table";

import SearchCase from "./SearchCase";

const initialState = {
  docs: [],
  docName: "",
  docUrl: "",
  message: {},
  loaderActive: false,
  modalOpen: false,
  selectedDocName: "",
  delete: false
};

class ClientDocPage extends React.Component {
  state = initialState;
  config = { headers: { "x-auth": localStorage.getItem("x-auth") } };

  filterCaseInsensitive = (filter, row) => {
    const id = filter.pivotId || filter.id;
    if (!isNaN(row[id])) {
      return row[id] !== undefined
        ? String(row[id]).startsWith(filter.value)
        : true;
    }
    return row[id] !== undefined
      ? String(row[id].toLowerCase()).includes(filter.value.toLowerCase())
      : true;
  };

  getData = clientId => {
    axios
      .get(`/clientdoc/${clientId}`, this.config)
      .then(response => {
        this.setState({ ...initialState, docs: response.data.docs });
      })
      .catch(e => {
        alert("Unable to get clients docs");
        console.log(e);
      });
  };

  componentDidMount() {
    if (!this.props.clientId) {
      return;
    }
    this.getData(this.props.clientId);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.clientId !== this.props.clientId) {
      if (!nextProps.clientId) {
        return;
      }
      this.getData(nextProps.clientId);
    }
  }

  onDocUrlChange = e => {
    this.setState({ docUrl: e.target.files[0] });
  };

  onDocNameChange = e => this.setState({ docName: e.target.value });

  onSubmit = e => {
    e.preventDefault();
    e.persist();
    this.setState({ loaderActive: true });
    let { docName, docUrl } = this.state;
    const fd = new FormData();
    fd.append("file", docUrl);
    fd.append("name", docName);
    axios
      .post(`/clientdoc/${this.props.clientId}`, fd, this.config)
      .then(response => {
        e.target.docUrl.value = "";
        this.setState(prevState => ({
          docs: [...prevState.docs, response.data.doc],
          message: {
            header: `Document ${docName} is uploaded`,
            type: "success"
          },
          docName: "",
          visible: true,
          loaderActive: false
        }));
      })
      .catch(e => {
        this.setState({
          message: {
            header: "Error while uploading document, try again!",
            type: "error"
          },
          visible: true,
          loaderActive: false
        });
        console.log(e);
      });
  };

  handleDeleteDoc = row => {
    axios
      .delete(`/clientdoc/${row.original._id}`, this.config)
      .then(response => {
        const temp = this.state.docs.filter(doc => {
          return doc._id !== response.data.doc._id;
        });
        this.setState({ docs: temp });
      })
      .catch(e => {
        alert("Unable to Delete doc");
        console.log(e);
      });
  };

  handleDismiss = () => {
    this.setState({ visible: false });
  };
  onSelectCase = () => this.setState({ selectCase: true });
  onEndSelectCase = () => this.setState({ selectCase: false });

  hideModal = () => this.setState({ modalOpen: false });
  showModal = () => this.setState({ modalOpen: true });
  selectDoc = name => this.setState({ selectedDocName: name, modalOpen: true });
  render() {
    if (!this.props.caseId) {
      return (
        <Grid>
          <Grid.Column mobile={16} computer={12}>
            <Message warning>
              <Message.Header>
                <p>Search and select a case</p>
                <Form.Input
                  size="mini"
                  icon={<Icon name="search" />}
                  placeholder="Search Case..."
                  onClick={this.onSelectCase}
                />
              </Message.Header>
              <SearchCase
                modalOpen={this.state.selectCase}
                onEndSelectCase={this.onEndSelectCase}
                {...this.props}
              />
            </Message>
          </Grid.Column>
        </Grid>
      );
    }
    const panes = [
      {
        menuItem: "Upload",
        render: () => (
          <Tab.Pane attached={false}>
            <Grid>
              <Grid.Column mobile={16} tablet={8} computer={10}>
                {this.state.visible && (
                  <Message
                    onDismiss={this.handleDismiss}
                    className={this.state.message.type}
                    header={this.state.message.header}
                    content={this.state.message.content}
                  />
                )}
                <Form onSubmit={this.onSubmit}>
                  <Form.Input
                    type="text"
                    name="docName"
                    placeholder="Name of document"
                    value={this.state.docName}
                    onChange={this.onDocNameChange}
                    required
                  />
                  <Form.Input
                    placeholder="Reason"
                    name="docUrl"
                    type="file"
                    onChange={this.onDocUrlChange}
                    required
                  />
                  <Button color="orange" disabled={this.props.caseClosed}>
                    {this.state.loaderActive ? "Uploading" : "Upload"}
                    <Loader
                      active={this.state.loaderActive}
                      inline
                      size="small"
                      style={{ marginLeft: "7px", color: "white" }}
                    />
                  </Button>
                </Form>
              </Grid.Column>
            </Grid>
          </Tab.Pane>
        )
      },
      {
        menuItem: "List",
        render: () => (
          <Tab.Pane attached={false}>
            <ReactTable
              data={this.state.docs}
              columns={[
                {
                  Header: "Date",
                  accessor: "createdAt",
                  Cell: ({ value }) => moment(value).format("LLL"),
                  filterable: true
                },
                {
                  Header: "Document Name",
                  accessor: "name",
                  filterable: true
                },
                {
                  Header: "Doc",
                  accessor: "url",
                  Cell: row => (
                    <a
                      href={`/files/${row.original.url}`}
                      target="internal"
                      onClick={() => this.selectDoc(row.original.name)}
                    >
                      view
                    </a>
                  )
                },
                {
                  Header: "Action",
                  Cell: row => {
                    return (
                      <Button
                        negative
                        size="tiny"
                        style={{ marginLeft: "1.7rem" }}
                        onClick={() => this.handleDeleteDoc(row)}
                      >
                        Delete
                      </Button>
                    );
                  }
                }
              ]}
              defaultFilterMethod={this.filterCaseInsensitive}
              defaultPageSize={7}
              pageSizeOptions={[5, 10, 15]}
              className="-striped -highlight"
            />
          </Tab.Pane>
        )
      }
    ];
    return (
      <Grid>
        <Grid.Column mobile={16} computer={12}>
          <Message info>
            <Message.Header>
              Selected Client: {this.props.clientName}
            </Message.Header>
            <p>
              Case id - {this.props.caseId}{" "}
              {this.props.caseNo && (
                <span>, Case No - {this.props.caseNo}</span>
              )}
            </p>
            <Button color="orange" onClick={this.onSelectCase}>
              Change Client
            </Button>
            <SearchCase
              modalOpen={this.state.selectCase}
              onEndSelectCase={this.onEndSelectCase}
              {...this.props}
            />
          </Message>
          <Tab menu={{ secondary: true }} panes={panes} />
        </Grid.Column>
        <Modal open={this.state.modalOpen} onClose={this.hideModal}>
          <Modal.Content scrolling>
            <Modal.Description>
              <Header>{this.state.selectedDocName}</Header>
              <iframe
                src=""
                name="internal"
                style={{
                  width: "100%",
                  minHeight: "300px",
                  border: "1px solid rgba(34,36,38,.15)"
                }}
              />
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button primary onClick={this.hideModal}>
              Close
            </Button>
          </Modal.Actions>
        </Modal>
      </Grid>
    );
  }
}

const mapStateToProps = ({ selectedClient }) => ({
  clientName: selectedClient.fullName,
  clientId: selectedClient._id,
  caseId: selectedClient.caseId,
  caseNo: selectedClient.caseNo,
  caseClosed: selectedClient.caseClosed
});

export default connect(mapStateToProps)(ClientDocPage);
