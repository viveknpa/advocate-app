import React from "react";
import axios from "axios";
import _ from "lodash";
import { connect } from "react-redux";
import {
  Form,
  Button,
  Input,
  Dropdown,
  Icon,
  Message,
  Grid
} from "semantic-ui-react";
import printJS from "print-js";
import AlertModel from "./AlertModel";

import {
  EditorState,
  convertToRaw,
  convertFromHTML,
  ContentState
} from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import draftToHtml from "draftjs-to-html";

import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import SearchCase from "./SearchCase";

function uploadImageCallBack(file) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest(); // eslint-disable-line no-undef
    xhr.open("POST", "https://api.imgur.com/3/image");
    xhr.setRequestHeader("Authorization", "Client-ID 8d26ccd12712fca");
    const data = new FormData(); // eslint-disable-line no-undef
    data.append("image", file);
    xhr.send(data);
    xhr.addEventListener("load", () => {
      const response = JSON.parse(xhr.responseText);
      resolve(response);
    });
    xhr.addEventListener("error", () => {
      const error = JSON.parse(xhr.responseText);
      reject(error);
    });
  });
}

const initialEditorState = text => {
  return EditorState.createWithContent(
    ContentState.createFromBlockArray(
      convertFromHTML(text || "Write new file...")
    )
  );
};

const initialState = {
  files: [],
  selectedFileId: "",
  fileName: "",
  editorState: initialEditorState(),
  activeButton: "create",
  alertModelOpen: false,
  alertMessage: "",
  templateFiles: [],
  selectedTemplateFileId: ""
};

class ClientFilePage extends React.Component {
  state = initialState;
  config = { headers: { "x-auth": localStorage.getItem("x-auth") } };

  getData = clientId => {
    this.setState({ ...initialState });

    const files = axios
      .get(`/clientfile/${clientId}`, this.config)
      .then(response => {
        this.setState({ files: response.data.files });
      })
      .catch(e => {
        alert("Unable to get client files");
        console.log(e);
      });

    const tempFiles = axios
      .get("/templatefile", this.config)
      .then(response => {
        this.setState({ templateFiles: response.data.files });
      })
      .catch(e => {
        alert("Unable to get template files");
        console.log(e);
      });

    Promise.all([files, tempFiles]).then(() => {
      const { fileId, filename } = this.props.location.state;
      if (fileId) {
        this.onFileChange({}, { value: fileId });
      }
    });
  };
  componentDidMount() {
    if (!this.props.clientId) {
      return;
    }
    this.getData(this.props.clientId);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.clientId !== this.props.clientId) {
      if (!nextProps.clientId) {
        return;
      }
      this.getData(nextProps.clientId);
    }
  }

  onFileNameChange = e => {
    this.setState({ fileName: e.target.value });
  };

  createFile = () => {
    this.setState({
      editorState: initialEditorState(),
      fileName: "",
      selectedFileId: "",
      activeButton: "create"
    });
  };

  onEditorStateChange = editorState => this.setState({ editorState });

  onTemplateFileChange = (e, { value }) => {
    if (!value) {
      return;
    }
    const selectedFile = this.state.templateFiles.filter(
      ({ _id }) => _id === value
    )[0];
    if (confirm("are you sure?")) {
      this.setState({
        editorState: initialEditorState(selectedFile.text),
        selectedTemplateFileId: value
      });
    }
  };

  onFileChange = (e, { value }) => {
    if (!value) {
      return;
    }
    this.setState({ activeButton: "edit" });
    const selectedFile = this.state.files.filter(({ _id }) => _id === value)[0];
    const fileName = selectedFile.name;
    const fileText = selectedFile.text;
    let rawFileText;
    if (convertFromHTML(fileText).contentBlocks) {
      rawFileText = convertFromHTML(fileText);
    } else {
      rawFileText = convertFromHTML("<p>Write...</p>");
    }
    const editorState = EditorState.createWithContent(
      ContentState.createFromBlockArray(rawFileText)
    );
    this.setState({
      selectedFileId: value,
      editorState,
      fileName
    });
  };

  onSave = () => {
    const fileText = draftToHtml(
      convertToRaw(this.state.editorState.getCurrentContent())
    );
    const fileName = this.state.fileName;
    if (!fileName) {
      return alert("Give a name to your file!");
    }
    const fileId = this.state.selectedFileId;
    if (!fileId) {
      return axios
        .post(
          `/clientfile/${this.props.clientId}`,
          { name: fileName, text: fileText },
          this.config
        )
        .then(response => {
          const newFile = response.data.file;
          this.setState(prevState => ({
            files: [newFile, ...prevState.files],
            selectedFileId: newFile._id,
            fileName: newFile.name,
            alertModelOpen: true,
            alertType: "success",
            alertMessage: "File Saved Successfuly!"
          }));
        })
        .catch(e => {
          this.setState({
            alertModelOpen: true,
            alertType: "error",
            alertMessage: "Error: Unable to Save File!"
          });
          console.log(e);
        });
    }

    axios
      .patch(
        `/clientfile/${fileId}`,
        { name: fileName, text: fileText },
        this.config
      )
      .then(response => {
        const editedFile = response.data.file;
        this.setState(prevState => ({
          files: prevState.files.map(file => {
            if (file._id === editedFile._id) {
              return editedFile;
            }
            return file;
          }),
          alertModelOpen: true,
          alertType: "success",
          alertMessage: "File Saved Successfuly!"
        }));
      })
      .catch(e => {
        this.setState({
          alertModelOpen: true,
          alertType: "error",
          alertMessage: "Error: Unable to Save File!"
        });
        console.log(e);
      });
  };
  handlePrintFile = () => {
    printJS({
      printable: "printFileContent",
      type: "html",
      css: "/dist/styles.css"
    });
  };

  onHideAlertModel = () =>
    this.setState({ alertModelOpen: false, alertMessage: "" });

  onSelectCase = () => this.setState({ selectCase: true });
  onEndSelectCase = () => this.setState({ selectCase: false });
  render() {
    if (!this.props.caseId) {
      return (
        <Grid>
          <Grid.Column mobile={16} computer={12}>
            <Message warning>
              <Message.Header>
                <p>Search and select a case</p>
                <Form.Input
                  size="mini"
                  icon={<Icon name="search" />}
                  placeholder="Search Case..."
                  onClick={this.onSelectCase}
                />
              </Message.Header>
              <SearchCase
                modalOpen={this.state.selectCase}
                onEndSelectCase={this.onEndSelectCase}
                {...this.props}
              />
            </Message>
          </Grid.Column>
        </Grid>
      );
    }
    const { alertModelOpen, alertType, alertMessage } = this.state;
    const fileOptions = this.state.files.map(({ _id, name }) => ({
      key: _id,
      value: _id,
      text: name
    }));
    fileOptions.unshift({ key: "select", value: "", text: "Select" });

    const templateOptions = this.state.templateFiles.map(({ _id, name }) => ({
      key: _id,
      value: _id,
      text: name
    }));
    templateOptions.unshift({ key: "select", value: "", text: "Select" });
    return (
      <Grid>
        <Grid.Column mobile={16} computer={12}>
          <Form>
            <Message info>
              <Message.Header>
                Selected Client: {this.props.clientName}
              </Message.Header>
              <p>
                Case id - {this.props.caseId}{" "}
                {this.props.caseNo && (
                  <span>, Case No - {this.props.caseNo}</span>
                )}
              </p>
              <Button color="orange" onClick={this.onSelectCase}>
                Change Client
              </Button>
              <SearchCase
                modalOpen={this.state.selectCase}
                onEndSelectCase={this.onEndSelectCase}
                {...this.props}
              />
            </Message>
            <div style={{ marginBottom: "15px" }}>
              <Button
                style={
                  this.state.activeButton === "create"
                    ? { background: "#634987", color: "white" }
                    : {}
                }
                onClick={this.createFile}
              >
                <Icon name="add" />
                New File
              </Button>
              <Dropdown
                button
                className="icon"
                floating
                labeled
                icon="edit outline"
                options={fileOptions}
                search
                text="Edit file"
                style={
                  this.state.activeButton === "edit"
                    ? { background: "#634987", color: "white" }
                    : {}
                }
                value={this.state.selectedFileId}
                onChange={this.onFileChange}
              />
              <div id="printFileContent" className="printable">
                <h2>
                  {this.state.fileName}
                  <span style={{ marginLeft: "10px" }}>
                    ({this.props.clientName} |{" "}
                    {this.props.caseNo
                      ? `Case number: ${this.props.caseNo}`
                      : `Case id: ${this.props.caseId}`}
                    )
                  </span>
                </h2>
                <div
                  dangerouslySetInnerHTML={{
                    __html: draftToHtml(
                      convertToRaw(this.state.editorState.getCurrentContent())
                    )
                  }}
                />
              </div>
              <Button
                type="button"
                onClick={this.handlePrintFile}
                color="teal"
                style={{ marginLeft: "5px", marginTop: "5px", float: "right" }}
              >
                Print File
              </Button>
            </div>
            <Form.Group>
              <Form.Field>
                <label>Name of file</label>
                <Input
                  style={{ width: "260px" }}
                  type="text"
                  placeholder="Name of file"
                  value={this.state.fileName}
                  onChange={this.onFileNameChange}
                />
              </Form.Field>
              <Form.Field style={{ position: "relative", zIndex: 100 }}>
                <label>Select Template file</label>
                <Dropdown
                  button
                  className="icon"
                  floating
                  labeled
                  icon="paste"
                  options={templateOptions}
                  search
                  text="Select Template"
                  value={this.state.selectedTemplateFileId}
                  onChange={this.onTemplateFileChange}
                />
              </Form.Field>
            </Form.Group>
            <Editor
              editorState={this.state.editorState}
              wrapperClassName="demo-wrapper"
              editorClassName="demo-editor"
              onEditorStateChange={this.onEditorStateChange}
              toolbar={{
                inline: { inDropdown: true },
                list: { inDropdown: true },
                textAlign: { inDropdown: true },
                link: { inDropdown: true },
                history: { inDropdown: true },
                image: {
                  uploadCallback: uploadImageCallBack,
                  previewImage: true,
                  alt: { present: true }
                }
              }}
            />
            <Button
              onClick={this.onSave}
              primary
              size="tiny"
              disabled={this.props.caseClosed}
            >
              Save
            </Button>
          </Form>
          <AlertModel
            open={alertModelOpen}
            alertType={alertType}
            message={alertMessage}
            onHideAlertModel={this.onHideAlertModel}
          />
        </Grid.Column>
      </Grid>
    );
  }
}

const mapStateToProps = ({ selectedClient }) => ({
  clientName: selectedClient.fullName,
  clientId: selectedClient._id,
  caseId: selectedClient.caseId,
  caseNo: selectedClient.caseNo,
  caseClosed: selectedClient.caseClosed
});

export default connect(mapStateToProps)(ClientFilePage);
