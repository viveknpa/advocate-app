import React from "react";
import { Segment, Button, Form, Input } from "semantic-ui-react";

const initialState = {
  fullName: "",
  fatherName: "",
  address: "",
  mobile: "",
  altMobile: "",
  antiClient_fullName: "",
  antiClient_fatherName: "",
  antiClient_address: "",
  antiClient_mobile: "",
  antiClient_altMobile: "",
  subject: "",
  caseNo: "",
  disabled: false
};

class ClientForm extends React.Component {
  constructor(props) {
    super(props);
    const {
      fullName,
      fatherName,
      address,
      mobile,
      altMobile,
      antiClient_fullName,
      antiClient_fatherName,
      antiClient_address,
      antiClient_mobile,
      antiClient_altMobile,
      subject,
      caseNo
    } = props;
    this.state = {
      fullName: fullName || "",
      fatherName: fatherName || "",
      address: address || "",
      mobile: mobile || "",
      altMobile: altMobile || "",
      antiClient_fullName: antiClient_fullName || "",
      antiClient_fatherName: antiClient_fatherName || "",
      antiClient_address: antiClient_address || "",
      antiClient_mobile: antiClient_mobile || "",
      antiClient_altMobile: antiClient_altMobile || "",
      subject: subject || "",
      caseNo: caseNo || "",
      disabled: false
    };
  }
  handleChange = (e, { name, value }) => this.setState({ [name]: value });
  onSubmit = e => {
    e.preventDefault();
    this.setState({ disabled: true });

    const client = this.state;

    this.props
      .onSubmit(client)
      .then(() => {
        if (this.props.action === "register") {
          return this.setState({ ...initialState, disabled: false });
        }
        this.setState({ disabled: false });
      })
      .catch(() => {
        this.setState({ disabled: false });
      });
  };
  render() {
    return (
      <Form onSubmit={this.onSubmit}>
        <Segment.Group>
          <Segment.Group horizontal className="clientForm__segment">
            <Segment>
              <h3>Client information</h3>
              <Form.Input
                name="fullName"
                value={this.state.fullName}
                placeholder="full name"
                onChange={this.handleChange}
                required
              />
              <Form.Input
                name="fatherName"
                value={this.state.fatherName}
                placeholder="Father name"
                onChange={this.handleChange}
                required
              />
              <Form.Input
                name="address"
                value={this.state.address}
                placeholder="Address"
                onChange={this.handleChange}
                required
              />
              <Form.Input
                type="number"
                name="mobile"
                value={this.state.mobile}
                placeholder="Mobile"
                onChange={this.handleChange}
                required
              />
              <Form.Input
                type="number"
                name="altMobile"
                value={this.state.altMobile}
                placeholder="Alt Mobile"
                onChange={this.handleChange}
              />
            </Segment>
            <Segment>
              <h3>Case against</h3>
              <Form.Input
                name="antiClient_fullName"
                value={this.state.antiClient_fullName}
                placeholder="full name"
                onChange={this.handleChange}
                required
              />
              <Form.Input
                name="antiClient_fatherName"
                value={this.state.antiClient_fatherName}
                placeholder="Father name"
                onChange={this.handleChange}
              />
              <Form.Input
                name="antiClient_address"
                value={this.state.antiClient_address}
                placeholder="Address"
                onChange={this.handleChange}
              />
              <Form.Input
                type="number"
                name="antiClient_mobile"
                value={this.state.antiClient_mobile}
                placeholder="Mobile"
                onChange={this.handleChange}
              />
              <Form.Input
                type="number"
                name="antiClient_altMobile"
                value={this.state.antiClient_altMobile}
                placeholder="Alt Mobile"
                onChange={this.handleChange}
              />
            </Segment>
          </Segment.Group>
          <Segment textAlign="center">
            <Form.TextArea
              autoHeight
              name="subject"
              value={this.state.subject}
              placeholder="Subject"
              onChange={this.handleChange}
            />
            <Form.Field
              inline
              style={{
                textAlign: "left",
                display: this.props.action === "edit" ? "block" : "none"
              }}
            >
              <label>Case number</label>
              <Input
                name="caseNo"
                value={this.state.caseNo}
                placeholder="Case no"
                onChange={this.handleChange}
              />
            </Form.Field>
            <Button disabled={this.state.disabled} size="large" color="violet">
              Submit
            </Button>
          </Segment>
        </Segment.Group>
      </Form>
    );
  }
}

export default ClientForm;
