import React from "react";
import { Link } from "react-router-dom";

import ReactTable from "react-table";
import selectTableHOC from "react-table/lib/hoc/selectTable";
import treeTableHOC from "react-table/lib/hoc/treeTable";
const SelectTreeTable = selectTableHOC(treeTableHOC(ReactTable));

import ClientListItem from "./ClientListItem";
import { Checkbox } from "semantic-ui-react";

class ClientList extends React.Component {
  state = {
    selectType: "radio",
    selectedClient: "",
    selectedRowKey: "",
    modalOpen: false
  };

  componentWillReceiveProps(nextProps) {
    if (!this.state.selectedClient) {
      return;
    }
    for (let i = 0; i <= nextProps.clients.length; i++) {
      if (nextProps.clients[i]._id === this.state.selectedClient._id) {
        this.setState({ selectedClient: nextProps.clients[i] });
        break;
      }
    }
  }

  filterCaseInsensitive = (filter, row) => {
    const id = filter.pivotId || filter.id;
    if (!isNaN(row[id])) {
      return row[id] !== undefined
        ? String(row[id]).startsWith(filter.value)
        : true;
    }
    return row[id] !== undefined
      ? String(row[id].toLowerCase()).includes(filter.value.toLowerCase())
      : true;
  };

  hideModal = () => this.setState({ modalOpen: false });

  toggleClientSelection = (key, shift, row) => {
    this.setState({
      selectedRowKey: key,
      selectedClient: row,
      modalOpen: true
    });
  };
  isClientSelected = key => {
    return this.state.selectedRowKey === key;
  };

  render() {
    const {
      isClientSelected: isSelected,
      toggleClientSelection: toggleSelection
    } = this;
    const { selectType, selectedClient, modalOpen } = this.state;
    const extraProps = { isSelected, toggleSelection, selectType };

    const employeeOptions = this.props.employees.map((employee, index) => {
      return {
        key: employee._id,
        text: employee.firstName + " " + employee.lastName,
        value: employee._id
      };
    });
    employeeOptions.unshift({ key: 0, text: "Select employee", value: "" });
    return (
      <div>
        <SelectTreeTable
          data={this.props.clients}
          columns={[
            {
              Header: "Case Id",
              accessor: "caseId",
              filterable: true
            },
            {
              Header: "case No",
              accessor: "caseNo",
              filterable: true
            },
            {
              Header: "Full Name",
              accessor: "fullName",
              filterable: true
            },
            {
              Header: "Mobile Number",
              accessor: "mobile",
              filterable: true
            },
            {
              Header: "Assign To",
              accessor: "assignTo",
              Cell: ({ value }) =>
                value ? value.firstName + " " + value.lastName : "",
              filterable: true
            },
            {
              Header: "Dahsboard",
              Cell: data => (
                <Link
                  to={{
                    pathname: "/dashboard/ClientDashboard",
                    state: {
                      clientId: data.original._id,
                      client: data.original
                    }
                  }}
                >
                  View
                </Link>
              )
            }
          ]}
          defaultFilterMethod={this.filterCaseInsensitive}
          defaultPageSize={7}
          pageSizeOptions={[5, 10, 15]}
          {...extraProps}
          className="-striped -highlight"
        />
        <ClientListItem
          {...selectedClient}
          onEditClient={this.props.onEditClient}
          employeeOptions={employeeOptions}
          modalOpen={modalOpen}
          hideModal={this.hideModal}
          onChangeAssignTo={this.props.onChangeAssignTo}
        />
      </div>
    );
  }
}

export default ClientList;
