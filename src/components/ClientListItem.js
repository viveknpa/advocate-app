import React from "react";
import axios from "axios";
import { Button, Header, Modal, Dropdown, Checkbox } from "semantic-ui-react";
import ReactTable from "react-table";

import ClientForm from "./ClientForm";

class ClientListItemUpdate extends React.Component {
  state = {
    showEditForm: false,
    assignTo: ""
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      assignTo: nextProps.assignTo ? nextProps.assignTo._id : ""
    });
  }

  toggleEditForm = () => {
    this.setState(prevState => ({
      showEditForm: !prevState.showEditForm
    }));
  };

  config = {
    headers: { "x-auth": localStorage.getItem("x-auth") }
  };

  hideModal = () => this.props.hideModal();

  handleCaseClosedChange = () => {
    return axios
      .patch(
        `/client/${this.props._id}`,
        { caseClosed: !this.props.caseClosed },
        this.config
      )
      .then(response => {
        this.props.onEditClient(response.data.client);
        alert("Case Closed Successfully!");
        console.log(response.data.client);
      })
      .catch(e => {
        console.log(e);
        alert("Unable to Close Case!");
      });
  };

  onsentMessagesChange = () => {
    return axios
      .patch(
        `/client/${this.props._id}`,
        { sentMessages: !this.props.sentMessages },
        this.config
      )
      .then(response => {
        this.props.onEditClient(response.data.client);
        alert("Done!");
        console.log(response.data.client);
      })
      .catch(e => {
        console.log(e);
        alert("Error!");
      });
  };

  onSubmit = data => {
    return axios
      .patch(`/client/${this.props._id}`, data, this.config)
      .then(response => {
        this.props.onEditClient(response.data.client);
        alert("Client Updated Successfully!");
      })
      .catch(e => {
        console.log(e);
        alert("Unable to update Client infos!");
      });
  };

  onChangeAssignTo = (e, { value }) => {
    if (!value || value === this.state.assignTo) {
      return;
    }

    const assignTo = value;
    this.props.onChangeAssignTo(this.props._id, assignTo).then(() => {
      this.setState({ assignTo });
    });
  };

  render() {
    const {
      fullName,
      fatherName,
      mobile,
      altMobile,
      address,
      antiClient_fullName,
      antiClient_fatherName,
      antiClient_mobile,
      antiClient_altMobile,
      antiClient_address,
      employeeOptions
    } = this.props;
    const tableData = [
      {
        about: "Client Information",
        fullName,
        fatherName,
        mobile,
        altMobile,
        address
      },
      {
        about: "Case Against",
        fullName: antiClient_fullName,
        fatherName: antiClient_fatherName,
        mobile: antiClient_mobile,
        altMobile: antiClient_altMobile,
        address: antiClient_address
      }
    ];
    return (
      <Modal open={this.props.modalOpen} onClose={this.hideModal}>
        <Modal.Content scrolling>
          <Modal.Description>
            <Header>Case informations</Header>
            {!this.state.showEditForm ? (
              <div>
                <Button
                  color="violet"
                  onClick={this.toggleEditForm}
                  style={{ marginBottom: "10px" }}
                >
                  Edit client informations
                </Button>
                <div style={{ display: "flex", margin: "10px 0" }}>
                  <div style={{ display: "flex", flexDirection: "column" }}>
                    <label style={{ marginBottom: "5px", fontWeight: 600 }}>
                      Assign Employee
                    </label>
                    <Dropdown
                      options={employeeOptions}
                      placeholder="Select Employee"
                      selection
                      value={this.state.assignTo}
                      onChange={this.onChangeAssignTo}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      marginLeft: "20px"
                    }}
                  >
                    <label style={{ marginBottom: "5px", fontWeight: 600 }}>
                      Close Case
                    </label>
                    <Checkbox
                      slider
                      checked={this.props.caseClosed}
                      onChange={this.handleCaseClosedChange}
                    />
                  </div>
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      marginLeft: "20px"
                    }}
                  >
                    <label style={{ marginBottom: "5px", fontWeight: 600 }}>
                      Send SMS
                    </label>
                    <Checkbox
                      slider
                      checked={this.props.sentMessages}
                      onChange={this.onsentMessagesChange}
                    />
                  </div>
                </div>
                <ReactTable
                  data={tableData}
                  columns={[
                    {
                      Header: "About",
                      accessor: "about"
                    },
                    {
                      Header: "Full Name",
                      accessor: "fullName"
                    },
                    {
                      Header: "Father Name",
                      accessor: "fatherName"
                    },
                    {
                      Header: "Mobile Number",
                      accessor: "mobile"
                    },
                    {
                      Header: "Alt Mobile Number",
                      accessor: "altMobile"
                    },
                    {
                      Header: "Address",
                      accessor: "address"
                    }
                  ]}
                  className="-striped -highlight"
                  defaultPageSize={3}
                  showPagination={false}
                />
              </div>
            ) : (
              <div>
                <Button
                  color="violet"
                  onClick={this.toggleEditForm}
                  style={{ marginBottom: "10px" }}
                >
                  Show client informations
                </Button>
                <ClientForm
                  {...this.props}
                  onSubmit={this.onSubmit}
                  changeAssignTo={this.changeAssignTo}
                  action="edit"
                />
              </div>
            )}
          </Modal.Description>
        </Modal.Content>
        <Modal.Actions>
          <Button primary onClick={this.hideModal}>
            Close
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default ClientListItemUpdate;
