import React from 'react';
import axios from 'axios';
import { Tab, Grid } from 'semantic-ui-react';
import RegisterClient from './RegisterClient';
import ClientList from './ClientList';
import { connect } from 'react-redux';

import { selectClient } from '../actions/selectedClient';

export class ClientPage extends React.Component {
    state = {
        clients: [],
        employees: []
    }
    config = { headers: { 'x-auth': localStorage.getItem('x-auth') } }
    componentWillMount() {
        axios.get('/client', this.config).then((response) => {
            this.setState({ clients: response.data.clients });
        }).catch((e) => {
            alert('Unable to get clients list');
            console.log(e);
        })
        axios.get('/employee', this.config).then((response) => {
            this.setState({ employees: response.data.employees });
        }).catch((e) => {
            alert('Unable to get employees list');
            console.log(e);
        })
    }
    onRegisterClient = (newClient) => {
        console.log(newClient);
        this.setState((prevState) => ({
            clients: [...prevState.clients, newClient]
        }))
    }
    onEditClient = (editedClient) => {
        this.setState((prevState) => ({
            clients: prevState.clients.map((client) => {
                if(client._id === editedClient._id) {
                    return editedClient;
                }
                return client
            })
        }))
        if(this.props.selectedClientId === editedClient._id) {
            this.props.dispatch(selectClient(editedClient));
        }
    }
    onChangeAssignTo = (clientId, assignTo) => {
        return axios.patch(`/client/assign/${clientId}`, { assignTo }, this.config).then((response) => {
            alert('Changes Saved!');
            this.setState((prevState) => ({ 
                clients: prevState.clients.map((client) => {
                    if(client._id === clientId) {
                        client.assignTo = response.data.client.assignTo
                    }
                    return client;
                })
             }))
        }).catch((e) => {
            alert('Unable to assign');
            console.log(e);
        })
    }
    render() {
        const panes = [
            { menuItem: 'Register', render: () => <Tab.Pane attached={false}><RegisterClient onRegisterClient={this.onRegisterClient}/></Tab.Pane> },
            { menuItem: 'View', render: () => 
                <Tab.Pane 
                    attached={false}
                >
                    <ClientList 
                        clients={this.state.clients} 
                        employees={this.state.employees} 
                        onEditClient={this.onEditClient}
                        onChangeAssignTo={this.onChangeAssignTo}
                    />
                </Tab.Pane> 
            }
        ]
        return (
            <Grid>
                <Grid.Column mobile={16} tablet={8} computer={14}>
                    <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
                </Grid.Column>
            </Grid>
        )
    }
}

const mapDispatchToProps = (state) => ({
    selectedClientId: state.selectedClient._id,
})

export default connect(mapDispatchToProps)(ClientPage);