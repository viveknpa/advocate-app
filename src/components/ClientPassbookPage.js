import React from "react";
import axios from "axios";
import { connect } from "react-redux";
import {
  Dropdown,
  Button,
  Form,
  Grid,
  Label,
  Message,
  Icon,
  Tab,
  Input
} from "semantic-ui-react";
import ReactTable from "react-table";
import moment from "moment";
import printJS from "print-js";

import SearchCase from "./SearchCase";

const initialState = {
  passbook: { balance: 0, transactions: [] },
  action: "",
  amount: "",
  reason: "",
  message: {},
  visible: false //success or error message in transaction visibility
};

class ClientPassbookPage extends React.Component {
  state = initialState;

  config = { headers: { "x-auth": localStorage.getItem("x-auth") } };
  getData = clientId => {
    axios
      .get(`/passbook/${clientId}`, this.config)
      .then(response => {
        // list of all client passbook
        this.setState({ ...initialState, passbook: response.data.passbook });
      })
      .catch(e => {
        alert("Unable to get clients list");
        console.log(e);
      });
  };
  componentDidMount() {
    if (!this.props.clientId) {
      return;
    }
    this.getData(this.props.clientId);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.clientId !== this.props.clientId) {
      if (!nextProps.clientId) {
        return;
      }
      this.getData(nextProps.clientId);
    }
  }
  onActionChange = (e, { value }) => this.setState({ action: value });
  onAmountChange = e => {
    const amount = e.target.value;
    if (!amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)) {
      this.setState(() => ({ amount }));
    }
  };
  onReasonChange = e => this.setState({ reason: e.target.value });

  handleNewTransaction = e => {
    e.preventDefault();
    let balance = this.state.passbook.balance;
    let { action, reason } = this.state;
    const amount = parseFloat(this.state.amount, 10);
    balance = action === "credit" ? balance + amount : balance - amount;
    axios
      .patch(
        `/passbook/${this.props.clientId}`,
        { action, amount, reason, balance },
        this.config
      )
      .then(response => {
        const currentBalance = response.data.currentBalance;
        this.setState(prevState => ({
          passbook: {
            balance: currentBalance,
            transactions: [
              ...prevState.passbook.transactions,
              { action, amount, reason, balance }
            ]
          },
          action: "",
          amount: "",
          reason: "",
          message: {
            header: "Transaction Successful!",
            content: `Rs ${amount} is ${action}ed to acoount, Current balance is ${currentBalance}`,
            type: "info"
          },
          visible: true
        }));
      })
      .catch(e => {
        this.setState({
          message: {
            header: "Transaction failed",
            type: "error"
          },
          visible: true
        });
        console.log(e);
      });
  };
  handleDismiss = () => {
    this.setState({ visible: false });
  };

  handleDeleteTransaction = row => {
    axios
      .delete(`/passbook/${row.original._id}`, this.config)
      .then(response => {
        const temp = this.state.passbook.transactions.filter(doc => {
          return doc._id !== response.data.doc._id;
        });
        this.setState({
          passbook: { balance: this.state.passbook.balance, transactions: temp }
        });
      })
      .catch(e => {
        alert("Unable to Delete doc");
        console.log(e);
      });
  };

  handlePrintPassbook = () => {
    printJS({
      printable: this.state.passbook.transactions.map(transaction => {
        transaction.date = moment(transaction.createdAt).format("LLL");
        return transaction;
      }),
      header: `Passbook  (${this.props.clientName} | ${
        this.props.caseNo
          ? `Case number: ${this.props.caseNo}`
          : `Case id: ${this.props.caseId}`
      })`,
      properties: ["date", "action", "amount", "reason", "balance"],
      type: "json",
      gridHeaderStyle:
        "color: gray;  border: 1px solid #3971A5;padding: 8px; font-size: 20px; color: blue",
      gridStyle: "border: 1px solid #3971A5;text-align: center;padding: 5px"
    });
  };
  onSelectCase = () => this.setState({ selectCase: true });
  onEndSelectCase = () => this.setState({ selectCase: false });
  render() {
    if (!this.props.caseId) {
      return (
        <Grid>
          <Grid.Column mobile={16} tablet={8} computer={12}>
            <Message warning>
              <Message.Header>
                <p>Search and select a case</p>
                <Form.Input
                  size="mini"
                  icon={<Icon name="search" />}
                  placeholder="Search Case..."
                  onClick={this.onSelectCase}
                />
              </Message.Header>
              <SearchCase
                modalOpen={this.state.selectCase}
                onEndSelectCase={this.onEndSelectCase}
                {...this.props}
              />
            </Message>
          </Grid.Column>
        </Grid>
      );
    }
    const dropdownOptions = [
      { key: 1, text: "Debit", value: "debit" },
      { key: 2, text: "Credit", value: "credit" }
    ];
    const formattedPassbookData = this.state.passbook.transactions.map(
      transaction => {
        transaction.createdAt = moment(transaction.createdAt).format("LLL");
        return transaction;
      }
    );
    const panes = [
      {
        menuItem: "New transaction",
        render: () => (
          <Tab.Pane attached={false}>
            <Grid>
              <Grid.Column mobile={16} tablet={8} computer={8}>
                {this.state.visible && (
                  <Message
                    onDismiss={this.handleDismiss}
                    className={this.state.message.type}
                    header={this.state.message.header}
                    content={this.state.message.content}
                  />
                )}
                <Form onSubmit={this.handleNewTransaction}>
                  <Form.Select
                    placeholder="Action"
                    options={dropdownOptions}
                    value={this.state.action}
                    onChange={this.onActionChange}
                    required
                  />
                  <Form.Input
                    labelPosition="right"
                    type="number"
                    placeholder="Amount"
                    value={this.state.amount}
                    onChange={this.onAmountChange}
                    required
                  >
                    <Label basic>
                      <Icon name="rupee" />
                    </Label>
                    <input />
                    <Label>.00</Label>
                  </Form.Input>
                  <Form.TextArea
                    placeholder="Reason"
                    name="reason"
                    value={this.state.reason}
                    onChange={this.onReasonChange}
                    required
                  />
                  <Button color="orange" disabled={this.props.caseClosed}>
                    Save
                  </Button>
                </Form>
              </Grid.Column>
            </Grid>
          </Tab.Pane>
        )
      },
      {
        menuItem: "Passbook",
        render: () => (
          <Tab.Pane attached={false}>
            <Button
              type="button"
              onClick={this.handlePrintPassbook}
              color="teal"
              size="tiny"
              style={{ marginBottom: "10px" }}
            >
              Print passbook
            </Button>
            <ReactTable
              data={formattedPassbookData}
              columns={[
                {
                  Header: "Date",
                  accessor: "createdAt",
                  minWidth: 160
                },
                {
                  Header: "Action",
                  accessor: "action"
                },
                {
                  Header: "Amount",
                  accessor: "amount"
                },
                {
                  Header: "Reason",
                  accessor: "reason"
                },
                {
                  Header: "Balance",
                  accessor: "balance"
                },
                {
                  Header: "Action",
                  Cell: row => {
                    return (
                      <Button
                        negative
                        size="tiny"
                        style={{ marginLeft: "1.7rem" }}
                        onClick={() => this.handleDeleteTransaction(row)}
                      >
                        Delete
                      </Button>
                    );
                  }
                }
              ]}
              defaultSorted={[
                {
                  id: "createdAt",
                  desc: true
                }
              ]}
              defaultPageSize={7}
              className="-striped -highlight"
            />
          </Tab.Pane>
        )
      }
    ];
    return (
      <Grid>
        <Grid.Column mobile={16} computer={14}>
          <Message info>
            <Message.Header>
              Selected Client: {this.props.clientName}
            </Message.Header>
            <p>
              Case id - {this.props.caseId}{" "}
              {this.props.caseNo && (
                <span>, Case No - {this.props.caseNo}</span>
              )}
            </p>
            <Button color="orange" onClick={this.onSelectCase}>
              Change Client
            </Button>
            <SearchCase
              modalOpen={this.state.selectCase}
              onEndSelectCase={this.onEndSelectCase}
              {...this.props}
            />
          </Message>
          <Tab menu={{ secondary: true }} panes={panes} />
        </Grid.Column>
      </Grid>
    );
  }
}

const mapStateToProps = ({ selectedClient }) => ({
  clientName: selectedClient.fullName,
  clientId: selectedClient._id,
  caseId: selectedClient.caseId,
  caseNo: selectedClient.caseNo,
  caseClosed: selectedClient.caseClosed
});

export default connect(mapStateToProps)(ClientPassbookPage);
