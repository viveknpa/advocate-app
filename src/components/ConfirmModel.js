import React from 'react';

class ConfirmModel extends React.Component {
    handleConfirm = () => this.setState({ result: 'confirmed', open: false })
    handleCancel = () => this.setState({ result: 'cancelled', open: false })
    render() {
        return (
            <Modal 
                size="mini" 
                open={this.props.open} 
                onClose={this.onClose} 
                centered={false}
            >
                <Modal.Content style={{ padding: '40px' }}>
                <h4 style={{ display: 'flex', alignItems: 'center' }}>
                    <div style={{ marginRight: '10px' }}>
                    <Icon 
                        size="large" 
                        name={iconName}
                        color={iconColor}
                    />
                    </div>
                    <span>{this.props.message}</span>
                </h4>
                <div style={{ display: 'flex', justifyContent: 'flex-end', paddingRight: '35px' }}>
                    <Button 
                    size="tiny" 
                    onClick={this.onClose} 
                    primary
                    >
                    OK
                    </Button>
                </div>
                </Modal.Content>
            </Modal>
        )
    }
}