import React from 'react';
import { connect } from 'react-redux';
import AdvocateDashboard from './AdvocateDashboard';

class DashboardPage extends React.Component {
    render() {
        const { userType } = this.props;
        return (
            <div>
                {
                    userType === 9 ? (
                        <AdvocateDashboard />
                    ) : (
                        <div>Welcome to advocate panel</div>
                    )
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    uid: state.auth.uid,
    userType: state.auth.userType,
    authority: state.auth.authority,
})

export default connect(mapStateToProps)(DashboardPage);