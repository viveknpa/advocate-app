import React from 'react';
import { Button, Checkbox, Form, Icon, Input, Grid } from 'semantic-ui-react';
import _ from 'lodash';

const initialState = {
    firstName: '',
    lastName: '',
    email: '',
    mobile: '',
    altMobile: '',
    address: '',
    password: '',
    disabled: false
}

class EmployeeForm extends React.Component {
    constructor(props) {
        super(props);
        const { firstName, lastName, mobile, altMobile, address, password, email } = props;
        this.state = {
            firstName: firstName || '',
            lastName: lastName || '',
            email: email || '',
            mobile: mobile || '',
            altMobile: altMobile || '',
            address: address || '',
            password: password || '',
            disabled: false
        }
    }
    handleChange = (e, { name, value }) => this.setState({ [name]: value });

    onSubmit = (e) => {
        e.preventDefault();
        this.setState({ disabled: true })
        const employee = _.pick(this.state, ['firstName', 'lastName', 'email', 'mobile', 'address', 'altMobile', 'password'])
        this.props.onSubmit(employee).then(() => {
            if(this.props.action === 'register') {
                return this.setState({ ...initialState, disabled: false });
            }
            this.setState({ disabled: false })
        }).catch(() => {
            this.setState({ disabled: false })
        })
    }
    render() {
        const { action } = this.props;
        return (
            <Grid>
                <Grid.Column mobile={16} tablet={8} computer={10}>
                    <Form size="large" onSubmit={this.onSubmit}>
                        <Form.Input
                            name="firstName" 
                            placeholder="First name"  
                            value={this.state.firstName} 
                            onChange={this.handleChange}
                            required
                            />
                        <Form.Input
                            name="lastName" 
                            placeholder="last name"
                            value={this.state.lastName} 
                            onChange={this.handleChange}
                            required 
                            />
                        <Form.Input
                            type="email" 
                            name="email" 
                            placeholder="Email" 
                            value={this.state.email} 
                            onChange={this.handleChange}
                            required 
                            />
                        <Form.Input
                            type="number" 
                            name="mobile" 
                            placeholder="Mobile number" 
                            value={this.state.mobile} 
                            onChange={this.handleChange}
                            required 
                            />
                        <Form.Input
                            type="number" 
                            name="altMobile" 
                            placeholder="Alternate Mobile" 
                            value={this.state.altMobile} 
                            onChange={this.handleChange}
                            />
                        <Form.Input
                            name="address" 
                            placeholder="address" 
                            value={this.state.address} 
                            onChange={this.handleChange}
                            required 
                            />
                        {
                            action === 'register' && (
                                <Form.Input
                                    type="password" 
                                    name="password" 
                                    placeholder="Set a password" 
                                    value={this.state.password} 
                                    onChange={this.handleChange}
                                />
                            )
                        }
                        <Button
                            color="violet"
                            disabled={this.state.disabled}
                        >
                            {action === 'register' ? 'Register Employee' : 'Save'}
                        </Button>
                    </Form>
                </Grid.Column>
            </Grid>
        )
    }
}

export default EmployeeForm;