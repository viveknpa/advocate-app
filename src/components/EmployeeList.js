import React from 'react';
import axios from 'axios';
import ReactTable from "react-table";
import {  Button, Modal, Header } from 'semantic-ui-react'
import AlertModel from './AlertModel';

import EmployeeForm from './EmployeeForm';

class EmployeeList extends React.Component {
    state = {
        modalOpen: false,
        alertModelOpen: false,
        alertMessage: ''
    }

    handleClose = () => this.setState({ modalOpen: false })

    filterCaseInsensitive = (filter, row) => {
        const id = filter.pivotId || filter.id;
        if (!isNaN(row[id])) {
            return row[id] !== undefined ? String(row[id]).startsWith(filter.value) : true
        }
        return row[id] !== undefined ? String(row[id].toLowerCase()).includes(filter.value.toLowerCase()) : true
    }

    handleSelectEmployee = (selectedEmployee) => {
        this.setState({ selectedEmployee, modalOpen: true })
    }

    onSubmit = (data) => { // edit employee
        return axios.patch(`/employee/${this.state.selectedEmployee._id}`, data, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            this.setState({ 
                alertModelOpen: true, 
                alertType: 'success',
                alertMessage: 'Changes Saved Successfully!'
            })
            this.props.onEditEmployee(response.data.employee);
            this.handleClose();
        }).catch((e) => {
            console.log(e);
            this.setState({ 
                alertModelOpen: true, 
                alertType: 'error',
                alertMessage: 'Unable to update employee!'
            })
        })
    }
    onHideAlertModel = () => this.setState({ alertModelOpen: false })

    render() {
        return (
            <div>
                <ReactTable
                    data={this.props.employees}
                    columns={[
                    {
                        Header: "Full Name",
                        accessor: "firstName",
                        Cell: row => (
                            <span 
                                style={{color: 'blue', cursor: 'pointer'}}
                                onClick={() => this.handleSelectEmployee(row.original)}
                            >
                                {row.original.firstName +' '+ row.original.lastName}
                            </span>
                        ),
                        filterable: true
                    },
                    {
                        Header: "Email Id",
                        accessor: "email",
                        filterable: true
                    },
                    {
                        Header: "Mobile",
                        accessor: "mobile",
                        filterable: true
                    },
                    {
                        Header: "Alt Mobile",
                        accessor: "altMobile",
                        filterable: true
                    },
                    {
                        Header: "Address",
                        accessor: "address",
                        filterable: true
                    }]}
                    defaultFilterMethod={this.filterCaseInsensitive}
                    defaultPageSize={7}
                    pageSizeOptions={[5, 10, 15]}
                    className="-striped -highlight"
                />
                <Modal
                    open={this.state.modalOpen}
                    onClose={this.handleClose}
                >
                    <Modal.Content scrolling>
                        <Modal.Description>
                            <Header>Edit employee informations</Header>
                            <EmployeeForm
                                {...this.state.selectedEmployee}
                                action="edit"
                                onSubmit={this.onSubmit}
                            />
                        </Modal.Description>
                    </Modal.Content>
                </Modal>
                <AlertModel 
                    open={this.state.alertModelOpen}
                    alertType={this.state.alertType}
                    message={this.state.alertMessage}
                    onHideAlertModel={this.onHideAlertModel}
                />
            </div>
        )
    }
}

export default EmployeeList;