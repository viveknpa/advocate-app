import React from 'react';
import axios from 'axios';
import { Tab, Grid } from 'semantic-ui-react';
import RegisterEmployee from './RegisterEmployee';
import EmployeeList from './EmployeeList';

class EmployeePage extends React.Component {
    state = {
        employees: []
    }
    config = { headers: { 'x-auth': localStorage.getItem('x-auth') } }
    componentWillMount() {
        axios.get('/employee', this.config).then((response) => {
            this.setState({ employees: response.data.employees });
        }).catch((e) => {
            alert('Unable to get employees list');
            console.log(e);
        })
    }
    onRegisterEmployee = (newEmployee) => {
        this.setState((prevState) => ({
            employees: [newEmployee, ...prevState.employees]
        }))
    }

    onEditEmployee = (editedEmployee) => {
        this.setState((prevState) => ({
            employees: prevState.employees.map((employee) => {
                if(employee._id === editedEmployee._id) {
                    return editedEmployee;
                }
                return employee
            })
        }))
    }
    render() {
        const panes = [
            { menuItem: 'Register', render: () => <Tab.Pane attached={false}><RegisterEmployee onRegisterEmployee={this.onRegisterEmployee}/></Tab.Pane> },
            { menuItem: 'View', render: () => <Tab.Pane attached={false}><EmployeeList employees={this.state.employees} onEditEmployee={this.onEditEmployee}/></Tab.Pane> }
        ]
        return (
            <Grid>
                <Grid.Column mobile={16} tablet={8} computer={12}>
                    <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
                </Grid.Column>
            </Grid>
        )
    }
}

export default EmployeePage;