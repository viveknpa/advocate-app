import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { Button, Form, Icon, Input } from "semantic-ui-react";
class ForgotPassword extends React.Component {
  state = {
    linkSent: false,
    error: ""
  };
  handleSubmit = e => {
    e.preventDefault();
    axios
      .post("/sendVerificationCode", {
        action: "forgot",
        email: e.target.email.value
      })
      .then(() => {
        this.setState({ error: "" });
        this.setState({ linkSent: true });
      })
      .catch(e => {
        if (e.response.status == 404) {
          return this.setState({ error: "Account not found" });
        }
        this.setState({ error: "invalid email" });
      });
  };
  render() {
    return (
      <div className="box-layout">
        <div className="box-layout__box">
          <h1 className="box-layout__title">Forgot your password?</h1>
          <h3 className="box-layout__title">
            Please enter your email address to search for your account.
          </h3>
          <Form
            size="big"
            className="box-layout-form"
            onSubmit={this.handleSubmit}
          >
            <Form.Field>
              <Input
                iconPosition="left"
                type="email"
                name="email"
                placeholder="Email address"
                required
              >
                <Icon name="user" size="small" />
                <input />
              </Input>
            </Form.Field>
            <Button
              fluid
              size="big"
              style={{ background: "#865493", color: "white" }}
            >
              Send me Email
            </Button>
            <div className="box-layout__title">
              It may take several minutes to receive a password reset email.
              Make sure to check your junk mail.
            </div>
          </Form>
        </div>
      </div>
    );
  }
}

export default ForgotPassword;
