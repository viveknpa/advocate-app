import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Checkbox, Form, Icon, Input } from 'semantic-ui-react';

class LoginForm extends React.Component {
    state = {
        disabled: false
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ disabled: false })
    }
    onLoginSubmit = (e) => {
        e.preventDefault();
        this.setState({ disabled: true })
        this.props.startLogin({
            email: e.target.email.value,
            password: e.target.password.value
        })
    }
    render() {
        return (
            <div className="box-layout">
                <div className="box-layout__box">
                    <h1 className="box-layout__title">Login to your account</h1>
                    <Form size="big" className="box-layout-form" onSubmit={this.onLoginSubmit}>
                        {this.props.loginError && <span className="box-layout-form__error"> {this.props.loginError} </span>}
                        <Form.Field>
                            <Input
                                iconPosition="left"
                                type="email"
                                name="email"
                                placeholder="Email address"
                                required
                            >
                                <Icon name="user" size="small" />
                                <input />
                            </Input>
                        </Form.Field>
                        <Form.Field>
                            <Input iconPosition="left" type="password" name="password" placeholder="Password">
                                <Icon name="lock" size="small" />
                                <input />
                            </Input>
                        </Form.Field>
                        <Button
                            fluid
                            size="big"
                            style={{ background: "#865493", color: "white" }}
                            disabled={this.state.disabled}
                        >
                            Log In
                        </Button>
                        <h3 className="box-layout-form__links">
                            Need an account?
                            <span onClick={this.props.handleFormDisplay}>Signup</span>
                            <span style={{margin: '0 10px'}}>-</span>
                            <Link to="/forgot">forgot password?</Link>
                        </h3>
                    </Form>
                    <div className="box-layout__footer">
                        <Link to="/advocate-profile/search">Search An Advocate</Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default LoginForm;