import React from 'react';
import { connect } from 'react-redux';
import { startSignUp, startLogin } from '../actions/auth';
import LoginForm from './LoginForm';
import SignupForm from './SignupForm';
export class LoginPage extends React.Component {
    state = {
        showSignupForm: true,
        isOpen: false
    }
    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    handleFormDisplay = () => {
        this.setState((prevState) => ({
            showSignupForm: !prevState.showSignupForm
        }))
    }
    render() {
        return (
            <div>
                {this.state.showSignupForm ? (
                    <SignupForm
                        startSignUp={this.props.startSignUp}
                        signUpError={this.props.signUpError}
                        handleFormDisplay={this.handleFormDisplay}
                    />

                ) : (   
                    <LoginForm
                        startLogin={this.props.startLogin}
                        loginError={this.props.loginError}
                        handleFormDisplay={this.handleFormDisplay}
                    />  
                )}

            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    loginError: state.auth.error.loginError,
    signUpError: state.auth.error.signUpError
})

const mapDispatchToProps = (dispatch) => ({
    startSignUp: (user) => dispatch(startSignUp(user)),
    startLogin: (user) => dispatch(startLogin(user))
})

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);