import React from 'react';
import { NavLink, Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { startLogout } from '../actions/auth';
import jQuery from 'jquery';
import moment from 'moment';

import { Sidebar, Segment, Button, Menu, Image, Icon, Header, Dropdown, Divider } from 'semantic-ui-react';

class SideNav extends React.Component {
  state = { visible: true, activeItem: "account", animation: "push" };

  handleItemClick = (e, { name }) => this.setState({ activeItem: name });
  toggleVisibility = () => this.setState({ visible: !this.state.visible });

  componentWillMount() {
    jQuery(window).resize(() => {
      if (jQuery(window).width() < 768) {
        this.setState({ visible: false, animation: 'overlay' })
      } else {
        this.setState({ visible: true, animation: 'push' })
      }
    })

    if (jQuery(window).width() < 768) {
      this.setState({ visible: false, animation: 'overlay' })
    }
    else {
      this.setState({ visible: true, animation: 'push' })
    }
  }

  render() {
    const { visible } = this.state;
    const { activeItem } = this.state;
    const authority = this.props.authority;
    return (
      <div style={{ display: 'flex', flexDirection: 'column', height: '100vh' }}>
        <Segment style={{ paddingTop: 0, paddingBottom: 0, margin: 0, borderBottom: '2px solid #9D81C3' }}>
          <Menu secondary>
            <Menu.Item style={{ padding: 0 }}>
              <i
                className="sidebar icon show-for-mobile"
                onClick={this.toggleVisibility}
              ></i>
              <Link to="/dashboard">
                <img
                  src='/files/logo.png'
                  style={{ width: '190px', height: '75px', paddingBottom: '5px' }}
                />
              </Link>
            </Menu.Item>
            <Menu.Menu position='right'>
              <Menu.Item
                className="show-for-desktop"
                name='Support'
                as={NavLink}
                to="/dashboard/support"
              />
              <Menu.Item>
                <Dropdown item text='Account'>
                  <Dropdown.Menu>
                    {authority['profile'] && <Dropdown.Item as={NavLink} to={`/advocate-profile/${this.props.uid}`}>Edit Account</Dropdown.Item>}
                    <Dropdown.Item as={NavLink} to="/reset/password">Change password</Dropdown.Item>
                    <Dropdown.Item className="show-for-mobile" onClick={this.props.startLogout}>Logout</Dropdown.Item>
                    <Divider />
                    <Dropdown.Item><span style={{ fontWeight: 'bold' }}>Account expires on<br />{moment(this.props.accountExpiresOn).format('LLL')}</span></Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown>
              </Menu.Item>
              <Menu.Item className="show-for-desktop"><Button primary onClick={this.props.startLogout}>Logout</Button></Menu.Item>
            </Menu.Menu>
          </Menu>
        </Segment>
        <Sidebar.Pushable as={Segment} style={{ margin: 0, borderTop: 0 }}>
          <Sidebar
            as={Menu}
            animation={this.state.animation}
            width="thin"
            visible={visible}
            icon="labeled"
            vertical
            inverted
          >
            {authority['users'] && <Menu.Item
              name="users"
              as={NavLink}
              to="/dashboard/users"
              active={activeItem === "users"}
              onClick={this.handleItemClick}
            />}
            {authority['add-court'] && <Menu.Item
              name="add court"
              as={NavLink}
              to="/dashboard/add-court"
              active={activeItem === "add court"}
              onClick={this.handleItemClick}
            />}
            {authority['app-features'] && <Menu.Item
              name="App features"
              as={NavLink}
              to="/dashboard/app-features"
              active={activeItem === "App features"}
              onClick={this.handleItemClick}
            />}
            {authority['support-reply'] && <Menu.Item
              name="Support reply"
              as={NavLink}
              to="/dashboard/support-reply"
              active={activeItem === "Support reply"}
              onClick={this.handleItemClick}
            />}
            {authority['court-event'] && <Menu.Item
              name="Court events"
              as={NavLink}
              to="/dashboard/court-event"
              active={activeItem === "Court events"}
              onClick={this.handleItemClick}
            />}
            {authority['advocate-employee'] && <Menu.Item
              name="Associates"
              as={NavLink}
              to="/dashboard/advocate-employee"
              active={activeItem === "Employee"}
              onClick={this.handleItemClick}
            />}
            {authority['advocate-client'] && <Menu.Item
              name="clients"
              as={NavLink}
              to="/dashboard/advocate-client"
              active={activeItem === "clients"}
              onClick={this.handleItemClick}
            />}
            {authority['CauseList'] && <Menu.Item
              name="Cause List"
              as={NavLink}
              to="/dashboard/CauseList"
              active={activeItem === "Cause List"}
              onClick={this.handleItemClick}
            />}
            {authority['passbook'] && <Menu.Item
              name="My Passbook"
              as={NavLink}
              to="/dashboard/passbook/advocate"
              active={activeItem === "My Passbook"}
              onClick={this.handleItemClick}
            />}
            {authority['passbook'] && <Menu.Item
              name="Client Passbook"
              as={NavLink}
              to="/dashboard/passbook/client"
              active={activeItem === "Client Passbook"}
              onClick={this.handleItemClick}
            />}
            {authority['advocate-tarikh'] && <Menu.Item
              name="tarikh"
              as={NavLink}
              to="/dashboard/advocate-tarikh"
              active={activeItem === "tarikh"}
              onClick={this.handleItemClick}
            />}
            {authority['advocate-calender'] && <Menu.Item
              name="calender"
              as={NavLink}
              to="/dashboard/advocate-calender"
              active={activeItem === "calender"}
              onClick={this.handleItemClick}
            />}
            {authority['advocate-clientfile'] && <Menu.Item
              name="Case file"
              as={NavLink}
              to="/dashboard/advocate-clientfile"
              active={activeItem === "Case file"}
              onClick={this.handleItemClick}
            />}
            {authority['advocate-templatefile'] && <Menu.Item
              name="Template file"
              as={NavLink}
              to="/dashboard/advocate-templatefile"
              active={activeItem === "Template file"}
              onClick={this.handleItemClick}
            />}
            {authority['advocate-clientDoc'] && <Menu.Item
              name="Client documents"
              as={NavLink}
              to="/dashboard/advocate-clientDoc"
              active={activeItem === "Client documents"}
              onClick={this.handleItemClick}
            />}
            {authority['advocate-messageClient'] && <Menu.Item
              name="message client"
              as={NavLink}
              to="/dashboard/advocate-messageClient"
              active={activeItem === "message client"}
              onClick={this.handleItemClick}
            />}
            {authority['advocate-transferClient'] && <Menu.Item
              name="transfer case"
              as={NavLink}
              to="/dashboard/advocate-transferClient"
              active={activeItem === "transfer case"}
              onClick={this.handleItemClick}
            />}
            {authority['advocate-ads'] && <Menu.Item
              name="request ads"
              as={NavLink}
              to="/dashboard/advocate-ads"
              active={activeItem === "request ads"}
              onClick={this.handleItemClick}
            />}
            {authority['approve-ads'] && <Menu.Item
              name="approve ads"
              as={NavLink}
              to="/dashboard/approve-ads"
              active={activeItem === "approve ads"}
              onClick={this.handleItemClick}
            />}
            {authority['bulk-email'] && <Menu.Item
              name="emails"
              as={NavLink}
              to="/dashboard/bulk-email"
              active={activeItem === "emails"}
              onClick={this.handleItemClick}
            />}
            <Menu.Item
              name="Support"
              className="show-for-mobile"
              as={NavLink}
              to="/dashboard/support"
              active={activeItem === "Support"}
              onClick={this.handleItemClick}

            />
          </Sidebar>
          <Sidebar.Pusher style={{ paddingBottom: '50px' }}>
            <Segment basic className="customSegment">
              {this.props.children}
            </Segment>
          </Sidebar.Pusher>
        </Sidebar.Pushable>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  uid: state.auth.uid,
  userType: state.auth.userType,
  authority: state.auth.authority,
  accountExpiresOn: state.auth.accountExpiresOn
})

const mapDispatchToProps = (dispatch) => ({
  startLogout: () => dispatch(startLogout())
})

export default connect(mapStateToProps, mapDispatchToProps)(SideNav);