import React, { Component } from "react";
import axios from "axios";
import {
  Accordion,
  Icon,
  Label,
  Form,
  Button,
  List,
  Message
} from "semantic-ui-react";
import { connect } from "react-redux";
import moment from "moment";

import AlertModel from "./AlertModel";
import SearchCase from "./SearchCase";

class MessageClientPage extends Component {
  state = {
    messages: [],
    activeIndex: 1,
    selectedMobile: this.props.mobile,
    text: "",
    disabled: false,
    alertModelOpen: false,
    alertMessage: "",
    selectedmode: "Old",
    display: "none",
    selectedMob: ""
  };

  config = { headers: { "x-auth": localStorage.getItem("x-auth") } };

  getData = clientId => {
    axios
      .get(`/sms/${clientId}`, this.config)
      .then(response => {
        this.setState({ messages: response.data.messages });
      })
      .catch(e => {
        this.setState({
          alertModelOpen: true,
          alertType: "error",
          alertMessage: "Unable to Get Messages!"
        });
        console.log(e);
      });
  };
  componentDidMount() {
    if (!this.props.clientId) {
      return;
    }
    this.getData(this.props.clientId);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.clientId !== this.props.clientId) {
      if (!nextProps.clientId) {
        return;
      }
      this.getData(nextProps.clientId);
    }
    if (nextProps.mobile !== this.state.selectedMobile) {
      this.setState({ selectedMobile: nextProps.mobile });
    }
  }

  handleClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? -1 : index;
    this.setState({ activeIndex: newIndex });
  };

  handleChange = (e, { name, value }) => {
    this.setState({ [name]: value });
  };

  handleChangeMob = (e, prop) => {
    this.setState({ selectedmode: prop.value });
  };

  mobileChange = (e, prop) => {
    this.setState({ selectedMobile: prop.value, selectedMob: prop.value });
  };

  sendSms = e => {
    const { selectedMobile: mobile, text } = this.state;
    if (!text) {
      return alert("Please enter message");
    }
    this.setState({ disabled: true });
    axios
      .post(`/sms/${this.props.clientId}`, { mobile, text }, this.config)
      .then(response => {
        this.setState(prevState => ({
          messages: [
            { text, createdAt: moment().valueOf() },
            ...prevState.messages
          ],
          text: "",
          disabled: false,
          alertModelOpen: true,
          alertType: "success",
          alertMessage: "Message Sent Successfully!"
        }));
      })
      .catch(e => {
        this.setState({
          disabled: false,
          alertModelOpen: true,
          alertType: "error",
          alertMessage: "Unable to send message, try again!"
        });
        console.log(e);
      });
  };
  onHideAlertModel = () => this.setState({ alertModelOpen: false });

  onSelectCase = () => this.setState({ selectCase: true });
  onEndSelectCase = () => this.setState({ selectCase: false });
  render() {
    if (!this.props.caseId) {
      return (
        <Message warning style={{ maxWidth: "500px" }}>
          <Message.Header>
            <p>Search and select a case</p>
            <Form.Input
              size="mini"
              icon={<Icon name="search" />}
              placeholder="Search Case..."
              onClick={this.onSelectCase}
            />
          </Message.Header>
          <SearchCase
            modalOpen={this.state.selectCase}
            onEndSelectCase={this.onEndSelectCase}
            {...this.props}
          />
        </Message>
      );
    }
    const { activeIndex, alertModelOpen, alertType, alertMessage } = this.state;
    const { mobile, altMobile } = this.props;

    return (
      <div style={{ maxWidth: "500px" }}>
        <Message info>
          <Message.Header>
            Selected Client: {this.props.clientName}
          </Message.Header>
          <p>
            Case id - {this.props.caseId}{" "}
            {this.props.caseNo && <span>, Case No - {this.props.caseNo}</span>}
          </p>
          <Button color="orange" onClick={this.onSelectCase}>
            Change Client
          </Button>
          <SearchCase
            modalOpen={this.state.selectCase}
            onEndSelectCase={this.onEndSelectCase}
            {...this.props}
          />
        </Message>
        <Accordion>
          <Accordion.Title
            active={activeIndex === 0}
            index={0}
            onClick={this.handleClick}
          >
            <Icon name="dropdown" />
            <Label color="blue">Send New Message</Label>
          </Accordion.Title>
          <Accordion.Content active={activeIndex === 0}>
            <Form style={{ maxWidth: "400px" }} onSubmit={this.sendSms}>
              <Form.Select
                label="Select Mode"
                name="selectedmode"
                options={[
                  {
                    key: 0,
                    value: "Old",
                    text: "Old Number"
                  },
                  {
                    key: 1,
                    value: "New",
                    text: "New Number"
                  }
                ]}
                value={this.state.selectedmode}
                onChange={this.handleChangeMob}
              />
              {this.state.selectedmode === "New" && (
                <Form.Input
                  placeholder="Enter Mobile:"
                  name="mobile"
                  onChange={this.mobileChange}
                  value={this.state.selectedMob}
                />
              )}
              {this.state.selectedmode === "Old" && (
                <Form.Select
                  label="Select Mobile"
                  name="selectedMobile"
                  options={[
                    {
                      key: 0,
                      value: mobile,
                      text: mobile
                    },
                    {
                      key: 1,
                      value: altMobile,
                      text: altMobile
                    }
                  ]}
                  value={this.state.selectedMobile}
                  onChange={this.handleChange}
                />
              )}
              <Form.TextArea
                autoHeight
                name="text"
                placeholder="Write message..."
                style={{ minHeight: 70 }}
                value={this.state.text}
                onChange={this.handleChange}
              />
              <div style={{ display: "flex", justifyContent: "flex-end" }}>
                <Button
                  color="orange"
                  disabled={this.props.caseClosed || this.state.disabled}
                >
                  Send
                </Button>
              </div>
            </Form>
          </Accordion.Content>
        </Accordion>
        <List celled>
          {this.state.messages.map((message, index) => (
            <List.Item key={index}>
              <List.Content>
                <List.Header>{message.text}</List.Header>
                {moment(message.createdAt).format("LLL")}
              </List.Content>
            </List.Item>
          ))}
        </List>
        <AlertModel
          open={alertModelOpen}
          alertType={alertType}
          message={alertMessage}
          onHideAlertModel={this.onHideAlertModel}
        />
      </div>
    );
  }
}

const mapStateToProps = ({ selectedClient }) => ({
  clientName: selectedClient.fullName,
  clientId: selectedClient._id,
  caseId: selectedClient.caseId,
  caseNo: selectedClient.caseNo,
  mobile: selectedClient.mobile,
  altMobile: selectedClient.altMobile,
  caseClosed: selectedClient.caseClosed
});

export default connect(mapStateToProps)(MessageClientPage);
