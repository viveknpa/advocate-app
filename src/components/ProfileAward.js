import React, { Component } from "react";
import { render } from "react-dom";
import { Form, Button} from "semantic-ui-react";

const monthOptions = [
    { key: 1, value: 'january', text: 'January' },
    { key: 2, value: 'february', text: 'February' },
    { key: 3, value: 'march', text: 'March' },
    { key: 4, value: 'april', text: 'April' },
    { key: 5, value: 'may', text: 'May' },
    { key: 6, value: 'june', text: 'June' },
    { key: 7, value: 'july', text: 'July' },
    { key: 8, value: 'august', text: 'August' },
    { key: 9, value: 'september', text: 'September' },
    { key: 10, value: 'october', text: 'October' },
    { key: 11, value: 'november', text: 'November' },
    { key: 12, value: 'december', text: 'December' }
];

const yearOptions = [];
for (let i = 2018; i >= 1955; i--) {
    yearOptions.push({ key: i, text: i, value: i });
}

class ProfileAward extends Component {
    state = {
        title: this.props.title ? this.props.title : "",
        issuer: this.props.issuer ? this.props.issuer : "",
        month: this.props.month ? this.props.month : "",
        year: this.props.year ? this.props.year : "",
        description: this.props.description ? this.props.description : "",
    };
    onTitleChange = e => this.setState({ title: e.target.value });
    onIssuerChange = e => this.setState({ issuer: e.target.value });
    onDescriptionChange = e => this.setState({ description: e.target.value });
    onMonthChange = (e, { value }) => this.setState({ month: value });
    onYearChange = (e, { value }) => this.setState({ year: value });

    onSubmit = e => {
        e.preventDefault();
        const { title, issuer, description, month, year } = this.state;
        let newAward = { title, issuer, description, month, year };
        let awardArray;
        if (this.props.award && this.props.award.length) {
            let idFound = 0;
            awardArray = this.props.award.map((award) => {
                if (award._id === this.props._id) {
                    idFound = 1;
                    return newAward;
                }
                return award;
            })
            if(!idFound) {
                awardArray.push(newAward);
            }
        } else {
            awardArray = [newAward];
        }
        this.props.onEdit('award', { award: awardArray });
    };

    render() {
        return (
            <div style={{padding: '20px'}}>
                <Form onSubmit={this.onSubmit}>
                    <Form.Input
                        fluid
                        label="Title"
                        value={this.state.title}
                        onChange={this.onTitleChange}
                    />
                    <Form.Input
                        fluid
                        label="Issuer"
                        value={this.state.issuer}
                        onChange={this.onIssuerChange}
                    />
                    <Form.Group widths="equal">
                        <Form.Select
                            fluid
                            label="Month"
                            placeholder="Year"
                            options={monthOptions}
                            value={this.state.month}
                            onChange={this.onMonthChange}
                        />
                        <Form.Select
                            fluid
                            label="Year"
                            placeholder="Year"
                            options={yearOptions}
                            value={this.state.year}
                            onChange={this.onYearChange}
                        />
                    </Form.Group>
                    <Form.TextArea
                        fluid
                        label="description"
                        value={this.state.description}
                        onChange={this.onDescriptionChange}
                    />
                    <Button color="blue">Save</Button>
                </Form>
            </div>
        );
    }
}

export default ProfileAward;