import React, { Component } from "react";
import { Form, Button } from "semantic-ui-react";
import axios from 'axios';

class ProfileBio extends Component {
    constructor(props) {
        super(props);
        const { fullName, heading, state, city } = props;
        this.state = {
            fullName: fullName ? fullName : '',
            heading: heading ? heading : '',
            state: state ? state : '',
            city: city ? city : '',
            stateInfo: []
        }
    }

    componentDidMount() {
        axios.get('/state', {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            this.setState({ stateInfo: response.data.infos })
        }).catch((e) => {
            console.log(e);
            alert('error: Unable to get state inoformations');
        })
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    onSubmit = e => {
        e.preventDefault();
        const { fullName, heading, state, city } = this.state;
        this.props.onEdit('bio', { bio: { fullName, heading, state, city } });
    };

    render() {
        const { stateInfo } = this.state;
        const stateOptions = stateInfo.map(({ state }) => ({ key: state, value: state, text: state }))
        let cityOptions = [];
        if (this.state.state) {
            const selectedStateInfo = stateInfo.filter(({ state }) => state === this.state.state);
            if (selectedStateInfo.length) {
                cityOptions = selectedStateInfo[0].cities.map((city) => ({
                    key: city, value: city, text: city
                }))
            }
        }
        return (
            <div style={{ padding: '20px' }}>
                <Form onSubmit={this.onSubmit}>
                    <Form.Input
                        fluid
                        label="Full Name"
                        name="fullName"
                        value={this.state.fullName}
                        onChange={this.handleChange}
                    />
                    <Form.Input
                        fluid
                        label="Heading"
                        name="heading"
                        value={this.state.heading}
                        onChange={this.handleChange}
                    />
                    <Form.Group widths="equal">
                        <Form.Select
                            fluid
                            label="State"
                            placeholder="Select State"
                            name="state"
                            options={stateOptions}
                            value={this.state.state}
                            onChange={this.handleChange}
                        />
                        <Form.Select
                            fluid
                            label="City"
                            placeholder="Select City"
                            name="city"
                            options={cityOptions}
                            value={this.state.city}
                            onChange={this.handleChange}
                        />
                    </Form.Group>
                    <Button color="blue">Save</Button>
                </Form>
            </div>
        );
    }
}

export default ProfileBio;