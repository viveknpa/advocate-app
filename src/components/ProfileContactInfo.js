import React, { Component } from "react";
import { Form, Button } from "semantic-ui-react";
import { monthOptions, monthDateOptions } from './utils/options';
import _ from 'lodash'

class ProfileContactInfo extends Component {
    constructor(props) {
        super(props);
        const { mobile, officeAddress, website, birthday } = props;
        this.state = {
            mobile: mobile ? mobile : '',
            officeAddress: officeAddress ? officeAddress : '',
            website: website ? website : '',
            birthdayDate: birthday ? birthday.split(",")[0] : '',
            birthdayMonth: birthday ? birthday.split(",")[1].trim() : ''
        }
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    onSubmit = e => {
        e.preventDefault()
        const contactInfo = _.pick(this.state, ['mobile', 'officeAddress', 'website']);
        contactInfo.birthday = this.state.birthdayDate + ', ' + this.state.birthdayMonth;
        this.props.onEdit('contactInfo', { contactInfo })
    }

    render() {
        return (
            <div style={{ padding: '20px' }}>
                <Form onSubmit={this.onSubmit}>
                    <Form.Input
                        fluid
                        label="Mobile"
                        name="mobile"
                        value={this.state.mobile}
                        onChange={this.handleChange}
                    />
                    <Form.TextArea
                        fluid
                        label="Office Address"
                        name="officeAddress"
                        value={this.state.officeAddress}
                        onChange={this.handleChange}
                    />
                    <Form.Group widths="equal">
                        <Form.Select
                            fluid
                            label="Month"
                            placeholder="Select Month"
                            name="birthdayMonth"
                            options={monthOptions}
                            value={this.state.birthdayMonth}
                            onChange={this.handleChange}
                        />
                        <Form.Select
                            fluid
                            label="Day"
                            placeholder="Select Day"
                            name="birthdayDate"
                            options={monthDateOptions}
                            value={this.state.birthdayDate}
                            onChange={this.handleChange}
                        />
                    </Form.Group>
                    <Button color="blue">Save</Button>
                </Form>
            </div>
        )
    }
}

export default ProfileContactInfo;