import React, { Component } from "react";
import { render } from "react-dom";
import { Form, Button } from "semantic-ui-react";

const yearOptions = [];
for (let i = 2018; i >= 1955; i--) {
    yearOptions.push({ key: i, text: i, value: i });
}

class ProfileEducation extends Component {
    state = {
        school: this.props.school ? this.props.school : "",
        degree: this.props.degree ? this.props.degree : "",
        field: this.props.field ? this.props.field : "",
        grade: this.props.grade ? this.props.grade : "",
        startYear: this.props.start ? this.props.start : "",
        endYear: this.props.end ? this.props.end : "",
    };
    onSchoolChange = e => this.setState({ school: e.target.value });
    onDegreeChange = e => this.setState({ degree: e.target.value });
    onFieldChange = e => this.setState({ field: e.target.value });
    onGradeChange = e => this.setState({ grade: e.target.value });
    onStartYearChange = (e, { value }) => this.setState({ startYear: value });
    onEndYearChange = (e, { value }) => this.setState({ endYear: value });

    onSubmit = e => {
        e.preventDefault();
        const { school, degree, field, grade, startYear, endYear } = this.state;
        let newEducation = { school, degree, field, grade, startYear, endYear };
        let educationArray;
        if (this.props.education && this.props.education.length) {
            let idFound = 0;
            educationArray = this.props.education.map((education) => {
                if (education._id === this.props._id) {
                    idFound = 1;
                    return newEducation;
                }
                return education;
            })
            if(!idFound) {
                educationArray.push(newEducation);
            }
        } else {
            educationArray = [newEducation];
        }
        this.props.onEdit('education', { education: educationArray });
    };

    render() {
        return (
            <div style={{padding: '20px'}}>
                <Form onSubmit={this.onSubmit}>
                    <Form.Input
                        fluid
                        label="School"
                        value={this.state.school}
                        onChange={this.onSchoolChange}
                    />
                    <Form.Input
                        fluid
                        label="Degree"
                        value={this.state.degree}
                        onChange={this.onDegreeChange}
                    />
                    <Form.Input
                        fluid
                        label="Field"
                        value={this.state.field}
                        onChange={this.onFieldChange}
                    />
                    <Form.Group widths="equal">
                        <Form.Select
                            fluid
                            label="From"
                            placeholder="Year"
                            options={yearOptions}
                            value={this.state.startYear}
                            onChange={this.onStartYearChange}
                        />
                        <Form.Select
                            fluid
                            label="To"
                            placeholder="Year"
                            options={yearOptions}
                            value={this.state.endYear}
                            onChange={this.onEndYearChange}
                        />
                    </Form.Group>
                    <Form.Input
                        fluid
                        label="Grade"
                        value={this.state.grade}
                        onChange={this.onGradeChange}
                    />
                    <Button color="blue">Save</Button>
                </Form>
            </div>
        );
    }
}

export default ProfileEducation;