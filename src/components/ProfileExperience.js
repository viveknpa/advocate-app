import React, { Component } from "react";
import { render } from "react-dom";
import { Form, Button } from "semantic-ui-react";

import { monthOptions, yearOptions } from './utils/options';

class ProfileExperience extends Component {
    state = {
        title: this.props.title ? this.props.title : "",
        company: this.props.company ? this.props.company : "",
        location: this.props.location ? this.props.location : "",
        startMonth:
            this.props.start && this.props.start.month
                ? this.props.start.month
                : "",
        startYear:
            this.props.start && this.props.start.year
                ? this.props.start.year
                : "",
        endMonth:
            this.props.end && this.props.end.month
                ? this.props.end.month
                : "",
        endYear:
            this.props.end && this.props.end.year
                ? this.props.end.year
                : "",
        description: this.props.description ? this.props.description : ""
    };
    onTitleChange = e => this.setState({ title: e.target.value });
    onCompanyChange = e => this.setState({ company: e.target.value });
    onLocationChange = e => this.setState({ location: e.target.value });
    onStartMonthChange = (e, { value }) => this.setState({ startMonth: value });
    onStartYearChange = (e, { value }) => this.setState({ startYear: value });
    onEndMonthChange = (e, { value }) => this.setState({ endMonth: value });
    onEndYearChange = (e, { value }) => this.setState({ endYear: value });
    onDescriptionChange = e => this.setState({ description: e.target.value });

    onSubmit = e => {
        e.preventDefault();
        const {
            title,
            company,
            location,
            startMonth,
            startYear,
            endMonth,
            endYear,
            description
        } = this.state;

        const newExperience = { title, company, location, description };
        newExperience.start = { month: startMonth, year: startYear };
        newExperience.end = { month: endMonth, year: endYear };

        let experienceArray;
        if (this.props.experience && this.props.experience.length) {
            let idFound = 0;
            experienceArray = this.props.experience.map((experience) => {
                if (experience._id === this.props._id) {
                    idFound = 1;
                    return newExperience;
                }
                return experience;
            })
            if(!idFound) {
                experienceArray.push(newExperience);
            }
        } else {
            experienceArray = [newExperience];
        }
        this.props.onEdit('experience', { experience: experienceArray });
    };

    render() {
        return (
            <div style={{padding: '20px'}}>
                <Form onSubmit={this.onSubmit}>
                    <Form.Input
                        fluid
                        label="Title"
                        value={this.state.title}
                        onChange={this.onTitleChange}
                    />
                    <Form.Input
                        fluid
                        label="Company"
                        value={this.state.company}
                        onChange={this.onCompanyChange}
                    />
                    <Form.Input
                        fluid
                        label="Location"
                        value={this.state.location}
                        onChange={this.onLocationChange}
                    />
                    <Form.Group widths="equal">
                        <Form.Select
                            fluid
                            placeholder="Month"
                            label="From"
                            options={monthOptions}
                            value={this.state.startMonth}
                            onChange={this.onStartMonthChange}
                        />
                        <Form.Select
                            fluid
                            placeholder="Month"
                            label="To"
                            options={monthOptions}
                            value={this.state.endMonth}
                            onChange={this.onEndMonthChange}
                        />
                    </Form.Group>
                    <Form.Group widths="equal">
                        <Form.Select
                            fluid
                            placeholder="Year"
                            options={yearOptions}
                            value={this.state.startYear}
                            onChange={this.onStartYearChange}
                        />
                        <Form.Select
                            fluid
                            placeholder="Year"
                            options={yearOptions}
                            value={this.state.endYear}
                            onChange={this.onEndYearChange}
                        />
                    </Form.Group>
                    <Form.TextArea
                        fluid
                        rows="2"
                        label="description"
                        value={this.state.description}
                        onChange={this.onDescriptionChange}
                    />
                    <Button color="blue">Save</Button>
                </Form>
            </div>
        );
    }
}

export default ProfileExperience;