import React from "react";
import { connect } from "react-redux";
import { Link, NavLink } from "react-router-dom";
import { FaCamera } from "react-icons/lib/fa";
import axios from "axios";
import _ from "lodash";
import moment from "moment";
import {
  Item,
  Segment,
  Divider,
  Icon,
  Modal,
  Button,
  Menu,
  Dropdown,
  Container,
  Input,
  Grid,
  Message,
  Feed
} from "semantic-ui-react";

import ProfileBio from "./ProfileBio";
import ProfileExperience from "./ProfileExperience";
import ProfileEducation from "./ProfileEducation";
import ProfileAward from "./ProfileAward";
import ProfileContactInfo from "./ProfileContactInfo";
import BookAppointment from "./BookAppointment";
import { startLogout } from "../actions/auth";

export class ProfilePage extends React.Component {
  state = {
    profile: null,
    appointments: [],
    selectedDp: null,
    dpUploadProgress: null,
    suggestedProfiles: [],
    selectedProfile: "",
    searchMessage: ""
  };

  config = { headers: { "x-auth": localStorage.getItem("x-auth") } };

  getFullProfile = id => {
    axios
      .get(`/profile/${id}`, this.config)
      .then(response => {
        //id is the user id of each user not profile doc id
        this.setState({ profile: response.data.profile });
      })
      .catch(e => {
        if (e.response) {
          if (e.response.status == 404) {
            this.setState({ profile: null });
          }
        }
        console.log(e);
      });
  };

  componentDidMount() {
    if (this.props.match.params.uid.length !== 24) {
      return;
    }
    this.getFullProfile(this.props.match.params.uid);

    if (!this.props.uid) {
      return;
    }
    axios
      .get("/appointment", this.config)
      .then(response => {
        this.setState({ appointments: response.data.appointments });
      })
      .catch(e => {
        console.log(e);
        alert("Unable to get appointments!");
      });
  }

  componentWillReceiveProps(nextProps) {
    this.getFullProfile(nextProps.match.params.uid);
  }

  dpUploadHandler = e => {
    this.setState(
      {
        selectedDp: e.target.files[0]
      },
      () => {
        const fd = new FormData();
        fd.append("image", this.state.selectedDp);
        axios
          .patch("/profile/image", fd, {
            headers: { "x-auth": localStorage.getItem("x-auth") },
            onUploadProgress: progressEvent => {
              const { loaded, total } = progressEvent;
              const dpUploadProgress = Math.round((loaded / total) * 100);
              this.setState({ dpUploadProgress });
            }
          })
          .then(res => {
            this.setState(prevState => ({
              dpUploadProgress: null,
              profile: { ...prevState.profile, dpUrl: res.data.dpUrl }
            }));
          });
      }
    );
  };

  //edit or create a section
  onEdit = (sectionName, data) => {
    axios
      .patch(`/profile/${sectionName}`, data, this.config)
      .then(response => {
        alert("Change saved!");
        this.setState({ profile: response.data.profile });
        if (!this.props.profileStatus) {
          axios.get("/user/profilestatus", this.config); //change profile update status to true, only after first login
        }
      })
      .catch(e => {
        console.log(e);
        alert("Unable to update data!");
      });
  };

  handleSearchProfile = e => {
    const value = e.target.value;
    if (e.key === "Enter") {
      if (value) {
        axios
          .get(`/profile/search/${value}`)
          .then(response => {
            if (!response.data.profiles.length) {
              return this.setState({
                suggestedProfiles: [],
                searchMessage: "No profile with this name found!"
              });
            }
            this.setState({
              suggestedProfiles: response.data.profiles,
              searchMessage: ""
            });
          })
          .catch(e => {
            console.log(e);
            this.setState({
              searchMessage: "No profile with this name found!"
            });
          });
      }
    }
  };

  onSelectProfile = id => {
    this.props.history.push(`/advocate-profile/${id}`);
  };

  render() {
    const uid = this.props.match.params.uid;
    const currentUser = uid === this.props.uid;
    const { suggestedProfiles, searchMessage } = this.state;
    return (
      <div style={{ background: "#f5f5f5" }}>
        <Segment
          style={{
            paddingTop: 0,
            paddingBottom: 0,
            margin: 0,
            borderBottom: "2px solid #9D81C3"
          }}
        >
          <Container>
            <Menu secondary>
              <Menu.Item style={{ padding: 0 }} as={Link} to="/">
                <img
                  src="/files/logo.png"
                  style={{
                    width: "190px",
                    height: "75px",
                    paddingBottom: "5px"
                  }}
                />
              </Menu.Item>
              <Menu.Menu position="right">
                {this.props.uid && (
                  <Menu.Item
                    className="show-for-desktop"
                    name="Support"
                    as={NavLink}
                    to="/dashboard/support"
                  />
                )}
                {this.props.uid && (
                  <Menu.Item>
                    <Dropdown item text="Account">
                      <Dropdown.Menu>
                        {this.props.authority["profile"] && (
                          <Dropdown.Item
                            as={NavLink}
                            to={`/advocate-profile/${this.props.uid}`}
                          >
                            Edit Account
                          </Dropdown.Item>
                        )}
                        <Dropdown.Item as={NavLink} to="/reset/password">
                          Change password
                        </Dropdown.Item>
                        <Dropdown.Item
                          className="show-for-mobile"
                          onClick={this.props.startLogout}
                        >
                          Logout
                        </Dropdown.Item>
                        <Divider />
                        <Dropdown.Item>
                          <span style={{ fontWeight: "bold" }}>
                            Account expires on
                            <br />
                            {moment(this.props.accountExpiresOn).format("LLL")}
                          </span>
                        </Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </Menu.Item>
                )}
                {this.props.uid && (
                  <Menu.Item className="show-for-desktop">
                    <Button primary onClick={this.props.startLogout}>
                      Logout
                    </Button>
                  </Menu.Item>
                )}
                {!this.props.uid && (
                  <Menu.Item className="show-for-desktop">
                    <Button primary as={Link} to="/">
                      Login/Signup
                    </Button>
                  </Menu.Item>
                )}
              </Menu.Menu>
            </Menu>
          </Container>
        </Segment>
        <Container>
          <Grid stackable style={{ paddingTop: "20px" }}>
            <Grid.Column width={10}>
              {this.state.profile && (
                <div className="profile-container">
                  <div>
                    <div className="page-header">
                      <div className="page-header__profile-info">
                        <div>
                          <div className="dp-container">
                            <img
                              className="img"
                              src={`/files/${this.state.profile.dpUrl}`}
                            />
                            {currentUser && (
                              <div>
                                <input
                                  style={{ display: "none" }}
                                  type="file"
                                  onChange={this.dpUploadHandler}
                                  ref={fileInput =>
                                    (this.fileInput = fileInput)
                                  }
                                />
                                {this.state.dpUploadProgress && (
                                  <progress
                                    value={this.state.dpUploadProgress}
                                    max="100"
                                  />
                                )}
                                <button
                                  onClick={() => this.fileInput.click()}
                                  className="upload-button"
                                >
                                  <span className="show-for-desktop">
                                    <FaCamera /> upload profile pic
                                  </span>
                                  <span className="show-for-mobile">
                                    <FaCamera /> Edit
                                  </span>
                                </button>
                              </div>
                            )}
                          </div>
                          {currentUser && (
                            <Modal
                              trigger={<Icon name="pencil alternate" />}
                              header="Add Bio"
                              content={
                                <ProfileBio
                                  {...this.state.profile.bio}
                                  fullName={this.state.profile.fullName}
                                  onEdit={this.onEdit}
                                />
                              }
                              actions={[
                                { key: "done", content: "Done", positive: true }
                              ]}
                            />
                          )}
                        </div>
                        <ul className="intro">
                          <li>
                            <b>{this.state.profile.fullName}</b>
                          </li>
                          <li>{this.state.profile.bio.heading}</li>
                          <li className="muted-text">
                            {this.state.profile.bio.state},{" "}
                            {this.state.profile.bio.city}
                          </li>
                        </ul>
                      </div>
                    </div>

                    <Segment>
                      <h3
                        style={{
                          display: "flex",
                          justifyContent: "space-between"
                        }}
                      >
                        Experience
                        {currentUser && (
                          <Modal
                            trigger={<Icon name="add" />}
                            header="Add Experience"
                            content={
                              <ProfileExperience
                                experience={this.state.profile.experience}
                                onEdit={this.onEdit}
                              />
                            }
                            actions={[
                              { key: "done", content: "Done", positive: true }
                            ]}
                          />
                        )}
                      </h3>
                      <Item.Group unstackable>
                        {this.state.profile.experience.map(experience => (
                          <Item key={experience._id}>
                            <Item.Image
                              size="tiny"
                              src="https://react.semantic-ui.com/images/wireframe/image.png"
                            />
                            <Item.Content>
                              <Item.Header
                                style={{
                                  display: "flex",
                                  justifyContent: "space-between"
                                }}
                              >
                                {experience.title}
                                {currentUser && (
                                  <Modal
                                    trigger={<Icon name="pencil alternate" />}
                                    header="Edit experience"
                                    content={
                                      <ProfileExperience
                                        {...experience}
                                        experience={
                                          this.state.profile.experience
                                        }
                                        onEdit={this.onEdit}
                                      />
                                    }
                                    actions={[
                                      {
                                        key: "done",
                                        content: "Done",
                                        positive: true
                                      }
                                    ]}
                                  />
                                )}
                              </Item.Header>
                              <Item.Meta>{experience.company}</Item.Meta>
                              <Item.Extra>
                                {experience.start && (
                                  <span>
                                    {experience.start.month}{" "}
                                    {experience.start.year} -{" "}
                                  </span>
                                )}
                                {experience.end && (
                                  <span>
                                    {" "}
                                    {experience.end.month} {experience.end.year}
                                  </span>
                                )}
                                <br />
                                {experience.location}
                                <br />
                              </Item.Extra>
                              <Item.Description>
                                {experience.description}
                              </Item.Description>
                            </Item.Content>
                          </Item>
                        ))}
                      </Item.Group>
                    </Segment>

                    <Segment>
                      <h3
                        style={{
                          display: "flex",
                          justifyContent: "space-between"
                        }}
                      >
                        Education
                        {currentUser && (
                          <Modal
                            trigger={<Icon name="add" />}
                            header="Add Education"
                            content={
                              <ProfileEducation
                                education={this.state.profile.education}
                                onEdit={this.onEdit}
                              />
                            }
                            actions={[
                              { key: "done", content: "Done", positive: true }
                            ]}
                          />
                        )}
                      </h3>
                      <Item.Group unstackable>
                        {this.state.profile.education.map(education => (
                          <Item key={education._id}>
                            <Item.Image
                              size="tiny"
                              src="https://angel.co/images/shared/nopic_college.png"
                            />
                            <Item.Content>
                              <Item.Header
                                style={{
                                  display: "flex",
                                  justifyContent: "space-between"
                                }}
                              >
                                {education.school}
                                {currentUser && (
                                  <Modal
                                    trigger={<Icon name="pencil alternate" />}
                                    header="Edit education"
                                    content={
                                      <ProfileEducation
                                        {...education}
                                        education={this.state.profile.education}
                                        onEdit={this.onEdit}
                                      />
                                    }
                                    actions={[
                                      {
                                        key: "done",
                                        content: "Done",
                                        positive: true
                                      }
                                    ]}
                                  />
                                )}
                              </Item.Header>
                              <Item.Meta>
                                {education.degree}, {education.field}
                              </Item.Meta>
                              <Item.Extra>
                                {
                                  <span>
                                    {education.startYear} {education.endYear}
                                  </span>
                                }
                              </Item.Extra>
                              <Item.Description>
                                {education.description}
                              </Item.Description>
                            </Item.Content>
                          </Item>
                        ))}
                      </Item.Group>
                    </Segment>

                    <Segment>
                      <h3
                        style={{
                          display: "flex",
                          justifyContent: "space-between"
                        }}
                      >
                        Awards
                        {currentUser && (
                          <Modal
                            trigger={<Icon name="add" />}
                            header="Add Award"
                            content={
                              <ProfileAward
                                award={this.state.profile.award}
                                onEdit={this.onEdit}
                              />
                            }
                            actions={[
                              { key: "done", content: "Done", positive: true }
                            ]}
                          />
                        )}
                      </h3>
                      <Item.Group unstackable>
                        {this.state.profile.award.map(award => (
                          <Item key={award._id}>
                            <Item.Content>
                              <Item.Header
                                style={{
                                  display: "flex",
                                  justifyContent: "space-between"
                                }}
                              >
                                {award.title}
                                {currentUser && (
                                  <Modal
                                    trigger={<Icon name="pencil alternate" />}
                                    header="Edit Award"
                                    content={
                                      <ProfileAward
                                        {...award}
                                        award={this.state.profile.award}
                                        onEdit={this.onEdit}
                                      />
                                    }
                                    actions={[
                                      {
                                        key: "done",
                                        content: "Done",
                                        positive: true
                                      }
                                    ]}
                                  />
                                )}
                              </Item.Header>
                              <Item.Meta>{award.issuer}</Item.Meta>
                              <Item.Extra>
                                {
                                  <span>
                                    {award.month} {award.year}
                                  </span>
                                }
                              </Item.Extra>
                              <Item.Description>
                                {award.description}
                              </Item.Description>
                            </Item.Content>
                          </Item>
                        ))}
                      </Item.Group>
                    </Segment>

                    <Segment>
                      <h3
                        style={{
                          display: "flex",
                          justifyContent: "space-between"
                        }}
                      >
                        Contact Info
                        {currentUser && (
                          <Modal
                            trigger={<Icon name="pencil alternate" />}
                            header="Add Contact Info"
                            content={
                              <ProfileContactInfo
                                {...this.state.profile.contactInfo}
                                onEdit={this.onEdit}
                              />
                            }
                            actions={[
                              { key: "done", content: "Done", positive: true }
                            ]}
                          />
                        )}
                      </h3>
                      <Item>
                        <Item.Content>
                          <Feed>
                            <Feed.Event
                              style={{ marginBottom: "10px" }}
                              icon={
                                <Icon
                                  name="mobile alternate"
                                  style={{
                                    marginRight: "10px",
                                    fontSize: "20px"
                                  }}
                                />
                              }
                              date={
                                <h4 style={{ marginBottom: "5px" }}>Mobile</h4>
                              }
                              summary={this.state.profile.contactInfo.mobile}
                            />

                            <Feed.Event
                              style={{ marginBottom: "10px" }}
                              icon={
                                <Icon
                                  name="mail"
                                  style={{
                                    marginRight: "10px",
                                    fontSize: "20px"
                                  }}
                                />
                              }
                              date={
                                <h4 style={{ marginBottom: "5px" }}>Email</h4>
                              }
                              summary={
                                <span style={{ color: "blue" }}>
                                  {this.state.profile._creator.email}
                                </span>
                              }
                            />

                            <Feed.Event
                              style={{ marginBottom: "10px" }}
                              icon={
                                <Icon
                                  name="address card outline"
                                  style={{
                                    marginRight: "10px",
                                    fontSize: "20px"
                                  }}
                                />
                              }
                              date={
                                <h4 style={{ marginBottom: "5px" }}>
                                  Office Address
                                </h4>
                              }
                              summary={
                                this.state.profile.contactInfo.officeAddress
                              }
                            />

                            <Feed.Event
                              style={{ marginBottom: "10px" }}
                              icon={
                                <Icon
                                  name="birthday"
                                  style={{
                                    marginRight: "10px",
                                    fontSize: "20px"
                                  }}
                                />
                              }
                              date={
                                <h4 style={{ marginBottom: "5px" }}>
                                  Birthday
                                </h4>
                              }
                              summary={this.state.profile.contactInfo.birthday}
                            />
                          </Feed>
                          {this.props.uid && !currentUser && (
                            <BookAppointment
                              appointmentTo={this.props.match.params.uid}
                            />
                          )}
                          {currentUser && (
                            <div>
                              <hr />
                              <h3>Appointment details</h3>
                              {this.state.appointments.map(appointment => (
                                <ul>
                                  <li>
                                    Appointment requested by -{" "}
                                    {appointment._creator.fullName},{" "}
                                    {appointment._creator.mobile}
                                  </li>
                                  <li>
                                    Schedule -{" "}
                                    {moment(appointment.date).format("L")},{" "}
                                    {appointment.startTime} -{" "}
                                    {appointment.endTime}
                                  </li>
                                  <li>details - {appointment.description}</li>
                                </ul>
                              ))}
                            </div>
                          )}
                        </Item.Content>
                      </Item>
                    </Segment>
                  </div>
                </div>
              )}
              {!this.state.profile && (
                <Message positive>
                  <Message.Header>Search Profiles</Message.Header>
                  <p>
                    Click the <b>Link appeared in Search</b> to see full
                    profile.
                  </p>
                </Message>
              )}
            </Grid.Column>
            <Grid.Column width={6}>
              <Segment>
                <Item.Group>
                  <Item>
                    <Input
                      label="Search Profile"
                      placeholder="full name..."
                      onKeyPress={this.handleSearchProfile}
                    />
                  </Item>
                  {searchMessage && (
                    <span style={{ color: "orange" }}>
                      No profile with this name found!
                    </span>
                  )}
                  {suggestedProfiles.map(
                    ({ _creator: id, bio, dpUrl, fullName }) => (
                      <Item key={id}>
                        <Item.Image
                          size="tiny"
                          src={`/files/${dpUrl || "default_dp.jpg"}`}
                        />
                        <Item.Content>
                          <Item.Header
                            as="a"
                            onClick={() => this.onSelectProfile(id)}
                          >
                            {fullName}
                          </Item.Header>
                          {bio && <Item.Meta>{bio.heading}</Item.Meta>}
                          {bio && <Item.Extra>{bio.location}</Item.Extra>}
                        </Item.Content>
                      </Item>
                    )
                  )}
                </Item.Group>
              </Segment>
            </Grid.Column>
          </Grid>
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  uid: state.auth.uid,
  fullName: state.auth.fullName,
  profileStatus: state.auth.profileStatus,
  authority: state.auth.authority
});

const mapDispatchToProps = dispatch => ({
  startLogout: () => dispatch(startLogout())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfilePage);
