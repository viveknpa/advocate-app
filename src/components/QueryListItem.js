import React from 'react';
import axios from 'axios';
import { Button, Header, Comment, Input, Form, Icon, Accordion } from 'semantic-ui-react';
import moment from 'moment';

class QueryListItem extends React.Component {
    state = {
        reply: this.props.reply || [],
        replyText: '',
        showReplies: false,
        activeIndex: 0
    }

    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index

        this.setState({ activeIndex: newIndex })
    }

    config = { headers: { 'x-auth': localStorage.getItem('x-auth') } }

    handleReplyTextChange = (e) => {
        this.setState({ replyText: e.target.value })
    }

    handleReplySubmit = (e) => {
        const text = e.target.value.trim();
        if (!text) { return }
        if (e.keyCode === 13) {
            axios.patch(`/support/reply/${this.props._id}`, { text }, this.config).then((response) => {
                this.setState((prevState) => ({
                    reply: response.data.query.reply,
                    replyText: ''
                }))
            }).catch((e) => {
                alert('Unable to post reply');
                console.log(e);
            })
        }
    }

    onShowshowReplies = () => {
        this.setState((prevState) => ({
            showReplies: !prevState.showReplies
        }))
    }

    render() {
        const { _creator, queriedAt, query, category } = this.props;
        const { activeIndex } = this.state
        return (
            <Comment>

                <Comment.Avatar src='/files/media1.png'/>
                <Comment.Content onClick={this.onShowshowReplies} style={{cursor: 'pointer'}}>
                    <Comment.Author as='a'>{_creator.fullName} <Icon name="angle right" /> {category}</Comment.Author>
                    <Comment.Metadata>
                        <div>{moment(queriedAt).format('LLL')}</div>
                    </Comment.Metadata>
                    <Comment.Text>
                        <p>{query}</p>
                    </Comment.Text>
                </Comment.Content>

                {this.state.showReplies && <Comment.Group>
                    {
                        this.state.reply.map((replyItem, index) => {
                            return (
                                <Comment key={index}>
                                    <Comment.Avatar src='/files/media2.png' />
                                    <Comment.Content>
                                        <Comment.Author as='a'>{replyItem._repliedBy.fullName}</Comment.Author>
                                        <Comment.Metadata>
                                            <div>{moment(replyItem.repliedAt).format('LLL')}</div>
                                        </Comment.Metadata>
                                        <Comment.Text>{replyItem.text}</Comment.Text>
                                    </Comment.Content>
                                </Comment>
                            )
                        })
                    }
                </Comment.Group>}
                { this.state.showReplies && <Input
                    style={{ flexGrow: 1 }}
                    className="comments"
                    fluid
                    placeholder="Write a reply..."
                    value={this.state.replyText}
                    onChange={this.handleReplyTextChange}
                    onKeyDown={this.handleReplySubmit}
                />}

            </Comment>
        )
    }
}

export default QueryListItem;