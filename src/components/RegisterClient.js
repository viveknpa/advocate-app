import React from 'react';
import axios from 'axios';

import ClientForm from './ClientForm';
import AlertModel from './AlertModel';

class RegisterClient extends React.Component {
    state = {
        alertModelOpen: false,
        alertMessage: ''
    }
    onSubmit = (data) => {
        return axios.post('/client', data, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            this.setState({ 
                alertModelOpen: true, 
                alertType: 'success', 
                alertMessage: 'New Client Created Successfully!'
            })
            this.props.onRegisterClient(response.data.client);
        }).catch((e) => {
            this.setState({ 
                alertModelOpen: true,
                alertType: 'error', 
                alertMessage: 'Unable To Create Client!'
            })
            console.log(e);
        })
    }
    onHideAlertModel = () => this.setState({ alertModelOpen: false })
    render() {
        const { alertModelOpen, alertType, alertMessage } = this.state;
        return (
            <div>
                <ClientForm onSubmit={this.onSubmit} action="register"/>
                <AlertModel 
                    open={alertModelOpen}
                    alertType={alertType}
                    message={alertMessage}
                    onHideAlertModel={this.onHideAlertModel}
                />
            </div>
        )
    }
}

export default RegisterClient;