import React from 'react';
import axios from 'axios';
import EmployeeForm from './EmployeeForm';
import AlertModel from './AlertModel';

class RegisterEmployee extends React.Component {
    state = {
        alertModelOpen: false,
        alertMessage: ''
    }
    onSubmit = (data) => {
        return axios.post('/employee', data, { 
            headers: { 'x-auth': localStorage.getItem('x-auth') } 
        }).then((response) => {
            this.setState({ 
                alertModelOpen: true, 
                alertType: 'success',
                alertMessage: 'Employee Registered Successfuly!'
            })
            this.props.onRegisterEmployee(response.data.employee);
        }).catch((e) => {
            console.log(e);
            this.setState({ 
                alertModelOpen: true, 
                alertType: 'error',
                alertMessage: 'Unable to Regsiter employee!'
            })
        })
    }
    onHideAlertModel = () => this.setState({ alertModelOpen: false, alertMessage: '' })
    render() {
        const { alertModelOpen, alertType, alertMessage } = this.state;
        return (
            <div>
                <EmployeeForm onSubmit={this.onSubmit} action="register"/>
                <AlertModel 
                    open={alertModelOpen}
                    alertType={alertType}
                    message={alertMessage}
                    onHideAlertModel={this.onHideAlertModel}
                />
            </div>
        )
    }
}

export default RegisterEmployee;