import React from 'react';
import axios from 'axios';
import { Segment, Image, Form, Button, Input } from 'semantic-ui-react'

class RequestAdsPage extends React.Component {
    state = { message: '' }
    onSubmit = (e) => {
        e.preventDefault();
        e.persist();
        const formData = new FormData(e.target);
        axios({
            url: '/advertise',
            method: 'post',
            data: formData,
            headers: {
                'Content-Type': 'multipart/form-data',
                'x-auth': localStorage.getItem('x-auth')
            }
        }).then((response) => {
            alert('ads request sent!')
            e.target.title.value = '';
            e.target.description.value = '';
            e.target.file.value = '';
        }).catch((e) => {
            alert('Unable to send ads request!')
            this.setState({ message: 'Unable to make ad request' })
        })
    }
    render() {
        return (
            <Form onSubmit={this.onSubmit} style={{ maxWidth: '400px' }}>
                <Form.Field>
                    <label>Title</label>
                    <input type="text" name="title" placeholder="Title" required/>
                </Form.Field>
                <Form.Field>
                    <label>Description</label>
                    <input type="text" name="description" placeholder="description" />
                </Form.Field>
                <Form.Field>
                    <label>Upload file</label>
                    <Input type="file" name="file" />
                </Form.Field>
                <Button primary color="violet">Send request</Button>
            </Form>
        )
    }
}

export default RequestAdsPage;