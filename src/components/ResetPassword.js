import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { Segment, Button, Menu, Form, Input } from "semantic-ui-react";

export default class ResetPassword extends React.Component {
  state = {
    resetStatus: false,
    error: ""
  };
  onSubmit = e => {
    e.preventDefault();
    const password = e.target.password.value;
    const confirmPassword = e.target.confirmPassword.value;
    if (!password) {
      this.setState({ error: "Password cannot be empty" });
    } else if (!confirmPassword) {
      this.setState({ error: "confirm your password" });
    } else if (password !== confirmPassword) {
      this.setState({ error: "Passwords do not match" });
    } else {
      const config = {};
      let url;
      const token = localStorage.getItem("x-auth");
      if (token) {
        config.headers = { "x-auth": token };
        url = "/reset";
        console.log(url, config);
      } else {
        url = `/reset/${this.props.match.params.secretToken}`;
      }
      return axios
        .post(url, { password }, config)
        .then(() => {
          this.setState({ error: "", resetStatus: true });
        })
        .catch(e => {
          this.setState({ error: "Unable to reset password, try later" });
        });
    }
  };

  render() {
    return (
      <div>
        <Segment
          style={{
            paddingTop: 0,
            paddingBottom: 0,
            margin: 0,
            borderBottom: "2px solid #9D81C3"
          }}
        >
          <Menu secondary>
            <Menu.Item as={Link} to="/">
              <img
                src="/files/logo.png"
                style={{ width: "190px", height: "75px", paddingBottom: "5px" }}
              />
            </Menu.Item>
          </Menu>
        </Segment>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            marginTop: "40px"
          }}
        >
          <Form onSubmit={this.onSubmit}>
            <h1 style={{ fontWeight: 200 }}>Change your password</h1>
            {this.state.resetStatus && (
              <span
                style={{
                  display: "block",
                  marginBottom: "10px",
                  color: "orange",
                  fontStyle: "italic",
                  fontSize: "18px"
                }}
              >
                Password reset successful <Link to="/"> Go back</Link>
              </span>
            )}
            {this.state.error && (
              <span
                style={{
                  display: "block",
                  marginBottom: "10px",
                  color: "orange",
                  fontStyle: "italic",
                  fontSize: "18px"
                }}
              >
                {this.state.error}
              </span>
            )}
            <Form.Field>
              <label>Password</label>
              <Input
                type="password"
                name="password"
                placeholder="Password(min 6 characters)"
              />
            </Form.Field>
            <Form.Field>
              <label>Confirm password</label>
              <Input
                type="password"
                name="confirmPassword"
                placeholder="confirm password"
              />
            </Form.Field>
            <Button color="violet">Change password</Button>
          </Form>
        </div>
      </div>
    );
  }
}
