import _ from 'lodash'
import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { Button, Header, Icon, Image, Modal, Container, Input, List, Dropdown, Menu, Message, Form } from 'semantic-ui-react'

import { selectClient } from '../actions/selectedClient';

const initialState = {
        clients: [],
        searchBy: 'fullName',
        message: '',
        currentCaseId: '',
        modalOpen: false,
};

class SearchCase extends React.Component {
    state = initialState;
    config = { headers: { 'x-auth': localStorage.getItem('x-auth') } }
    
    handleOpen = () => this.setState({ modalOpen: true })
    handleClose = () => {
        this.setState({ modalOpen: false });
        try {
            this.props.onEndSelectCase();
        } catch (e) {

        }
        if (this.state.currentCaseId) {
            this.props.dispatch(selectClient(this.state.clients.filter((client) => client.caseId === this.state.currentCaseId)[0]));
        }
        this.setState(initialState);
    }

    searchByChange = (e, { value }) => this.setState({ searchBy: value })

    handleSearch = (e) => {
        const value = e.target.value;
        if (value.length < 2) { return this.setState({ clients: [] }) }
        const searchBy = this.state.searchBy;
        if (this.state.currentCaseId) {
            this.setState({ currentCaseId: '' });
        }
                axios.get(`/case/search/${searchBy}/${value}`, this.config).then((response) => {
                    if (!response.data.clients.length) {
                        return this.setState({ clients: [], message: `No client with this ${searchBy} found!` });
                    }
                    this.setState({ clients: response.data.clients, message: '' })
                }).catch((e) => {
                    console.log(e);
                    this.setState({ message: 'Unable to Search now!' });
                })
            }
    selectClient = (client) => {
        this.setState({ currentCaseId: client.caseId }, () => this.handleClose());
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.modalOpen) {
            this.setState({ modalOpen: true })
        }
    }
    render() {
        const options = [
            { key: '1', text: 'Name', value: 'fullName' },
            { key: '2', text: 'Case id', value: 'caseId' },
            { key: '3', text: 'Case Number', value: 'caseNo' },
        ]
        return (
            <Modal
                open={this.state.modalOpen}
                onClose={this.handleClose}
                size="tiny"
            >
                <Modal.Header>
                    <Input
                        action={
                            <Dropdown
                                button
                                basic
                                floating
                                options={options}
                                defaultValue='fullName'
                                value={this.searchBy}
                                onChange={this.searchByChange}
                            />
                        }
                        fluid
                        autoFocus
                        icon='search'
                        iconPosition='left'
                        placeholder="Search"
                        onKeyUp={this.handleSearch}
                    />
                </Modal.Header>
                <Modal.Content scrolling>
                    <Modal.Description>
                        {this.state.message && <Modal.Header style={{ color: 'orange', fontStyle: 'italic' }}>{this.state.message}</Modal.Header>}
                        <List divided verticalAlign='middle'>
                            {
                                this.state.clients.map((client) =>
                                    <List.Item key={client._id}>
                                        <List.Content floated='right'>
                                            <Button 
                                                color="orange" 
                                                onClick={() => this.selectClient(client)} 
                                                >
                                                {client.caseId === this.props.caseId ? 'Selected' : 'Select'}
                                            </Button>
                                        </List.Content>
                                        <Image avatar src='https://react.semantic-ui.com/images/avatar/small/lena.png' />
                                        <List.Content>
                                            <List.Header>{client.fullName}</List.Header>
                                            Case id - {client.caseId}, Case No - {client.caseNo}
                                        </List.Content>
                                    </List.Item>
                                )}
                        </List>
                    </Modal.Description>
                </Modal.Content>
            </Modal>
        )
    }
}

const mapStateToProps = ({ selectedClient }) => ({
    caseId: selectedClient.caseId
})

export default connect(mapStateToProps)(SearchCase);

