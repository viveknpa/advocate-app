import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Checkbox, Form, Icon, Input } from 'semantic-ui-react';

export default class SignupForm extends React.Component {
    state = {
        validationError: '',
        disabled: false
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ disabled: false })
    }
    onSignupSubmit = (e) => {
        e.preventDefault();
        const fullName = e.target.fullName.value;
        const email = e.target.email.value;
        const mobile = e.target.mobile.value;
        const password = e.target.password.value;
        const confirmPassword = e.target.confirmPassword.value;
        const userType = e.target.advocateSignUp.checked ? 9 : 16;
        if (!fullName) {
            this.setState({ validationError: 'full name is required' });
        } else if (!email) {
            this.setState({ validationError: 'email is required' });
        } else if (mobile.toString().length < 10) {
            this.setState({ validationError: 'Mobile no is invalid' });
        } else if (password.length < 6) {
            this.setState({ validationError: 'minimun 6 alphanumeric character is required for password' });
        } else if (password !== confirmPassword) {
            this.setState({ validationError: 'passwords do not match' });
        } else {
            this.setState({ validationError: '', disabled: true });
            this.props.startSignUp({ fullName, email, mobile, password, userType });
        }
    }
    render() {
        return (
            <div className="box-layout">
                <div className="box-layout__box">
                    <h1 className="box-layout__title">Create a new account</h1>
                    <Form size="large" className="box-layout-form" onSubmit={this.onSignupSubmit}>
                        {this.state.validationError && <span className="box-layout-form__error"> {this.state.validationError} </span>}
                        {this.props.signUpError && <span className="box-layout-form__error"> {this.props.signUpError} </span>}
                        <Form.Field>
                            <input type="text" name="fullName" placeholder="Full Name"/>
                        </Form.Field>
                        <Form.Field>
                            <input type="email" name="email" placeholder="Email"/>
                        </Form.Field>
                        <Form.Field>
                            <input type="number" name="mobile" placeholder="Mobile number"/>
                        </Form.Field>
                        <Form.Field>
                            <input type="password" name="password" placeholder="Password"/>
                        </Form.Field>
                        <Form.Field>
                            <input type="password" name="confirmPassword" placeholder="Confirm password"/>
                        </Form.Field>
                        <Form.Field>
                            <Checkbox label='Sign up as Advocate' name="advocateSignUp"/>
                        </Form.Field>
                        <Button
                            fluid
                            size="big"
                            style={{ background: "#865493", color: "white" }}
                            disabled={this.state.disabled}
                        >
                            Sign Up
                        </Button>
                        <h3 className="box-layout-form__links">
                            Already have an account?
                            <span onClick={this.props.handleFormDisplay}>Log In</span>
                        </h3>
                    </Form>
                    <div className="box-layout__footer">
                        <Link to="/advocate-profile/search">Search An Advocate</Link>
                    </div>
                </div>
            </div>
        )
    }
}
