import React from 'react';
import axios from 'axios';
import { Dropdown, TextArea, Button, Form, Grid, Header, Segment, Comment, Input, Loader } from 'semantic-ui-react';
import { connect } from 'react-redux';

import QueryListItem from './QueryListItem';

class SupportPage extends React.Component {
    state = {
        queries: [],
        category: '',
        query: '',
        error: '',
        loaderActive: true
    }
    config = { headers: { 'x-auth': localStorage.getItem('x-auth') } }
    onCategoryChange = (e, { value }) => {
        this.setState({ category: value, error: '' })
    }
    onQueryChange = (e) => {
        this.setState({ query: e.target.value, error: '' })
    }
    onSubmitNewQuery = (e) => {
        e.preventDefault();
        const { category, query } = this.state;
        if (!category || !query) {
            return this.setState({ error: 'Fill the forms properly!' })
        }
        axios.post('/support', { category, query }, this.config).then((response) => {
            alert('query submitted!');
            this.setState((prevState) => ({ 
                category: '', 
                query: '',
                queries: [...prevState.queries, response.data.query] 
            }))
        }).catch((e) => {
            alert('Unable to send query now!')
            console.log(e);
        })
    }
    componentWillMount() {
        axios.get(`/support/me`, this.config).then((response) => { //all queries by me
            this.setState({ queries: response.data.queries, loaderActive: false  });
        }).catch((e) => {
            this.setState({ loaderActive: false })
            alert('error: Unable to get query list');
            console.log(e);
        })
    }
    render() {
        const dropdownOptions = [
            { key: 1, text: 'Users', value: 'users' },
            { key: 2, text: 'Employee', value: 'advocate-employee' },
            { key: 3, text: 'Client', value: 'advocate-client' },
            { key: 4, text: 'Account', value: 'advocate-account' },
            { key: 5, text: 'Tarikh', value: 'advocate-tarikh' },
            { key: 6, text: 'client folder', value: 'advocate-clientFile' },
            { key: 7, text: 'client doc', value: 'advocate-clientDoc' },
            { key: 8, text: 'Message client', value: 'advocate-messageClient' },
            { key: 9, text: 'Transfer client', value: 'advocate-transferClient' }
        ]
        return (
            <Grid>
                <Grid.Column mobile={16} tablet={8} computer={11}>
                    <Form onSubmit={this.onSubmitNewQuery}>
                        {this.state.error && <h4 style={{ color: 'orange', fontStyle: 'italic' }}>{this.state.error}</h4>}
                        <Form.Dropdown
                            label='Select complaint category'
                            name="category"
                            options={dropdownOptions}
                            placeholder='Select'
                            selection
                            value={this.state.category}
                            onChange={this.onCategoryChange}
                        />
                        <Form.TextArea
                            label='Write your problem here'
                            name="query"
                            autoHeight
                            placeholder='write...'
                            value={this.state.query}
                            onChange={this.onQueryChange}
                        />
                        <Button color="violet">Send</Button>
                    </Form>
                    <Comment.Group>
                        <Header as='h3' dividing>
                            Recent Queries
                        </Header>
                        <Loader active={this.state.loaderActive} inline='centered' />
                        { this.state.queries.map((query) =>  
                                <QueryListItem { ...query } key={query._id} />
                            )
                        }
                    </Comment.Group>
                </Grid.Column>
            </Grid>
        )
    }
}

const mapStateToProps = (state) => ({
    uid: state.auth.uid
})

export default connect(mapStateToProps)(SupportPage);