import React from 'react';
import axios from 'axios';
import { Comment, Loader, Header } from 'semantic-ui-react';

import QueryListItem from './QueryListItem';

class SupportReplyPage extends React.Component {
    state = {
        queries: [],
    }
    config = { headers: { 'x-auth': localStorage.getItem('x-auth') } }
    componentWillMount() {
        axios.get(`/support`, this.config).then((response) => { //all queries
            this.setState({ queries: response.data.queries });
        }).catch((e) => {
            alert('error: Unable to get query list');
            console.log(e);
        })
    }
    render() {
        return (
            <Comment.Group>
                <Header as='h3' dividing>
                    Resolve Queries
                </Header>
                <Loader active={!!!this.state.queries.length} inline='centered' />
                { this.state.queries.map((query) =>
                    <QueryListItem {...query} key={query._id} />
                )}
            </Comment.Group>
        )
    }
}

export default SupportReplyPage;