import React from "react";
import { SingleDatePicker } from "react-dates";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import TimePicker from "rc-time-picker";
import "rc-time-picker/assets/index.css";
import moment from "moment";
import axios from "axios";
import { connect } from "react-redux";
import {
  Button,
  Form,
  Accordion,
  Icon,
  Label,
  Message,
  Grid
} from "semantic-ui-react";

import ReactTable from "react-table";
import selectTableHOC from "react-table/lib/hoc/selectTable";
import treeTableHOC from "react-table/lib/hoc/treeTable";
const SelectTreeTable = selectTableHOC(treeTableHOC(ReactTable));

import TarikhUpdate from "./TarikhUpdate";
import SearchCase from "./SearchCase";
import AlertModel from "./AlertModel";
import ViewComment from "./common/ViewComment";

const initialState = {
  tarikhs: [],
  calenderFocused: false,
  date: new Date(),
  startTime: "",
  endTime: "",
  reason: "",
  activeIndex: 1,
  selectCase: false,
  selectType: "radio",
  selectedTarikh: "",
  selectedRowKey: "",
  modalOpen: false,
  alertModelOpen: false,
  btnDisabled: false,
  commentModalShow: false,
  comment: ""
};

class TarikhPage extends React.Component {
  state = initialState;

  config = { headers: { "x-auth": localStorage.getItem("x-auth") } };
  hideModal = () => this.setState({ modalOpen: false });

  onDateChange = date => {
    if (date) {
      this.setState(() => ({ date, displaySaveButton: true }));
    }
  };
  onFocusChange = ({ focused }) => {
    this.setState(() => ({ calenderFocused: focused }));
  };
  onReasonChange = e => {
    this.setState({ reason: e.target.value });
  };
  onStartTimeChange = e => {
    this.setState({ startTime: e.format("HH:mm") });
  };
  onEndTimeChange = e => {
    this.setState({ endTime: e.format("HH:mm") });
  };
  onSubmit = () => {
    this.setState({ btnDisabled: true });
    const { reason, startTime, endTime } = this.state;
    let date = this.state.date.valueOf();
    if (!reason || !startTime || !endTime) {
      return alert("all fields are required!");
    }
    const body = { date, reason, startTime, endTime };
    axios
      .post(`/tarikh/${this.props.clientId}`, body, this.config)
      .then(response => {
        this.setState(prevState => ({
          tarikhs: [response.data.tarikh, ...prevState.tarikhs],
          reason: "",
          startTime: "",
          endTime: "",
          date: new Date(),
          activeIndex: -1,
          alertModelOpen: true,
          alertType: "success",
          alertMessage: "New tarikh scheduled!",
          btnDisabled: false
        }));
      })
      .catch(e => {
        this.setState({
          alertModelOpen: true,
          alertType: "error",
          alertMessage: "Unable To Schedule Tarikh!",
          btnDisabled: false
        });
        console.log(e);
      });
  };
  onEditTarikh = editedTarikh => {
    this.setState(prevState => {
      let selectedTarikh;
      const tarikhs = prevState.tarikhs.map(tarikh => {
        if (tarikh._id === editedTarikh._id) {
          selectedTarikh = editedTarikh;
          return editedTarikh;
        }
        return tarikh;
      });
      return {
        tarikhs,
        selectedTarikh
      };
    });
  };
  handleClick = (e, titleProps) => {
    const { index } = titleProps;
    const { activeIndex } = this.state;
    const newIndex = activeIndex === index ? -1 : index;
    this.setState({ activeIndex: newIndex });
  };
  getData = clientId => {
    axios
      .get(`/tarikh/${clientId}`, this.config)
      .then(response => {
        this.setState({ ...initialState, tarikhs: response.data.tarikhs });
      })
      .catch(e => {
        alert("Unable to client tarikhs list");
        console.log(e);
      });
  };
  componentDidMount() {
    if (!this.props.clientId) {
      return;
    }
    this.getData(this.props.clientId);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.clientId !== this.props.clientId) {
      if (!nextProps.clientId) {
        return;
      }
      this.getData(nextProps.clientId);
    }
  }
  onSelectCase = () => this.setState({ selectCase: true });
  onEndSelectCase = () => this.setState({ selectCase: false });
  filterCaseInsensitive = (filter, row) => {
    const id = filter.pivotId || filter.id;
    if (!isNaN(row[id])) {
      return row[id] !== undefined
        ? String(row[id]).startsWith(filter.value)
        : true;
    }
    return row[id] !== undefined
      ? String(row[id].toLowerCase()).includes(filter.value.toLowerCase())
      : true;
  };
  toggleTarikhSelection = (key, shift, row) => {
    this.setState({
      selectedRowKey: key,
      selectedTarikh: row,
      modalOpen: true
    });
  };
  isTarikhSelected = key => {
    return this.state.selectedRowKey === key;
  };
  onHideAlertModel = () => this.setState({ alertModelOpen: false });
  showComment = comment => this.setState({ commentModalShow: true, comment });
  hideComment = () => this.setState({ commentModalShow: false, comment: "" });
  render() {
    const {
      isTarikhSelected: isSelected,
      toggleTarikhSelection: toggleSelection
    } = this;
    const {
      selectType,
      modalOpen,
      alertModelOpen,
      alertType,
      alertMessage
    } = this.state;
    const extraProps = { isSelected, toggleSelection, selectType };
    if (!this.props.caseId) {
      return (
        <Grid>
          <Grid.Column mobile={16} computer={12}>
            <Message warning>
              <Message.Header>
                <p>Search and select a case</p>
                <Form.Input
                  size="mini"
                  icon={<Icon name="search" />}
                  placeholder="Search Case..."
                  onClick={this.onSelectCase}
                />
              </Message.Header>
              <SearchCase
                modalOpen={this.state.selectCase}
                onEndSelectCase={this.onEndSelectCase}
                {...this.props}
              />
            </Message>
          </Grid.Column>
        </Grid>
      );
    }
    const { activeIndex } = this.state;
    return (
      <Grid>
        <Grid.Column mobile={16} computer={12}>
          <Message info>
            <Message.Header>
              Selected Client: {this.props.clientName}
            </Message.Header>
            <p>
              Case id - {this.props.caseId}{" "}
              {this.props.caseNo && (
                <span>, Case No - {this.props.caseNo}</span>
              )}
            </p>
            <Button color="orange" onClick={this.onSelectCase}>
              Change Client
            </Button>
            <SearchCase
              modalOpen={this.state.selectCase}
              onEndSelectCase={this.onEndSelectCase}
              {...this.props}
            />
          </Message>
          <Accordion style={{ margin: "10px 0" }}>
            <Accordion.Title
              active={activeIndex === 0}
              index={0}
              onClick={this.handleClick}
            >
              <Icon name="dropdown" />
              <Label color="blue">Schedule New Tarikh</Label>
            </Accordion.Title>
            <Accordion.Content active={activeIndex === 0}>
              <Form style={{ maxWidth: "400px" }}>
                <Form.Group>
                  <Form.Field inline>
                    <label>Pick Date</label>
                    <DatePicker
                      selected={this.state.date}
                      onChange={this.onDateChange}
                    />
                  </Form.Field>
                  <Form.Field inline>
                    <label>Start Time</label>
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      defaultValue={this.state.startTime}
                      onChange={this.onStartTimeChange}
                      use12Hours
                    />
                  </Form.Field>
                  <Form.Field inline>
                    <label>End Time</label>
                    <TimePicker
                      style={{ width: 100 }}
                      showSecond={false}
                      defaultValue={this.state.endTime}
                      onChange={this.onEndTimeChange}
                      use12Hours
                    />
                  </Form.Field>
                </Form.Group>
                <Form.TextArea
                  rows={2}
                  placeholder="write a reason..."
                  value={this.state.reason}
                  onChange={this.onReasonChange}
                  style={{ marginBottom: "5px" }}
                />
                <div style={{ display: "flex", justifyContent: "flex-end" }}>
                  <Button
                    color="orange"
                    size="tiny"
                    onClick={this.onSubmit}
                    disabled={this.props.caseClosed || this.state.btnDisabled}
                  >
                    Schedule New Tarikh
                  </Button>
                </div>
              </Form>
            </Accordion.Content>
          </Accordion>
          <SelectTreeTable
            data={this.state.tarikhs}
            columns={[
              {
                Header: "Date",
                accessor: "date",
                Cell: ({ value }) => moment(value).format("LL")
              },
              {
                Header: "Time",
                Cell: row =>
                  `${row.original.startTime} - ${row.original.endTime}`
              },
              {
                Header: "Reason",
                accessor: "reason",
                Cell: ({ value }) => (
                  <Icon
                    color="violet"
                    name="comment outline"
                    onClick={() => this.showComment(value)}
                  />
                )
              },
              {
                Header: "Appeared",
                accessor: "appeared",
                Cell: ({ value }) => (value ? "Yes" : "No")
              },
              {
                Header: "Conclusion",
                accessor: "conclusion",
                Cell: ({ value }) => (
                  <Icon
                    color="green"
                    name="comment outline"
                    onClick={() => this.showComment(value)}
                  />
                )
              }
            ]}
            defaultFilterMethod={this.filterCaseInsensitive}
            defaultPageSize={7}
            pageSizeOptions={[5, 10, 15]}
            {...extraProps}
            className="-striped -highlight"
          />
        </Grid.Column>
        <TarikhUpdate
          {...this.state.selectedTarikh}
          hideModal={this.hideModal}
          modalOpen={modalOpen}
          onEditTarikh={this.onEditTarikh}
          caseClosed={this.props.caseClosed}
        />
        <AlertModel
          open={alertModelOpen}
          alertType={alertType}
          message={alertMessage}
          onHideAlertModel={this.onHideAlertModel}
        />
        <ViewComment
          show={this.state.commentModalShow}
          comment={this.state.comment}
          hideComment={this.hideComment}
        />
      </Grid>
    );
  }
}

const mapStateToProps = ({ selectedClient }) => ({
  clientName: selectedClient.fullName,
  clientId: selectedClient._id,
  caseId: selectedClient.caseId,
  caseNo: selectedClient.caseNo,
  caseClosed: selectedClient.caseClosed
});

export default connect(mapStateToProps)(TarikhPage);
