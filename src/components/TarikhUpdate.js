import React from 'react';
import axios from 'axios';
import { Button, TextArea, Checkbox, Modal, Header, Form } from 'semantic-ui-react';
import { SingleDatePicker } from 'react-dates';
import moment from 'moment';
class TarikhUpdate extends React.Component {
    state = {
        appeared: false,
        conclusion: '',
        disabled: false,
        startTime: '',
        endTime: '',
        reason: '',
        date: moment(),
        calenderFocused: false
    }
    componentWillReceiveProps({ appeared = false, conclusion = '', startTime = '', endTime = '', reason = '', date = null }) {

        this.setState({ appeared, conclusion, startTime, endTime, reason, date: moment(date) });

    }
    onDateChange = (date) => {
        if (date) {
            this.setState(() => ({ date, displaySaveButton: true }))
        }
    }
    onFocusChange = ({ focused }) => {
        this.setState(() => ({ calenderFocused: focused }))
    }
    onAppearedChange = (e) => {
        this.setState((prevState) => ({
            appeared: !prevState.appeared,
        }))
    }
    onReasonChange = (e) => {
        this.setState({ reason: e.target.value })
    }
    onEndTimeChange = (e) => {
        this.setState({ endTime: e.target.value })
    }
    onStartTimeChange = (e) => {
        this.setState({ startTime: e.target.value })
    }
    onConclusionChange = (e) => {
        this.setState({ conclusion: e.target.value })
    }
    onSubmit = () => {
        this.setState({ disabled: true })
        const { appeared, conclusion, reason, startTime, endTime } = this.state;
        let date = this.state.date.valueOf();
        const data = { appeared, conclusion, reason, startTime, endTime, date };
        axios.patch(`/tarikh/${this.props._id}`, data, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            alert('Tarikh Updated Successfully!')
            this.setState({ disabled: false })
            this.props.onEditTarikh(response.data.tarikh);
        }).catch((e) => {
            alert('Unable to update');
            this.setState({ disabled: false })
        })
    }
    hideModal = () => this.props.hideModal();
    render() {
        return (
            <Modal
                open={this.props.modalOpen}
                onClose={this.hideModal}
                size="tiny"
            >
                <Modal.Content scrolling>
                    <Modal.Description>
                        <Header>Update Tarikh</Header>
                        <Form>
                            <Form.Field inline>
                                <label>Pick Date</label>
                                <SingleDatePicker
                                    date={this.state.date}
                                    onDateChange={this.onDateChange}
                                    focused={this.state.calenderFocused}
                                    onFocusChange={this.onFocusChange}
                                    isOutsideRange={() => false}
                                    numberOfMonths={1}
                                />

                            </Form.Field>
                            <Form.Group width="equal">
                                <Form.Input label="Start time" type="time" value={this.state.startTime} onChange={this.onStartTimeChange} style={{ marginBottom: '3px' }} />
                                <Form.Input label="End time" type="time" value={this.state.endTime} onChange={this.onEndTimeChange} />
                            </Form.Group>
                            <Form.TextArea
                                rows={2}
                                placeholder="write a reason..."
                                value={this.state.reason}
                                onChange={this.onReasonChange}
                                style={{ marginBottom: '5px' }}
                                onChange={this.onReasonChange}
                            />

                            <Form.Checkbox
                                slider
                                label="Client Appeared"
                                checked={this.state.appeared}
                                onChange={this.onAppearedChange}
                            />
                            <Form.TextArea
                                rows={2}
                                label="Conclusion"
                                value={this.state.conclusion}
                                onChange={this.onConclusionChange}
                            />
                            <Button
                                onClick={this.onSubmit}
                                disabled={this.state.disabled}
                                color="orange"
                                size="tiny"
                                disabled={this.props.caseClosed}
                            >
                                Save
                            </Button>
                        </Form>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button primary onClick={this.hideModal}>
                        Close
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

export default TarikhUpdate;