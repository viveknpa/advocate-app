import React from 'react';
import axios from 'axios';
import _ from 'lodash';
import { connect } from 'react-redux';
import { Form, Breadcrumb, Segment, Button, Input, Dropdown, Icon, Grid } from 'semantic-ui-react';
import AlertModel from './AlertModel';

import { EditorState, convertToRaw, convertFromHTML, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

function uploadImageCallBack(file) {
    return new Promise(
        (resolve, reject) => {
            const xhr = new XMLHttpRequest(); // eslint-disable-line no-undef
            xhr.open('POST', 'https://api.imgur.com/3/image');
            xhr.setRequestHeader('Authorization', 'Client-ID 8d26ccd12712fca');
            const data = new FormData(); // eslint-disable-line no-undef
            data.append('image', file);
            xhr.send(data);
            xhr.addEventListener('load', () => {
                const response = JSON.parse(xhr.responseText);
                resolve(response);
            });
            xhr.addEventListener('error', () => {
                const error = JSON.parse(xhr.responseText);
                reject(error);
            });
        },
    );
}

const initialEditorState = () => {
    return EditorState.createWithContent(
        ContentState.createFromBlockArray(convertFromHTML('Write template content...'))
    )
}

class TemplateFilePage extends React.Component {
    state = {
        files: [],
        selectedFileId: '',
        fileName: '',
        editorState: initialEditorState(),
        activeButton: 'create',
        alertModelOpen: false,
        alertMessage: ''
    }
    config = { headers: { 'x-auth': localStorage.getItem('x-auth') } }

    componentDidMount() {
        axios.get(`/templatefile`, this.config).then((response) => {
            this.setState({ files: response.data.files })
        }).catch((e) => {
            alert('Unable to get template files');
            console.log(e);
        })
    }

    onFileNameChange = (e) => {
        this.setState({ fileName: e.target.value })
    }

    createFile = () => {
        this.setState({
            editorState: initialEditorState(),
            fileName: '',
            selectedFileId: '',
            activeButton: 'create'
        })
    }

    onEditorStateChange = (editorState) => this.setState({ editorState });

    onFileChange = (e, { value }) => {
        if (!value) { return }
        this.setState({ activeButton: 'edit' })
        const selectedFile = this.state.files.filter(({ _id }) => _id === value)[0];
        const fileName = selectedFile.name;
        const fileText = selectedFile.text;
        let rawFileText;
        if (convertFromHTML(fileText).contentBlocks) {
            rawFileText = convertFromHTML(fileText);
        } else {
            rawFileText = convertFromHTML('<p>Write...</p>');
        }
        const editorState = EditorState.createWithContent(
            ContentState.createFromBlockArray(rawFileText)
        )
        this.setState({
            selectedFileId: value,
            editorState,
            fileName
        });
    }

    onSave = () => {
        const fileText = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
        const fileName = this.state.fileName;
        if (!fileName) {
            return alert('Give a name to your file!')
        }
        const fileId = this.state.selectedFileId;
        if (!fileId) {
            return axios.post(`/templatefile`, { name: fileName, text: fileText }, this.config).then((response) => {
                const newFile = response.data.file;
                this.setState((prevState) => ({
                    files: [newFile, ...prevState.files],
                    selectedFileId: newFile._id,
                    fileName: newFile.name,
                    alertModelOpen: true,
                    alertType: 'success',
                    alertMessage: 'File Saved Successfuly!'
                }))
            }).catch((e) => {
                this.setState({
                    alertModelOpen: true,
                    alertType: 'error',
                    alertMessage: 'Error: Unable to Save File!'
                })
                console.log(e);
            })
        }

        axios.patch(`/templatefile/${fileId}`, { name: fileName, text: fileText }, this.config).then((response) => {
            const editedFile = response.data.file;
            this.setState((prevState) => ({
                files: prevState.files.map((file) => {
                    if (file._id === editedFile._id) {
                        return editedFile
                    }
                    return file;
                }),
                alertModelOpen: true,
                alertType: 'success',
                alertMessage: 'File Saved Successfuly!'
            }))
        }).catch((e) => {
            this.setState({
                alertModelOpen: true,
                alertType: 'error',
                alertMessage: 'Error: Unable to Save File!'
            })
            console.log(e);
        })
    }

    onHideAlertModel = () => this.setState({ alertModelOpen: false, alertMessage: '' })

    render() {
        const fileOptions = this.state.files.map(({ _id, name }) => ({ key: _id, value: _id, text: name }));
        fileOptions.unshift({ key: 'select', value: '', text: 'Select' });
        const { alertModelOpen, alertType, alertMessage } = this.state;
        return (
            <Grid>
                <Grid.Column mobile={16} computer={12}>
                    <Form>
                        <div style={{ marginBottom: '15px' }}>
                            <Button
                                style={this.state.activeButton === 'create' ? { background: "#634987", color: 'white' } : {}}
                                onClick={this.createFile}
                            >
                                <Icon name="add" />
                                New File
                    </Button>
                            <Dropdown
                                button
                                className='icon'
                                floating
                                labeled
                                icon='edit outline'
                                options={fileOptions}
                                search
                                text='Edit file'
                                style={this.state.activeButton === 'edit' ? { background: "#634987", color: 'white' } : {}}
                                value={this.state.selectedFileId}
                                onChange={this.onFileChange}
                            />
                        </div>
                        <Form.Field inline>
                            <label>Name of file</label>
                            <Input
                                style={{ width: '260px' }}
                                type="text"
                                placeholder="name of template file"
                                value={this.state.fileName}
                                onChange={this.onFileNameChange}
                            />
                        </Form.Field>
                        <Editor
                            editorState={this.state.editorState}
                            wrapperClassName="demo-wrapper"
                            editorClassName="demo-editor"
                            onEditorStateChange={this.onEditorStateChange}
                            toolbar={{
                                inline: { inDropdown: true },
                                list: { inDropdown: true },
                                textAlign: { inDropdown: true },
                                link: { inDropdown: true },
                                history: { inDropdown: true },
                                image: { uploadCallback: uploadImageCallBack, previewImage: true, alt: { present: true } },
                            }}
                        />
                        <Button onClick={this.onSave} primary size="tiny">Save</Button>
                    </Form>
                </Grid.Column>
                <AlertModel
                    open={alertModelOpen}
                    alertType={alertType}
                    message={alertMessage}
                    onHideAlertModel={this.onHideAlertModel}
                />
            </Grid>
        )
    }
}

export default TemplateFilePage;