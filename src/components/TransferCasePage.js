import React from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { Message, Icon, Input, Form, Button, Grid, Loader, Confirm } from 'semantic-ui-react'

import SearchCase from './SearchCase';
import AlertModel from './AlertModel';

import { selectClient } from '../actions/selectedClient';

class TransferCasePage extends React.Component {

    state = {
        email: '',
        selectCase: false,
        loaderActive: false,
        confirmModelOpen: false,
        isConfirmed: false
    }

    handleFieldChange = (e, { name, value }) => this.setState({ [name]: value });

    onSubmit = (e) => {
        e.preventDefault();
        this.setState({ confirmModelOpen: true });
    }

    handleConfirmTransferCase = () => {
        this.setState({ loaderActive: true, isConfirmed: false })
        axios.post(`/transfercase/${this.props.clientId}`, {email: this.state.email},  { 
            headers: { 'x-auth': localStorage.getItem('x-auth') } 
        }).then(() => {
            this.props.dispatch(selectClient({}));
            this.setState({ 
                alertModelOpen: true,
                alertType: 'success', 
                alertMessage: `Case is transferred to ${email}!`,
                loaderActive: false
            })
        }).catch((e) => {
            console.log(e);
            this.setState({ 
                alertModelOpen: true,
                alertType: 'error', 
                alertMessage: 'Error in transferring current case!',
                loaderActive: false
            })
        })
    }

    onSelectCase = () => this.setState({ selectCase: true })
    onEndSelectCase = () => this.setState({ selectCase: false });

    handleConfirm = () => this.setState({ isConfirmed: true, confirmModelOpen: false })
    handleCancel = () => this.setState({ isConfirmed: false, confirmModelOpen: false })

    onHideAlertModel = () => this.setState({ alertModelOpen: false })
    render() {
        if (!this.props.caseId) {
            return (
                <Message warning>
                    <Message.Header>
                        <p>Search and select a case</p>
                        <Form.Input
                            size="mini"
                            icon={<Icon name='search' />}
                            placeholder='Search Case...'
                            onClick={this.onSelectCase}
                        />
                    </Message.Header>
                    <SearchCase
                        modalOpen={this.state.selectCase}
                        onEndSelectCase={this.onEndSelectCase}
                        {...this.props}
                    />
                </Message>
            )
        }
        const { alertModelOpen, alertType, alertMessage, confirmModelOpen, isConfirmed, loaderActive } = this.state;
        if(!confirmModelOpen && isConfirmed) {
            this.handleConfirmTransferCase();
        }
        return (
            <Grid>
                <Grid.Column mobile={16} computer={12}>
                    <Message info>
                        <Message.Header>Selected Client: {this.props.clientName}</Message.Header>
                        <p>Case id - {this.props.caseId} {this.props.caseNo && <span>, Case No - {this.props.caseNo}</span>}</p>
                        <Button color="orange" onClick={this.onSelectCase}>Change Client</Button>
                        <SearchCase
                            modalOpen={this.state.selectCase}
                            onEndSelectCase={this.onEndSelectCase}
                            {...this.props}
                        />
                    </Message>
                    <Message compact warning content='You will lose all your access to this case by transferring!' />
                    <Form onSubmit={this.onSubmit}>
                        <Form.Group>
                            <Form.Input 
                                placeholder='Enter email' 
                                name='email' 
                                type="email" 
                                value={this.state.email} 
                                onChange={this.handleFieldChange}
                                required 
                            />
                            <Form.Button 
                                color="red"
                                disabled={this.props.caseClosed}
                            >
                                {loaderActive ? 'Transfering Case' : 'Transfer Case'}
                                <Loader 
                                    active={loaderActive} 
                                    inline  
                                    size="tiny"
                                    style={{marginLeft: '7px', color: 'white'}}
                                />
                            </Form.Button>
                        </Form.Group>
                    </Form>
                </Grid.Column>
                <Confirm 
                    size="mini" 
                    open={this.state.confirmModelOpen} 
                    onCancel={this.handleCancel} 
                    onConfirm={this.handleConfirm} 
                    centered={false}
                />
                <AlertModel 
                    open={alertModelOpen}
                    alertType={alertType}
                    message={alertMessage}
                    onHideAlertModel={this.onHideAlertModel}
                />
            </Grid>
        )
    }
}

const mapStateToProps = ({ selectedClient }) => ({
    clientName: selectedClient.fullName,
    clientId: selectedClient._id,
    caseId: selectedClient.caseId,
    caseNo: selectedClient.caseNo,
    caseClosed: selectedClient.caseClosed
})

export default connect(mapStateToProps)(TransferCasePage);