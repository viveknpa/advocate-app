import React from "react";
import UserListItem from "./UserListItem";
import { Icon } from "semantic-ui-react";
import moment from "moment";
import ReactTable from "react-table";
import selectTableHOC from "react-table/lib/hoc/selectTable";
import treeTableHOC from "react-table/lib/hoc/treeTable";
const SelectTreeTable = selectTableHOC(treeTableHOC(ReactTable));

import { Link } from "react-router-dom";
import { Checkbox, Dropdown, Button, Table, Modal } from "semantic-ui-react";

class UserList extends React.Component {
  state = {
    sentMessages: this.props.sentMessages,
    displaySaveButton: false,
    selectType: "radio",
    selectedTarikh: "",
    selectedRowKey: "",
    modalOpen: false,
    alertModelOpen: false,
    btnDisabled: false,
    commentModalShow: false
  };

  hideModal = () => this.setState({ modalOpen: false });

  filterCaseInsensitive = (filter, row) => {
    console.log(filter, "filter");
    console.log(row, "row");
  };

  toggleTarikhSelection = (key, shift, row) => {
    this.setState({
      selectedRowKey: key,
      selectedTarikh: row,
      modalOpen: true
    });
  };

  onsentMessagesChange = e => {
    this.setState(prevState => ({
      sentMessages: !prevState.sentMessages,
      displaySaveButton: true
    }));
  };
  isTarikhSelected = key => {
    return this.state.selectedRowKey === key;
  };
  render() {
    console.log(this.props);

    const {
      isTarikhSelected: isSelected,
      toggleTarikhSelection: toggleSelection
    } = this;
    const { selectType, modalOpen, alertModelOpen } = this.state;
    const extraProps = { isSelected, toggleSelection, selectType };

    const columns = [
      {
        Header: "Name",
        accessor: "fullName",
        filterMethod: (filter, row) => row[filter.id].startsWith(filter.value)
      },
      {
        Header: "Email",
        accessor: "email",
        filterMethod: (filter, row) => row[filter.id].startsWith(filter.value)
      },
      {
        Header: "Mobile",
        accessor: "mobile"
      },
      {
        Header: "LogIn details",
        accessor: "loginDetail",
        Cell: ({ value }) => (
          <div>
            {value.length && (
              <div>
                <span>{moment(value[value.length - 1]).format("L")}</span>{" "}
                <br />
                <Modal
                  size="tiny"
                  trigger={
                    <span style={{ color: "#634987", cursor: "pointer" }}>
                      See more...
                    </span>
                  }
                  header="Login details!"
                  content={value
                    .map(ts => (
                      <strong key={ts}>
                        {moment(ts).format("LLL")}
                        <br />
                      </strong>
                    ))
                    .reverse()}
                  actions={[
                    {
                      key: "done",
                      content: "close",
                      positive: true,
                      size: "tiny"
                    }
                  ]}
                />
              </div>
            )}
          </div>
        )
      },
      {
        Header: "Account expires on",
        accessor: "accountExpiresOn",
        Cell: ({ value }) => <div>{moment(value).format("l")}</div>
      },
      {
        Header: "Send Messages",
        accessor: "sentMessages",
        Cell: ({ value }) => <div>{value ? "Yes" : "No"}</div>
      },
      {
        Header: "User type",
        accessor: "userType",
        filterMethod: (filter, row) => {
          console.log(filter, "f");
          console.log(row, "r");
          return;
        },
        Cell: ({ value }) => (
          <div>
            {
              this.props.userTypes.filter(userType => userType.type == value)[0]
                .name
            }
          </div>
        )
      },
      {
        Header: "Account Activation",
        accessor: "active",
        Cell: ({ value }) => <div>{value ? "Yes" : "No"}</div>
      },
      {
        Header: "Account locked",
        accessor: "accountLocked",
        Cell: ({ value }) => <div>{value ? "Yes" : "No"}</div>
      }
    ];
    console.log(this.state);
    return (
      <div>
        <SelectTreeTable
          data={this.props.users}
          columns={columns}
          defaultPageSize={10}
          pageSizeOptions={[10, 20]}
          className="-striped -highlight"
          filterable
          defaultFilterMethod={(filter, row) =>
            String(row[filter.id]) == filter.value
          }
          {...extraProps}
        />
        <UserListItem
          {...this.state.selectedTarikh}
          hideModal={this.hideModal}
          modalOpen={modalOpen}
          userTypes={this.props.userTypes}
          handleUserUpdate={this.props.handleUserUpdate}
        />
      </div>
    );
  }
}

export default UserList;
