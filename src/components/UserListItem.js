import React, { Component } from "react";
import axios from "axios";
import {
  Button,
  TextArea,
  Checkbox,
  Modal,
  Header,
  Form
} from "semantic-ui-react";
import { SingleDatePicker } from "react-dates";
import moment from "moment";
class UserListItem extends Component {
  state = {
    _id: this.props._id,
    email: this.props.email,
    userType: this.props.userType,
    active: this.props.active,
    sentMessages: this.props.sentMessages,
    accountLocked: this.props.accountLocked,
    displaySaveButton: false,
    accountExpiresOn: this.props.accountExpiresOn
      ? moment(this.props.accountExpiresOn)
      : moment(),
    calenderFocused: false
  };

  componentWillReceiveProps(nextProps) {
    this.setState({
      _id: nextProps._id,
      email: nextProps.email,
      accountLocked: nextProps.accountLocked,
      active: nextProps.active,
      email: nextProps.email,
      userType: nextProps.userType
    });
  }
  hideModal = () => this.props.hideModal();

  onDateChange = accountExpiresOn => {
    if (accountExpiresOn) {
      this.setState(() => ({ accountExpiresOn, displaySaveButton: true }));
    }
  };
  onFocusChange = ({ focused }) => {
    this.setState(() => ({ calenderFocused: focused }));
  };

  onAccountLockedChange = e => {
    this.setState(prevState => ({
      accountLocked: !prevState.accountLocked,
      displaySaveButton: true
    }));
  };
  onUserTypeChange = (e, { value }) => {
    this.setState({ userType: value, displaySaveButton: true });
  };
  onAccountActiveChange = e => {
    this.setState(prevState => ({
      active: !prevState.active,
      displaySaveButton: true
    }));
  };
  onsentMessagesChange = e => {
    this.setState(prevState => ({
      sentMessages: !prevState.sentMessages,
      displaySaveButton: true
    }));
  };

  handleUserUpdate = () => {
    console.log(this.state);
    const {
      _id,
      userType,
      accountLocked,
      accountExpiresOn,
      active,
      sentMessages
    } = this.state;
    let updates = {
      userType,
      accountLocked,
      accountExpiresOn,
      active,
      sentMessages
    };
    updates.accountExpiresOn = accountExpiresOn.valueOf();
    this.props
      .handleUserUpdate(_id, updates)
      .then(() => {
        this.setState({ displaySaveButton: false });
      })
      .catch(e => {
        alert("Unable to update user");
        console.log(e);
      });
  };

  render() {
    console.log("props", this.props);
    console.log("state", this.state);
    return (
      <Modal open={this.props.modalOpen} onClose={this.hideModal} closeIcon>
        <Modal.Content image scrolling>
          <Modal.Description>
            <Header>Update Users</Header>
            <Form>
              <Form.Field inline>
                <label>Name: {this.props.fullName}</label>
                <label>Email: {this.props.email}</label>
                <br />
                <label>Mobile: {this.props.mobile}</label>
              </Form.Field>
              <Form.Field inline>
                <label>Account Expires on:</label>
                <SingleDatePicker
                  date={this.state.accountExpiresOn}
                  onDateChange={this.onDateChange}
                  focused={this.state.calenderFocused}
                  onFocusChange={this.onFocusChange}
                  isOutsideRange={() => false}
                  numberOfMonths={1}
                />
              </Form.Field>
              <Form.Checkbox
                slider
                label="Sent Messages"
                checked={this.state.sentMessages}
                onChange={this.onsentMessagesChange}
              />
              <Form.Checkbox
                slider
                label="Account Active"
                checked={this.state.active}
                onChange={this.onAccountActiveChange}
              />
              <Form.Checkbox
                slider
                label="Account Locked"
                checked={this.state.accountLocked}
                onChange={this.onAccountLockedChange}
              />
              <Form.Dropdown
                options={this.props.userTypes.map(element => ({
                  key: element._id,
                  text: element.name,
                  value: element.type
                }))}
                placeholder="Select"
                selection
                value={this.state.userType}
                onChange={this.onUserTypeChange}
              />
              {this.state.displaySaveButton && (
                <Button
                  inverted
                  color="violet"
                  size="tiny"
                  style={{ float: "right" }}
                  onClick={this.handleUserUpdate}
                >
                  Save
                </Button>
              )}
            </Form>
          </Modal.Description>
        </Modal.Content>
      </Modal>
    );
  }
}

export default UserListItem;
