import React from 'react';
import UserTypeListItem from './UserTypeListItem';

import { Icon, Button, Form, Table } from 'semantic-ui-react';

class UserTypeList extends React.Component {
    createUserType = (e) => {
        e.preventDefault();
        const name = e.target.name.value;
        const description = e.target.description.value;
        e.persist();
        this.props.createUserType(name, description).then(() => {
            e.target.name.value = '';
            e.target.description.value = '';
        })
    }
    render() {
        return (
            <div>
                <Form onSubmit={this.createUserType}>
                    <Form.Group>
                        <Form.Input placeholder='Name' name='name' required/>
                        <Form.Input placeholder='Description' name='description' />
                        <Form.Button content='Create new user type' color="violet" />
                    </Form.Group>
                </Form>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>User type</Table.HeaderCell>
                            <Table.HeaderCell>Name</Table.HeaderCell>
                            <Table.HeaderCell>Description</Table.HeaderCell>
                            <Table.HeaderCell>Action</Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {
                            this.props.userTypes.map((userType, index) =>
                                <UserTypeListItem
                                    {...userType}
                                    itemNo={index + 1}
                                    key={userType._id}
                                    deleteUserType={this.props.deleteUserType}
                                />
                            )
                        }
                    </Table.Body>
                </Table>
            </div>
        )
    }
}

export default UserTypeList;

