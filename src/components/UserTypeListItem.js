import React from 'react';
import axios from 'axios';

import { Icon, Button, Input, Table } from 'semantic-ui-react'

class UserTypeListItem extends React.Component {
    state = {
        name: this.props.name || '',
        description: this.props.description || '',
        displayForms: false
    }
    handleDisplayForms = () => {
        this.setState((prevState) => ({
            displayForms: !prevState.displayForms
        }))
    }
    handleNameChange = (e) => {
        this.setState({ name: e.target.value })
    }
    handleDescriptionChange = (e) => {
        this.setState({ description: e.target.value })
    }
    onSave = () => {
        const name = this.state.name;
        const description = this.state.description;
        axios.patch(`/userType/edit/${this.props._id}`, { description, name }, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            this.setState({ displayForms: false })
        }).catch((e) => {
            console.log(e);
        })
    }
    onDelete = () => {
        if (confirm("All users of this type will become normal user. Are you sure?")) {
            this.props.deleteUserType(this.props._id, this.props.type);
        }
    }
    render() {
        return (
            <Table.Row>
                <Table.Cell>{this.props.type}</Table.Cell>
                <Table.Cell>
                    {!this.state.displayForms ? this.state.name : (
                        <Input 
                            type="text" 
                            value={this.state.name} 
                            onChange={this.handleNameChange}
                            placeholder="Name"
                        />
                    )}
                </Table.Cell>
                <Table.Cell>
                    {!this.state.displayForms ? this.state.description : (
                        <Input 
                            type="text" 
                            value={this.state.description} 
                            onChange={this.handleDescriptionChange}
                            placeholder="Description"
                        />
                    )}
                </Table.Cell>
                <Table.Cell>
                    {!this.state.displayForms ? 
                        <Button onClick={this.handleDisplayForms} size="mini" color="violet" basic>Edit</Button> : (
                        <Button onClick={this.onSave} size="mini" color="violet" >Save</Button>
                    )}
                <Button onClick={this.onDelete} style={{marginLeft: '10px'}} size="mini" color="red">Delete</Button>
                </Table.Cell>
            </Table.Row>
        )
    }
}

export default UserTypeListItem;