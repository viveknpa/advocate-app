import React from 'react';
import axios from 'axios';
import { Tab } from 'semantic-ui-react';
import { connect } from 'react-redux';
import _ from 'lodash'

import UserTypeList from './UserTypeList';
import UserList from './UserList';
import Authority from './Authority';

class Users extends React.Component {
    state = {
        users: [],  //array of user docs 
        userTypes: [],  //will be assigned to an array containing all user types
        activeTab: '1'
    }

    handleUserUpdate = (id, updates) => {
        
        return axios({
            method: 'post',
            url: `/users/adminupdate/${id}`,
            data: updates,
            headers: {'x-auth': localStorage.getItem('x-auth')}
        }).then(() => {
            this.setState((prevState) => ({
                users: prevState.users.map((user) => {
                    if(user._id == id) {
                        user.userType = updates.userType;
                        return user;
                    }
                    return user;
                })
            }))
        })
    }

    createUserType = (name, description) => {
        return axios.post('/usertype/create', {name, description}, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            alert('new Usertype created');
            this.setState((prevState) => ({
                userTypes: [...prevState.userTypes, response.data.userType]
            }));
        }).catch((e) => {
            alert('Unable to create new user type');
            console.log(e);
        })
    }

    deleteUserType = (id, userType) => {
        axios.delete(`/usertype/remove/${id}`, {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then(() => {
            this.setState((prevState) => ({
                userTypes: prevState.userTypes.filter((userType) => userType._id !== id ),
                users: prevState.users.map((user) => {
                    if(user.userType == userType) {
                        user.userType = 5;
                        return user;
                    }
                    return user;
                })
            }))
        }).catch((e) => {
            alert('Unable to remove user type');
            console.log(e);
        })
    }

    componentWillMount() {
        axios.get('/users/list', {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            this.setState({ users: response.data.users })
        }).catch((e) => {
            console.log(e);
        })

        axios.get('/usertype/list', {
            headers: { 'x-auth': localStorage.getItem('x-auth') }
        }).then((response) => {
            this.setState({ userTypes: response.data.userTypes })
        }).catch((e) => {
            console.log(e);
        })
    }
    render() {
        const panes = [
            { menuItem: 'users',
              render: () => 
                <Tab.Pane attached={false}>
                    <UserList
                        users={this.state.users} 
                        userTypes={this.state.userTypes} 
                        currentUserType={this.props.userType}
                        handleUserUpdate={this.handleUserUpdate} 
                    />
                </Tab.Pane> 
            },
            { menuItem: 'user type',
              render: () => 
                <Tab.Pane attached={false}>
                    <UserTypeList 
                        userTypes={this.state.userTypes} 
                        createUserType={this.createUserType}
                        deleteUserType={this.deleteUserType}
                    /> 
                </Tab.Pane> 
            },
            { menuItem: 'Authority',
              render: () => 
                <Tab.Pane attached={false}>
                    <Authority 
                        userTypes={this.state.userTypes} 
                    /> 
                </Tab.Pane> 
            }
        ]
        return (
            <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
        )
    }
}

const mapStateToProps = (state) => ({
    currentUserType: state.auth.userType
})

export default connect(mapStateToProps)(Users);