import React from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Segment, Button, Menu, Form, Input } from 'semantic-ui-react';

class VerifyAccount extends React.Component {
    state = {
        secretToken: '',
        verified: false,
        error: '',
        resendingCode: false,
        newCodeSent: false,
        disabled: false
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value, error: '' });

    handleSubmit = (event) => {
        event.preventDefault();
        const secretToken = this.state.secretToken;
        if (secretToken.trim() < 32) {
            return this.setState({ error: 'invalid code', verified: false });
        }
        this.setState({ disabled: true })
        axios.post('/verifyaccount', { secretToken }).then(() => {
            this.setState({ secretToken: '', error: '', verified: true, disabled: false });
        }).catch((e) => {
            console.log(e);
            this.setState({ error: 'invalid code', verified: false, disabled: false, newCodeSent: false });
        })
    }

    resendEmail = () => {
        const email = this.props.match.params.email;
        if (!email) { return }
        this.setState({ resendingCode: true, error: '', secretToken: '' })
        axios.post('/sendVerificationCode', { action: 'resend', email }).then(() => {
            this.setState({ newCodeSent: true, resendingCode: false })
        }).catch((e) => {
            this.setState({ newCodeSent: false, resendingCode: false })
            alert('Unable to send Code!')
            console.log(e);
        })
    }
    render() {
        return (
            <div>
                <Segment style={{ paddingTop: 0, paddingBottom: 0, margin: 0, borderBottom: '2px solid #9D81C3' }}>
                    <Menu secondary>
                        <Menu.Item style={{ padding: 0 }} as={Link} to="/">
                            <img
                                src='/files/logo.png'
                                style={{ width: '190px', height: '75px', paddingBottom: '5px' }}
                            />
                        </Menu.Item>
                    </Menu>
                </Segment>
                <div style={{ display: 'flex', justifyContent: 'center', marginTop: '40px' }}>
                    <Form
                        onSubmit={this.handleSubmit}
                    >
                        <h1 style={{ fontWeight: 200 }}>Verify your email address.</h1>
                        <p>We have sent you a Security Code to verify your account</p>
                        {this.state.verified && <span style={{ display: 'block', marginBottom: '10px', color: 'orange', fontStyle: 'italic', fontSize: '18px' }}>Verified, now you may login <Link to="/"> Go back</Link></span>}
                        {this.state.error && <span style={{ display: 'block', marginBottom: '10px', color: 'orange', fontStyle: 'italic', fontSize: '18px' }}>{this.state.error}</span>}
                        <Form.Group style={{ marginBottom: '5px' }}>
                            <Form.Input
                                type="text"
                                name="secretToken"
                                placeholder="paste the Code here"
                                value={this.state.secretToken}
                                onChange={this.handleChange}
                                required
                            />
                            <Form.Button
                                content='Submit'
                                color="violet"
                                disabled={this.state.disabled}
                            />
                        </Form.Group>
                        {this.state.resendingCode &&
                            <span>Resending Code...</span>
                        }
                        {this.state.newCodeSent ? (
                            <span style={{ color: 'orange' }}>New Code Sent to Your Email!</span>
                        ) : (
                                <span
                                    style={{ cursor: 'pointer', color: 'blue' }}
                                    onClick={this.resendEmail}
                                >
                                    Didn't get a code?
                                </span>
                            )
                        }
                    </Form>
                </div>
            </div>
        )
    }
}

export default VerifyAccount;