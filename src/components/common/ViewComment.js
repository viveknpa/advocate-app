import React from 'react'
import { Button, Modal } from 'semantic-ui-react';

class ViewComment extends React.Component {
    state = {
        show: false,
        comment: ''
    }
    componentWillReceiveProps(nextProps) {
        this.setState({ show: nextProps.show, comment: nextProps.comment })
    }
    hideModal = () => this.props.hideComment();
    render() {
        return (
            <Modal
                open={this.props.show}
                onClose={this.hideModal}
                size="mini"
            >
                <Modal.Content scrolling>
                    <Modal.Description>
                        <p>{this.state.comment}</p>
                    </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                    <Button primary onClick={this.hideModal}>
                        Close
                </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

export default ViewComment;