const monthOptions = [
    { key: 1, value: 'January', text: 'January' },
    { key: 2, value: 'February', text: 'February' },
    { key: 3, value: 'March', text: 'March' },
    { key: 4, value: 'April', text: 'April' },
    { key: 5, value: 'May', text: 'May' },
    { key: 6, value: 'June', text: 'June' },
    { key: 7, value: 'July', text: 'July' },
    { key: 8, value: 'August', text: 'August' },
    { key: 9, value: 'September', text: 'September' },
    { key: 10, value: 'October', text: 'October' },
    { key: 11, value: 'November', text: 'November' },
    { key: 12, value: 'December', text: 'December' }
]

const monthDateOptions = [];
for (let i = 1; i <= 31; i++) {
    monthDateOptions.push({ key: i, text: i, value: i });
}

const yearOptions = [];
for (let i = 2018; i >= 1955; i--) {
    yearOptions.push({ key: i, text: i, value: i });
}

export { monthOptions, monthDateOptions, yearOptions }
