export default (state = {}, action) => {
    switch(action.type) {
        case 'SELECT_CLIENT': 
            return { ...action.client }
        default: 
            return state;
    }
}