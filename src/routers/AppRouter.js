import React from 'react';
import { Router, Route, Switch} from 'react-router-dom';
import PrivateRoute from './PrivateRoute';
import PublicRoute from './PublicRoute';
import createHistory from 'history/createBrowserHistory'
import DashboardPage from '../components/DashboardPage';
import NotFoundPage from '../components/NotFoundPage';
import LoginPage from '../components/LoginPage';
import VerifyAccount from '../components/VerifyAccount';
import ForgotPassword from '../components/ForgotPassword'; 
import ResetPassword from '../components/ResetPassword';
import Users from '../components/Users';
import EmployeePage from '../components/EmployeePage';
import ClientPage from '../components/ClientPage';
import ClientPassbookPage from '../components/ClientPassbookPage';
import AdvocatePassbookPage from '../components/AdvocatePassbookPage';
import TarikhPage from '../components/TarikhPage';
import CalenderPage from '../components/CalenderPage';
import TemplateFilePage from '../components/TemplateFilePage';
import ClientFilePage from '../components/ClientFilePage';
import ProfilePage from '../components/ProfilePage';
import ClientDocPage from '../components/ClientDocPage';
import MessageClientPage from '../components/MessageClientPage';
import TransferCasePage from '../components/TransferCasePage';
import SupportPage from '../components/SupportPage';
import RequestAdsPage from '../components/RequestAdsPage';
import ApproveAdsPage from '../components/ApproveAdsPage';
import AddCourtWiseEventPage from '../components/AddCourtWiseEventPage';
import AddCourtPage from '../components/AddCourtPage';
import AppFeaturePage from '../components/AppFeaturePage';
import SupportReplyPage from '../components/SupportReplyPage';
import BulkEmail from '../components/BulkEmail';
import ClientDashboard from '../components/ClientDashboard'
import CauseList from '../components/CauseList';

export const history = createHistory();

const AppRouter = () => ( 
    <Router history={history}>
        <div>
            <Switch>
                <PublicRoute path="/" component={LoginPage} exact={true}/>
                <PublicRoute path="/forgot" component={ForgotPassword} />
                <Route path="/reset/:secretToken" component={ResetPassword} />
                <PublicRoute path="/verify/:email" component={VerifyAccount}/>
                <Route path="/advocate-profile/:uid" component={ProfilePage}/>
                <PrivateRoute path="/dashboard" component={DashboardPage} exact={true}/>
                <PrivateRoute path="/dashboard/users" component={Users}/> 
                <PrivateRoute path="/dashboard/advocate-employee" component={EmployeePage}/> 
                <PrivateRoute path="/dashboard/advocate-client" component={ClientPage}/>  
                <PrivateRoute path="/dashboard/passbook/client" component={ClientPassbookPage}/>
                <PrivateRoute path="/dashboard/passbook/advocate" component={AdvocatePassbookPage}/> 
                <PrivateRoute path="/dashboard/advocate-tarikh" component={TarikhPage}/> 
                <PrivateRoute path="/dashboard/advocate-calender" component={CalenderPage}/> 
                <PrivateRoute path="/dashboard/advocate-clientfile" component={ClientFilePage}/>
                <PrivateRoute path="/dashboard/advocate-templatefile" component={TemplateFilePage}/>
                <PrivateRoute path="/dashboard/advocate-clientDoc" component={ClientDocPage}/> 
                <PrivateRoute path="/dashboard/advocate-messageClient" component={MessageClientPage}/>
                <PrivateRoute path="/dashboard/advocate-transferClient" component={TransferCasePage}/>
                <PrivateRoute path="/dashboard/advocate-ads" component={RequestAdsPage}/>
                <PrivateRoute path="/dashboard/approve-ads" component={ApproveAdsPage}/>
                <PrivateRoute path="/dashboard/court-event" component={AddCourtWiseEventPage}/>
                <PrivateRoute path="/dashboard/add-court" component={AddCourtPage}/>
                <PrivateRoute path="/dashboard/support" component={SupportPage}/> 
                <PrivateRoute path="/dashboard/support-reply" component={SupportReplyPage}/> 
                <PrivateRoute path="/dashboard/app-features" component={AppFeaturePage}/> 
                <PrivateRoute path="/dashboard/bulk-email" component={BulkEmail}/> 
                <PrivateRoute path="/dashboard/ClientDashboard" component={ClientDashboard}/>
                <PrivateRoute path="/dashboard/CauseList" component={CauseList}/>
                <Route component={NotFoundPage} />
            </Switch>
        </div>
    </Router>
);

export default AppRouter;
