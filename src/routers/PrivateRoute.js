import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import MainPage from '../components/MainPage';
import { history } from './AppRouter';

class PrivateRoute extends React.Component { 
    render() {
        const {
            isAuthenticated,
            component: Component,
            ...rest
        } = this.props;
        const path = history.location.pathname;
        const last = path.split("/");
        if( path !== '/dashboard' && 
            path !== '/dashboard/support' &&
            this.props.authority && 
            !this.props.authority[last[2]]
        ) {
            history.push('/not-found');
        } 
        
        return (
            <Route {...rest} component={(props) => (
                isAuthenticated ? (
                    <MainPage><Component {...props} /></MainPage>
                ) : (
                        <Redirect to="/" />
                    )
            )} />
        )
    }
}

const mapStateToProps = (state) => ({
    isAuthenticated: !!state.auth.uid,
    authority: state.auth.authority
})

export default connect(mapStateToProps)(PrivateRoute);
