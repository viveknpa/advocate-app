import thunk from 'redux-thunk';
import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
import authReducer from '../reducers/auth';
import selectedClientReducer from '../reducers/selectedClient';

const composeEnhacers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default () => {
    const store = createStore(
        combineReducers({
            auth: authReducer,
            selectedClient: selectedClientReducer
        }),
        composeEnhacers(applyMiddleware(thunk))
    )
    return store;
}

